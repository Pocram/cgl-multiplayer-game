# Dependencies #

* **SFML 2.3.2**
    * sfml-audio.lib
    * sfml-graphics.lib
    * sfml-system.lib
    * sfml-window.lib
* **MySQL 5.7.13, 32 bit**
    * libmysql.lib
* **Boost 1.59.0**