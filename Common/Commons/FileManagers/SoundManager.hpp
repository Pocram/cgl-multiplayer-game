﻿#ifndef COMMONS_FILE_MANAGERS_SOUND_MANAGER_H
#define COMMONS_FILE_MANAGERS_SOUND_MANAGER_H

#include <map>
#include <string>
#include <SFML/Audio.hpp>

////////////////////////////////////////////////////////////
/// \brief Loads and organizes sounds found in the Assets folder.
///
/// Supports **mp3**, **wav** and **ogg** files.
////////////////////////////////////////////////////////////
class SoundManager
{
public:
	// WHATEVER
	//====================================================================================================
	SoundManager( SoundManager const& ) = delete;
	void operator = ( SoundManager const& ) = delete;

	// METHODS
	//====================================================================================================
	////////////////////////////////////////////////////////////
	/// \brief Loads and organizes the sounds found in the Assets folder.
	///
	/// Use this function before using SoundManager::GetSound()!
	////////////////////////////////////////////////////////////
	static void Initialize();

	////////////////////////////////////////////////////////////
	/// \brief Gets a pointer to a sound found in the Assets folder.
	///
	/// \param soundName The name of the sound. Used as a relative path inside the Assets folder. Example: `Sounds/MySound.wav`
	///
	/// \returns A pointer to the sound. If the sound can not be found, returns `nullptr`.
	////////////////////////////////////////////////////////////
	static sf::SoundBuffer* GetSound( std::string soundName );

private:
	// CONSTRUCTOR
	//====================================================================================================
	SoundManager();
	~SoundManager();

	// METHODS
	//====================================================================================================
	static SoundManager& GetInstance();
	
	// MEMBERS
	//====================================================================================================
	std::map<std::string, sf::SoundBuffer*> _loadedSounds;	//!< A map that associates all sounds with their paths inside the Assets folder.
};

#endif // COMMONS_FILE_MANAGERS_SOUND_MANAGER_H