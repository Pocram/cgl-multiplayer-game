﻿#ifndef COMMONS_FILE_MANAGERS_FONT_MANAGER_H
#define COMMONS_FILE_MANAGERS_FONT_MANAGER_H

#include <map>
#include <string>

#include <SFML/Graphics/Font.hpp>

////////////////////////////////////////////////////////////
/// \brief Loads and organizes fonts found in the Assets folder.
///
/// Supports **otf** and **ttf** files.
////////////////////////////////////////////////////////////
class FontManager
{
public:
	// WHATEVER
	//====================================================================================================
	FontManager( FontManager const& ) = delete;
	void operator = ( FontManager const& ) = delete;

	// METHODS
	//====================================================================================================
	////////////////////////////////////////////////////////////
	/// \brief Loads and organizes the fonts found in the Assets folder.
	///
	/// Use this function before using FontManager::GetFont()!
	////////////////////////////////////////////////////////////
	static void Initialize();

	////////////////////////////////////////////////////////////
	/// \brief Gets a pointer to a font found in the Assets folder.
	///
	/// \param fontName The name of the font. Used as a relative path inside the Assets folder. Example: `Fonts/Arial.ttf`
	///
	/// \returns A pointer to the font. If the font can not be found, returns `nullptr`.
	////////////////////////////////////////////////////////////
	static sf::Font* GetFont( std::string fontName );

private:
	// CONSTRUCTORS
	//====================================================================================================
	FontManager();
	~FontManager();

	// METHODS
	//====================================================================================================
	static FontManager& GetInstance();

	// MEMBERS
	//====================================================================================================
	std::map<std::string, sf::Font*> _loadedFonts;	//!< A map that associates all fonts with their paths inside the Assets folder.
};

#endif // COMMONS_FILE_MANAGERS_FONT_MANAGER_H