﻿#ifndef COMMONS_FILE_MANAGERS_FILE_MANAGER_H
#define COMMONS_FILE_MANAGERS_FILE_MANAGER_H

#include <vector>
#include <string>
#include <boost/filesystem.hpp>

////////////////////////////////////////////////////////////
/// \brief Manages everything assets related.
////////////////////////////////////////////////////////////
class MainFileManager
{
public:
	// WHATEVER
	//====================================================================================================
	MainFileManager( MainFileManager const& ) = delete;
	void operator = ( MainFileManager const& ) = delete;

	// METHODS
	//====================================================================================================
	////////////////////////////////////////////////////////////
	/// \brief Initializes the manager by setting the path to the executable.
	///
	/// \param argv The argv[] of the main() function.
	////////////////////////////////////////////////////////////
	static void Initialize( char* argv[] );

	////////////////////////////////////////////////////////////
	/// \brief Gets the full path to the executable's directory.
	///
	/// \returns The full path as a string.
	////////////////////////////////////////////////////////////
	static const std::string& GetApplicationDirectory();

	////////////////////////////////////////////////////////////
	/// \brief Gets the full path to the Assets directory.
	///
	/// \returns The full path as a string.
	////////////////////////////////////////////////////////////
	static std::string GetAssetsDirectory();

	////////////////////////////////////////////////////////////
	/// \brief Gets the full path to an asset in the Assets directory.
	///
	/// \param pathInAssetFolder The relative path in the Assets folder including file ending.
	///
	/// \returns The full path as a string.
	////////////////////////////////////////////////////////////
	static std::string GetPathToAsset( const std::string& pathInAssetFolder );

	////////////////////////////////////////////////////////////
	/// \brief Checks whether a path points to a file inside the Assets folder.
	///
	/// \param pathToAsset The full boost path to the asset.
	///
	/// \returns Whether it is a part of the Assets folder or not.
	////////////////////////////////////////////////////////////
	static bool IsPathPartOfAssetFolder( boost::filesystem::path pathToAsset );

	////////////////////////////////////////////////////////////
	/// \brief Converts the full path to an asset to the relative path.
	///
	/// \param pathToAsset The full boost path to the asset.
	///
	/// \returns The relative path as a string.
	///
	/// \see MainFileManager::GetRelativeAssetPathFromFullPath( std::string pathToAsset )
	////////////////////////////////////////////////////////////
	static std::string GetRelativeAssetPathFromFullPath( boost::filesystem::path pathToAsset );

	////////////////////////////////////////////////////////////
	/// \brief Converts the full path to an asset to the relative path.
	///
	/// \param pathToAsset The full string path to the asset.
	///
	/// \returns The relative path as a string.
	///
	/// \see MainFileManager::GetRelativeAssetPathFromFullPath( std::string pathToAsset )
	////////////////////////////////////////////////////////////
	static std::string GetRelativeAssetPathFromFullPath( std::string pathToAsset );

	////////////////////////////////////////////////////////////
	/// \brief Finds all files of a specific type inside a folder.
	///
	/// This method only gets the files directly inside this folder.
	/// In order to get all files in each subfolder, use GetAllFilesWithExtensionWithSubdirectories() instead.
	///
	/// \param pathToDirectory	The full string path to the directory.
	/// \param fileExtension	The file extension including the leading dot.
	///
	/// \returns A list of paths relative to the given directory.
	////////////////////////////////////////////////////////////
	static std::vector<std::string> GetAllFilesWithExtension( std::string pathToDirectory, std::string fileExtension );

	////////////////////////////////////////////////////////////
	/// \brief Finds all files of a specific type inside a folder and each subfolder.
	///
	/// This method gets the files of this folder and each subfolder.
	/// If you only want the files in this folder alone, use GetAllFilesWithExtension() instead.
	///
	/// \param pathToDirectory	The full string path to the directory.
	/// \param fileExtension	The file extension including the leading dot.
	///
	/// \returns A list of paths relative to the given directory.
	////////////////////////////////////////////////////////////
	static std::vector<std::string> GetAllFilesWithExtensionWithSubdirectories( std::string pathToDirectory, std::string fileExtension );

	////////////////////////////////////////////////////////////
	/// \brief Gets the path to the meta file of a given path.
	///
	/// Simply exchanges the file ending with xml and adds a leading dot do the file name.
	///
	/// \param pathToAsset The absolute boost path to an asset.
	///
	/// \returns The absolute boost path to the meta file.
	////////////////////////////////////////////////////////////
	static boost::filesystem::path GetPathToMetaFile( boost::filesystem::path pathToAsset );

	////////////////////////////////////////////////////////////
	/// \brief Checks whether an asset has a meta file.
	///
	/// Checks whether an asset has an associated meta file. If desired, creates the file if missing.
	///
	/// \param pathToAsset			The absolute boost path to an asset.
	/// \param createMissingFile	Whether to create a meta file if it's missing.
	///
	/// \returns Whether a meta file exists for this asset or not.
	////////////////////////////////////////////////////////////
	static bool AssetHasMetaFile( boost::filesystem::path pathToAsset, bool createMissingFile );
	
	////////////////////////////////////////////////////////////
	/// \brief Create a meta file for a given asset.
	///
	/// \param filePath The absolute boost path including xml extension.
	///
	/// \returns Whether the file has been created or not.
	////////////////////////////////////////////////////////////
	static bool CreateMetaFile( boost::filesystem::path filePath );

	////////////////////////////////////////////////////////////
	/// \brief Checks whether a meta file specifies to ignore being preloaded.
	///
	/// \param metaFilePath The absolute boost path to the meta file including xml extension.
	///
	/// \returns Whether the Assets should be ignored or not.
	////////////////////////////////////////////////////////////
	static bool CheckIgnorePreload( boost::filesystem::path metaFilePath );


private:
	// CONSTRUCTORS
	//====================================================================================================
	MainFileManager();
	~MainFileManager();

	// METHODS
	//====================================================================================================
	static MainFileManager& GetInstance();
	
	// MEMBERS
	//====================================================================================================
	bool _isInitialized;						//!< Whether the manager has been initialized or not. I'm not even using this ...
	std::string _applicationDirectoryString;	//!< The path to the game's executable.
};

#endif // COMMONS_FILE_MANAGERS_FILE_MANAGER_H