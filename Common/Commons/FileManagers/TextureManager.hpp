﻿#ifndef COMMONS_FILE_MANAGERS_TEXTURE_MANAGER_H
#define COMMONS_FILE_MANAGERS_TEXTURE_MANAGER_H

#include <map>
#include <string>
#include <SFML/Graphics.hpp>

////////////////////////////////////////////////////////////
/// \brief Loads and organizes textures found in the Assets folder.
///
/// Supports **png**, **bmp** and **jpg** files.
////////////////////////////////////////////////////////////
class TextureManager
{
public:
	// WHATEVER
	//====================================================================================================
	TextureManager( TextureManager const& ) = delete;
	void operator = ( TextureManager const& ) = delete;

	// METHODS
	//====================================================================================================
	////////////////////////////////////////////////////////////
	/// \brief Loads and organizes the textures found in the Assets folder.
	///
	/// Use this function before using TextureManager::GetTexture()!
	////////////////////////////////////////////////////////////
	static void Initialize();

	////////////////////////////////////////////////////////////
	/// \brief Gets a pointer to a texture found in the Assets folder.
	///
	/// \param textureName The name of the texture. Used as a relative path inside the Assets folder. Example: `Textures/MyTexture.png`
	///
	/// \returns A pointer to the texture. If the texture can not be found, returns `nullptr`.
	////////////////////////////////////////////////////////////
	static sf::Texture* GetTexture( std::string textureName );

private:
	// CONSTRUCTORS
	//====================================================================================================
	TextureManager();
	~TextureManager();

	// METHODS
	//====================================================================================================
	static TextureManager& GetInstance();

	// MEMBERS
	//====================================================================================================
	std::map<std::string, sf::Texture*> _loadedTextures;	//!< A map that associates all texture with their paths inside the Assets folder.
};

#endif // COMMONS_FILE_MANAGERS_TEXTURE_MANAGER_H