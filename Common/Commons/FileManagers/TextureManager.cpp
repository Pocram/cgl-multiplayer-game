﻿#include "TextureManager.hpp"
#include "MainFileManager.hpp"

#include <boost/foreach.hpp>

void TextureManager::Initialize()
{
	printf( "-- Loading textures ...\n" );
	int TextureCount = 0;

	// Get path to asset folder.
	std::string pathToAssetFolder = MainFileManager::GetAssetsDirectory();

	// Find all PNGs, JPGs and BMPs
	std::vector<std::string> allTextures;
	std::vector<std::string> pngsInFolder = MainFileManager::GetAllFilesWithExtensionWithSubdirectories( pathToAssetFolder, ".png" );
	allTextures.insert( allTextures.end(), pngsInFolder.begin(), pngsInFolder.end() );

	std::vector<std::string> jpgsInFolder = MainFileManager::GetAllFilesWithExtensionWithSubdirectories( pathToAssetFolder, ".jpg" );
	allTextures.insert( allTextures.end(), jpgsInFolder.begin(), jpgsInFolder.end() );

	std::vector<std::string> bmpsInFolder = MainFileManager::GetAllFilesWithExtensionWithSubdirectories( pathToAssetFolder, ".bmp" );
	allTextures.insert( allTextures.end(), bmpsInFolder.begin(), bmpsInFolder.end() );

	// Load textures
	BOOST_FOREACH( std::string texturePath, allTextures )
	{
		sf::Texture* newTexture = new sf::Texture();
		if ( !newTexture->loadFromFile( texturePath ) )
		{
			// Texture could not be found or loaded, go to the next one
			continue;
		}

		std::string fileName = MainFileManager::GetRelativeAssetPathFromFullPath( texturePath );

		// Add texture to map
		GetInstance()._loadedTextures.insert_or_assign( fileName, newTexture );

		// Get size
		/*FILE *textFile = fopen( texturePath.c_str(), "rb" );
		fseek( textFile, 0, SEEK_END );
		std::size_t size = ftell( textFile );*/

		//LoadedTextureSizes.insert_or_assign( fileName, size );
		printf( "Loaded texture '%s'\n", fileName.c_str() );

		TextureCount++;
	}

	printf( "-- Finished loading textures: %d of %d textures loaded!\n", TextureCount, allTextures.size() );
}

sf::Texture* TextureManager::GetTexture( std::string textureName )
{
	std::map<std::string, sf::Texture*>::iterator i = GetInstance()._loadedTextures.find( textureName );
	if ( i != GetInstance()._loadedTextures.end() )
	{
		return i->second;
	}

	return nullptr;
}

TextureManager::TextureManager()
{
}

TextureManager::~TextureManager()
{
}

TextureManager& TextureManager::GetInstance()
{
	static TextureManager instance;
	return instance;
}
