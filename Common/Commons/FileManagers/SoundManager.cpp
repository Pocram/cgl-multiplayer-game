﻿#include "SoundManager.hpp"
#include "MainFileManager.hpp"

#include <boost/foreach.hpp>

void SoundManager::Initialize()
{
	printf( "-- Loading sounds ...\n" );
	int soundCount = 0;

	// Get path to asset folder.
	std::string pathToAssetFolder = MainFileManager::GetAssetsDirectory();

	// Find all PNGs, JPGs and BMPs
	std::vector<std::string> allSounds;
	std::vector<std::string> mp3sInFolder = MainFileManager::GetAllFilesWithExtensionWithSubdirectories( pathToAssetFolder, ".mp3" );
	allSounds.insert( allSounds.end(), mp3sInFolder.begin(), mp3sInFolder.end() );

	std::vector<std::string> wavsInFolder = MainFileManager::GetAllFilesWithExtensionWithSubdirectories( pathToAssetFolder, ".wav" );
	allSounds.insert( allSounds.end(), wavsInFolder.begin(), wavsInFolder.end() );

	std::vector<std::string> oggsInFolder = MainFileManager::GetAllFilesWithExtensionWithSubdirectories( pathToAssetFolder, ".ogg" );
	allSounds.insert( allSounds.end(), oggsInFolder.begin(), oggsInFolder.end() );

	// Load textures
	BOOST_FOREACH( std::string soundPath, allSounds )
	{
		sf::SoundBuffer* newSound = new sf::SoundBuffer();
		if ( !newSound->loadFromFile( soundPath ) )
		{
			// Sound could not be found or loaded, go to the next one
			continue;
		}

		std::string fileName = MainFileManager::GetRelativeAssetPathFromFullPath( soundPath );

		// Add sound to map
		GetInstance()._loadedSounds.insert_or_assign( fileName, newSound );

		// Get size
		/*FILE *textFile = fopen( texturePath.c_str(), "rb" );
		fseek( textFile, 0, SEEK_END );
		std::size_t size = ftell( textFile );*/

		//LoadedTextureSizes.insert_or_assign( fileName, size );
		printf( "Loaded sound '%s'\n", fileName.c_str() );

		soundCount++;
	}

	printf( "-- Finished loading sounds: %d of %d sounds loaded!\n", soundCount, allSounds.size() );
}

sf::SoundBuffer* SoundManager::GetSound( std::string soundName )
{
	std::map<std::string, sf::SoundBuffer*>::iterator i = GetInstance()._loadedSounds.find( soundName );
	if ( i != GetInstance()._loadedSounds.end() )
	{
		return i->second;
	}

	return nullptr;
}

SoundManager::SoundManager()
{
}

SoundManager::~SoundManager()
{
}

SoundManager& SoundManager::GetInstance()
{
	static SoundManager instance;
	return instance;
}
