﻿#include "MainFileManager.hpp"

#include <boost/lexical_cast.hpp>
#include <Commons/XML/tinyxml2.h>

void MainFileManager::Initialize( char* argv[] )
{
	std::string path( *argv );
	std::size_t lastSlash = path.find_last_of( "/\\" );
	std::string directory = path.substr( 0, lastSlash );
	GetInstance()._applicationDirectoryString = directory;
}

const std::string& MainFileManager::GetApplicationDirectory()
{
	return GetInstance()._applicationDirectoryString;
}

std::string MainFileManager::GetAssetsDirectory()
{
	std::string appPath = GetApplicationDirectory();
	appPath.append( "\\Assets\\" );
	return appPath;
}

std::string MainFileManager::GetPathToAsset( const std::string& pathInAssetFolder )
{
	std::string assetDir = GetAssetsDirectory();
	assetDir.append( pathInAssetFolder );

	return assetDir;
}

bool MainFileManager::IsPathPartOfAssetFolder( boost::filesystem::path pathToAsset )
{
	// Iterate over path to asset folder and compare it to the given path.

	// Normalize given path and get iterator
	boost::filesystem::path normalizedPath = boost::filesystem::canonical( pathToAsset );
	boost::filesystem::path::iterator givenPathIterator = normalizedPath.begin();

	// Get path to asset folder and normalize it
	boost::filesystem::path pathToAssetFolder( GetAssetsDirectory() );
	pathToAssetFolder = boost::filesystem::canonical( pathToAssetFolder );

	for ( boost::filesystem::path::iterator i = pathToAssetFolder.begin(); i != pathToAssetFolder.end(); ++i )
	{
		if ( *i != *givenPathIterator )
		{
			return false;
		}
		++givenPathIterator;
	}

	return true;
}

std::string MainFileManager::GetRelativeAssetPathFromFullPath( boost::filesystem::path pathToAsset )
{
	// Return original path if it does not exist or does not point to a file
	if ( !boost::filesystem::exists( pathToAsset ) || !boost::filesystem::is_regular_file( pathToAsset ) )
		return pathToAsset.string();

	// Check if pathToAsset leads to a path inside the asset folder
	boost::filesystem::path pathToAssetFolder( GetAssetsDirectory() );
	if ( IsPathPartOfAssetFolder( pathToAsset ) )
	{
		std::string relativePath;

		// Normalize path and get iterator
		boost::filesystem::path normalizedPath = boost::filesystem::canonical( pathToAsset );
		boost::filesystem::path::iterator pathIterator = normalizedPath.begin();

		// Normalize path to asset folder
		pathToAssetFolder = boost::filesystem::canonical( pathToAssetFolder );

		// Iterate over path to asset until asset folder has been reached.
		for ( boost::filesystem::path::iterator i = pathToAssetFolder.begin(); i != pathToAssetFolder.end(); ++i )
		{
			++pathIterator;
		}

		// Add remaining path to relativePath string.
		for ( pathIterator; pathIterator != normalizedPath.end(); ++pathIterator )
		{
			relativePath.append( pathIterator->string() ).append( "/" );
		}

		// Remove excess / and file extension.
		if ( relativePath.size() > 0 )
		{
			// Remove /
			relativePath.resize( relativePath.size() - 1 );
		}

		return relativePath;
	}

	return pathToAsset.string();
}

std::string MainFileManager::GetRelativeAssetPathFromFullPath( std::string pathToAsset )
{
	return GetRelativeAssetPathFromFullPath( boost::filesystem::path( pathToAsset ) );
}

std::vector<std::string> MainFileManager::GetAllFilesWithExtension( std::string pathToDirectory, std::string fileExtension )
{
	std::vector<std::string> output;
	boost::filesystem::path path( pathToDirectory );

	// Check if the directory exists and is a directory in the first place
	// If not, return the empty vector
	if ( !boost::filesystem::exists( path ) || !boost::filesystem::is_directory( path ) )
		return output;

	boost::filesystem::recursive_directory_iterator fileIterator( path );
	boost::filesystem::recursive_directory_iterator endingIterator;

	// Iterate through the files
	while ( fileIterator != endingIterator )
	{
		// If the current iterator points to a file and the extension matches ...
		if ( boost::filesystem::is_regular_file( *fileIterator ) && fileIterator->path().extension() == fileExtension )
		{
			// Add it to the vector!
			output.push_back( fileIterator->path().string() );
		}

		++fileIterator;
	}

	return output;
}

std::vector<std::string> MainFileManager::GetAllFilesWithExtensionWithSubdirectories( std::string pathToDirectory, std::string fileExtension )
{
	std::vector<std::string> output;
	boost::filesystem::path path( pathToDirectory );

	// Check if the directory exists and is a directory in the first place
	// If not, return the empty vector
	if ( !boost::filesystem::exists( path ) || !boost::filesystem::is_directory( path ) )
		return output;

	boost::filesystem::recursive_directory_iterator fileIterator( path );
	boost::filesystem::recursive_directory_iterator endingIterator;

	// Iterate through the files
	while ( fileIterator != endingIterator )
	{
		// If the current iterator points to a file and the extension matches ...
		if ( boost::filesystem::is_regular_file( *fileIterator ) && fileIterator->path().extension() == fileExtension )
		{
			// Check the meta file if the file should be skipped
			if ( AssetHasMetaFile( *fileIterator, true ) )
			{
				// Check whether to load the file or not.
				if ( CheckIgnorePreload( GetPathToMetaFile( *fileIterator ) ) == false )
				{
					output.push_back( fileIterator->path().string() );
				}
				else
				{
					// Ignoring file
					//printf("Ignoring %s due to meta file...\n", fileIterator->path().filename().string().c_str());
				}
			}
			else
			{
				// No meta file found?
				// Load it in!
				output.push_back( fileIterator->path().string() );
			}
		}
		else if ( boost::filesystem::is_directory( *fileIterator ) )
		{
			// If a subfolder is found: Delve into it and return the goods!
			std::vector<std::string> subFolderFiles = GetAllFilesWithExtensionWithSubdirectories( fileIterator->path().string(), fileExtension );

			// Okay, look at this line right here:
			// output.insert(output.end(), subFolderFiles.begin(), subFolderFiles.end());

			// Now, I know this can end up multiplying my results and I could account for that.
			// But WHY on earth does this work without appending the subfolders to the output?
			// It still works this way! HOW?!
		}

		++fileIterator;
	}

	return output;
}

boost::filesystem::path MainFileManager::GetPathToMetaFile( boost::filesystem::path pathToAsset )
{
	boost::filesystem::path metaPath = pathToAsset.parent_path();
	std::string metaFileName = ".";
	metaFileName.append( pathToAsset.stem().string() );
	metaFileName.append( ".xml" );
	metaPath.append( metaFileName );

	return metaPath;
}

bool MainFileManager::AssetHasMetaFile( boost::filesystem::path pathToAsset, bool createMissingFile )
{
	// Directories (folders) don't need meta files in my engine. Just nod, smile, say yes and move on.
	if ( !boost::filesystem::exists( pathToAsset ) || boost::filesystem::is_directory( pathToAsset ) )
		return true;

	// Get meta file name
	boost::filesystem::path metaPath = GetPathToMetaFile( pathToAsset );

	if ( boost::filesystem::exists( metaPath ) )
		return true;

	if ( !createMissingFile )
		return false;

	return CreateMetaFile( metaPath );
}

bool MainFileManager::CreateMetaFile( boost::filesystem::path filePath )
{
	tinyxml2::XMLDocument newMetaDoc;

	// Let's create the root node, then.
	tinyxml2::XMLNode *rootNode = newMetaDoc.NewElement( "MetaFileInfo" );
	newMetaDoc.InsertFirstChild( rootNode );

	// IgnorePreload tells the FileManager not to add the file to the respective buffer.
	// This can be used for large files like music and backgrounds in order to save on memory.
	tinyxml2::XMLElement *ignorePreloadNode = newMetaDoc.NewElement( "IgnorePreload" );
	ignorePreloadNode->SetText( 0 );

	rootNode->InsertEndChild( ignorePreloadNode );

	tinyxml2::XMLError error = newMetaDoc.SaveFile( filePath.string().c_str() );
	if ( error == tinyxml2::XMLError::XML_SUCCESS )
		return true;

	return false;
}

bool MainFileManager::CheckIgnorePreload( boost::filesystem::path metaFilePath )
{
	tinyxml2::XMLDocument metaFile;
	metaFile.LoadFile( metaFilePath.string().c_str() );

	tinyxml2::XMLElement *ignorePreloadNode = metaFile.FirstChildElement( "MetaFileInfo" )->FirstChildElement( "IgnorePreload" );
	if ( ignorePreloadNode != NULL )
	{
		// Check if content of node is a number or not
		// If it's not a number, treat it as "false"
		int ignorePreloadContent;
		try
		{
			ignorePreloadContent = boost::lexical_cast<int>( ignorePreloadNode->GetText() );
			if ( ignorePreloadContent == 1 )
				return true;
			return false;
		}
		catch ( const boost::bad_lexical_cast )
		{
			// It's not a number.
			return false;
		}
	}

	return false;
}

MainFileManager::MainFileManager()
{
	_isInitialized = false;
	_applicationDirectoryString = "";
}

MainFileManager::~MainFileManager()
{
}

MainFileManager& MainFileManager::GetInstance()
{
	static MainFileManager instance;
	return instance;
}
