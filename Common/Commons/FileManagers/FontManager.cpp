﻿#include "FontManager.hpp"
#include "MainFileManager.hpp"

#include <boost/foreach.hpp>

void FontManager::Initialize()
{
	printf( "-- Loading fonts ...\n" );
	int fontCount = 0;

	// Get path to asset folder.
	std::string pathToAssetFolder = MainFileManager::GetAssetsDirectory();

	// Find all OTFs, TTFs
	std::vector<std::string> allFonts;
	std::vector<std::string> otfsInFolder = MainFileManager::GetAllFilesWithExtensionWithSubdirectories( pathToAssetFolder, ".otf" );
	allFonts.insert( allFonts.end(), otfsInFolder.begin(), otfsInFolder.end() );

	std::vector<std::string> ttfsInFolder = MainFileManager::GetAllFilesWithExtensionWithSubdirectories( pathToAssetFolder, ".ttf" );
	allFonts.insert( allFonts.end(), ttfsInFolder.begin(), ttfsInFolder.end() );

	// Load textures
	BOOST_FOREACH( std::string fontPath, allFonts )
	{
		sf::Font* newFont = new sf::Font();
		if ( !newFont->loadFromFile( fontPath ) )
		{
			// Font could not be found or loaded, go to the next one
			continue;
		}

		std::string fileName = MainFileManager::GetRelativeAssetPathFromFullPath( fontPath );

		// Add font to map
		GetInstance()._loadedFonts.insert_or_assign( fileName, newFont );

		// Get size
		/*FILE *textFile = fopen( texturePath.c_str(), "rb" );
		fseek( textFile, 0, SEEK_END );
		std::size_t size = ftell( textFile );*/

		//LoadedTextureSizes.insert_or_assign( fileName, size );
		printf( "Loaded fonts '%s'\n", fileName.c_str() );

		fontCount++;
	}

	printf( "-- Finished loading fonts: %d of %d fonts loaded!\n", fontCount, allFonts.size() );
}

sf::Font* FontManager::GetFont( std::string fontName )
{
	std::map<std::string, sf::Font*>::iterator i = GetInstance()._loadedFonts.find( fontName );
	if ( i != GetInstance()._loadedFonts.end() )
	{
		return i->second;
	}

	return nullptr;
}

FontManager::FontManager()
{
}

FontManager::~FontManager()
{
}

FontManager& FontManager::GetInstance()
{
	static FontManager instance;
	return instance;
}
