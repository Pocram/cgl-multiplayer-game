#include "MySqlConnection.hpp"

MySqlConnection::MySqlConnection()
{
	_connection = mysql_init( nullptr );
	if ( _connection == nullptr )
	{
		printf( "Init error when intialising MySQL connection!\n" );
		_isInitiated = false;
		return;
	}

	_isInitiated = true;
}

MySqlConnection::~MySqlConnection()
{
	if ( _isInitiated )
	{
		mysql_close( _connection );
	}
}

bool MySqlConnection::ConnectToDatabase(const char* ip, const char* user, const char* password, const char* databaseName, int port)
{
	// Connection needs to be initialised properly.
	if ( _isInitiated == false )
	{
		printf( "Something tried to create to a MySql database, but the connection was not properly initialised!\n" );
		return false;
	}

	// Connect to database.
	bool connectionWasSuccessful = mysql_real_connect( _connection, ip, user, password, databaseName, 0, nullptr, 0 );
	
	// Check if connection was successful.
	if ( connectionWasSuccessful == false )
	{
		printf( "Error when connecting to MySQL databse '%s' at '%s': %s\n", databaseName, ip, mysql_error( _connection ) );
		return false;
	}

	return false;
}

bool MySqlConnection::Query( const char* query )
{
	// Connection needs to be initialised properly.
	if ( _isInitiated == false )
	{
		printf( "Something tried to run a MySql query, but the connection was not properly initialised!\n" );
		return false;
	}

	// Query!
	bool wasQuerySuccessful = mysql_query( _connection, query );

	// Check if query failed
	if ( wasQuerySuccessful == false )
	{
		printf( "Query '%s' failed: %s\n", query, mysql_error( _connection ) );
		return false;
	}

	return true;
}

bool MySqlConnection::ConnectToDatabase(const std::string ip, const std::string user, const std::string password, const std::string databaseName, int port)
{
	return ConnectToDatabase( ip.c_str(), user.c_str(), password.c_str(), databaseName.c_str(), port );
}

bool MySqlConnection::Query(const std::string query)
{
	return Query( query.c_str() );
}