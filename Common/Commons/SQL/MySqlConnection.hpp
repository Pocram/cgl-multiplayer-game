#ifndef COMMONS_SQL_MY_SQL_CONNECTION_H
#define COMMONS_SQL_MY_SQL_CONNECTION_H

#include <mysql.h>
#include <string>

////////////////////////////////////////////////////////////
/// \brief A class that allows the connection to a MySql database.
////////////////////////////////////////////////////////////
class MySqlConnection
{
public:
	// CONSTRUCTORS
	//====================================================================================================

	////////////////////////////////////////////////////////////
	/// \brief Default constructor
	///
	/// Initialises the MySql connection.
	////////////////////////////////////////////////////////////
	MySqlConnection();

	////////////////////////////////////////////////////////////
	/// \brief Destructor
	///
	/// Closes the MySql connection.
	////////////////////////////////////////////////////////////
	~MySqlConnection();


	// METHODS
	//====================================================================================================

	////////////////////////////////////////////////////////////
	/// \brief Establishes a connection with a MySql database
	///
	/// \return Whether connection was successful or not.
	///
	/// \param ip			The MySql server's address.
	/// \param user			The connection's user name.
	/// \param password		The password associated to the user name.
	/// \param databaseName	The name of the database.
	/// \param port			The port to connect to. (Defaults to 0)
	////////////////////////////////////////////////////////////
	bool ConnectToDatabase( const char* ip, const char* user, const char* password, const char* databaseName, int port = 0 );

	////////////////////////////////////////////////////////////
	/// \brief Establishes a connection with a MySql database
	///
	/// \return Whether connection was successful or not.
	///
	/// \param ip			The MySql server's address.
	/// \param user			The connection's user name.
	/// \param password		The password associated to the user name.
	/// \param databaseName	The name of the database.
	/// \param port			The port to connect to. (Defaults to 0)
	////////////////////////////////////////////////////////////
	bool ConnectToDatabase( const std::string ip, const std::string user, const std::string password, const std::string databaseName, int port = 0 );

	////////////////////////////////////////////////////////////
	/// \brief Performs a specific SQL query on the database.
	///
	/// \return Whether query was successful or not.
	///
	/// Requires a connection, see ConnectToDatabase().
	///
	/// \param query The desired SQL query.
	////////////////////////////////////////////////////////////
	bool Query( const char* query );

	////////////////////////////////////////////////////////////
	/// \brief Performs a specific SQL query on the database.
	///
	/// \return Whether query was successful or not.
	///
	/// Requires a connection, see ConnectToDatabase().
	///
	/// \param query The desired SQL query.
	////////////////////////////////////////////////////////////
	bool Query( const std::string query );

private:
	MYSQL* _connection; //!< The connection to the database.
	bool _isInitiated; //!< Whether or not the connection has been initialised properly.
};

#endif // COMMONS_SQL_MY_SQL_CONNECTION_H