#ifndef COMMONS_THREADING_MUTEX_H
#define COMMONS_THREADING_MUTEX_H

#include <windows.h>

////////////////////////////////////////////////////////////
/// \brief A simple wrapper for critical sections. Use these to
/// to make functions thread-safe.
////////////////////////////////////////////////////////////
class Mutex
{
public:
	// CONSTRUCTORS
	//====================================================================================================

	////////////////////////////////////////////////////////////
	/// \brief Default constructor. Initializes the critical section.
	////////////////////////////////////////////////////////////
	Mutex();

	////////////////////////////////////////////////////////////
	/// \brief Leaves and deletes the critical section.
	////////////////////////////////////////////////////////////
	~Mutex();


	// METHODS
	//====================================================================================================

	////////////////////////////////////////////////////////////
	/// \brief Marks the beginning of a thread-safe section
	///
	/// Past this function call only one thread may access the code
	/// at a time.
	/// Use Mutex::Leave() to end the section.
	////////////////////////////////////////////////////////////
	void Enter();

	////////////////////////////////////////////////////////////
	/// \brief Marks the end of a thread-safe section
	///
	/// Allows multiple threads to access the following code
	/// simultaneously.
	/// Use Mutex::Enter() to start the section.
	////////////////////////////////////////////////////////////
	void Leave();


private:
	CRITICAL_SECTION _criticalSection; //!< The critical section (mutex) itself.
};

#endif // COMMONS_THREADING_MUTEX_H