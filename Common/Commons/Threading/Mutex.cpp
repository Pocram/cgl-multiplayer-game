#include "Mutex.hpp"

Mutex::Mutex()
{
	::InitializeCriticalSection( &_criticalSection );
}

Mutex::~Mutex()
{
	::LeaveCriticalSection( &_criticalSection );
	::DeleteCriticalSection( &_criticalSection );
}

void Mutex::Enter()
{
	::EnterCriticalSection( &_criticalSection );
}

void Mutex::Leave()
{
	::LeaveCriticalSection( &_criticalSection );
}