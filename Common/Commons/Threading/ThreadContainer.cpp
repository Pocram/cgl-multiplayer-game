#include "ThreadContainer.hpp"

ThreadContainer::ThreadContainer()
{
	_threadId = 0;
	_threadHandle = nullptr;
}

ThreadContainer::~ThreadContainer()
{
}

void ThreadContainer::StartThread()
{
	_threadHandle = ::CreateThread( nullptr, 0, static_cast<LPTHREAD_START_ROUTINE>(ThreadFunction), this, 0, &_threadId );
}

void ThreadContainer::Terminate()
{
	::TerminateThread( _threadHandle, 0 );
}

void ThreadContainer::ThreadMain()
{
	// Overridden by the child class.
}

DWORD ThreadContainer::ThreadFunction(LPVOID lpVoid)
{
	ThreadContainer* thread = static_cast<ThreadContainer*>(lpVoid);
	thread->ThreadMain();
	return 0;
}