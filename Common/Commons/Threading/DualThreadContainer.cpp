﻿#include "DualThreadContainer.hpp"
#include <cstdio>

DualThreadContainer::DualThreadContainer()
{
	_threadIdA = 0;
	_threadIdB = 0;
	_threadHandleA = nullptr;
	_threadHandleB = nullptr;
}

DualThreadContainer::~DualThreadContainer()
{
}

void DualThreadContainer::StartThreadA()
{
	_threadHandleA = ::CreateThread( nullptr, 0, static_cast<LPTHREAD_START_ROUTINE>( ThreadFunctionA ), this, 0, &_threadIdA );
}

void DualThreadContainer::StartThreadB()
{
	_threadHandleB = ::CreateThread( nullptr, 0, static_cast<LPTHREAD_START_ROUTINE>( ThreadFunctionB ), this, 0, &_threadIdB );
}

void DualThreadContainer::TerminateA()
{
	::TerminateThread( _threadHandleA, 0 );
}

void DualThreadContainer::TerminateB()
{
	::TerminateThread( _threadHandleB, 0 );
}

void DualThreadContainer::ThreadMainA()
{
	// Overridden by the child class.
}

void DualThreadContainer::ThreadMainB()
{
	// Overridden by the child class.
}

DWORD DualThreadContainer::ThreadFunctionA( LPVOID lpVoid )
{
	DualThreadContainer* thread = static_cast<DualThreadContainer*>( lpVoid );
	thread->ThreadMainA();
	return 0;
}

DWORD DualThreadContainer::ThreadFunctionB( LPVOID lpVoid )
{
	DualThreadContainer* thread = static_cast<DualThreadContainer*>( lpVoid );
	thread->ThreadMainB();
	return 0;
}
