#ifndef COMMONS_THREADING_THREAD_CONTAINER_H
#define COMMONS_THREADING_THREAD_CONTAINER_H

#include <windows.h>


////////////////////////////////////////////////////////////
/// \brief An abstract class that contains a thread.
///
/// This class can be used as a base for a worker class
/// dedicated to a task that requires a thread.
/// Note that the StartThread() method is protected. This is due to
/// the fact that some classes manage their threads internally.
////////////////////////////////////////////////////////////
class ThreadContainer
{
public:
	////////////////////////////////////////////////////////////
	/// \brief Default constructor.
	////////////////////////////////////////////////////////////
	ThreadContainer();

	////////////////////////////////////////////////////////////
	/// \brief Default destructor.
	////////////////////////////////////////////////////////////
	virtual ~ThreadContainer();


protected:
	// METHODS
	//====================================================================================================

	////////////////////////////////////////////////////////////
	/// \brief Starts the thread.
	///
	/// The thread is started and executes ThreadMain()
	/// in the background until its completion.
	////////////////////////////////////////////////////////////
	void StartThread();

	////////////////////////////////////////////////////////////
	/// \brief Forcefully ends the thread
	///
	/// Forcefully ends the thread. In General, you should try
	/// to avoid using this function and let the thread return
	/// on its own.
	////////////////////////////////////////////////////////////
	void Terminate();

	////////////////////////////////////////////////////////////
	/// \brief The thread's main method.
	///
	/// This abstract method is overridden by any child class and
	/// used as the thread's main method.
	////////////////////////////////////////////////////////////
	virtual void ThreadMain();


private:
	// METHODS
	//====================================================================================================

	////////////////////////////////////////////////////////////
	/// \brief The main thread starter function
	///
	/// This function is called whenever a new thread is created.
	/// A pointer to this class is passed through the lpVoid parameter.
	////////////////////////////////////////////////////////////
	static DWORD WINAPI ThreadFunction( LPVOID lpVoid );

	// MEMBERS
	//====================================================================================================
	DWORD _threadId; //!< The thread's ID.
	HANDLE _threadHandle; //!< The thread's handle.

};

#endif // COMMONS_THREADING_THREAD_CONTAINER_H