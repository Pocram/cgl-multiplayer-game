﻿#ifndef COMMONS_THREADING_DUAL_THREAD_CONTAINER_H
#define COMMONS_THREADING_DUAL_THREAD_CONTAINER_H

#include <windows.h>

////////////////////////////////////////////////////////////
/// \brief An abstract class that contains two threads.
///
/// This class can be used as a base for a worker class
/// dedicated to a task that requires two threads.
/// Note that the StartThread() methods are protected. This is due to
/// the fact that some classes need to manage their threads internally.
////////////////////////////////////////////////////////////
class DualThreadContainer
{
public:
	////////////////////////////////////////////////////////////
	/// \brief Default constructor.
	////////////////////////////////////////////////////////////
	DualThreadContainer();

	////////////////////////////////////////////////////////////
	/// \brief Default destructor.
	////////////////////////////////////////////////////////////
	virtual ~DualThreadContainer();


protected:
	////////////////////////////////////////////////////////////
	/// \brief Starts the first thread.
	///
	/// The thread is started and executes ThreadMainA()
	/// in the background until its completion.
	////////////////////////////////////////////////////////////
	void StartThreadA();

	////////////////////////////////////////////////////////////
	/// \brief Starts the second thread.
	///
	/// The thread is started and executes ThreadMainB()
	/// in the background until its completion.
	////////////////////////////////////////////////////////////
	void StartThreadB();

	////////////////////////////////////////////////////////////
	/// \brief Forcefully ends the first thread.
	///
	/// Forcefully ends the thread. In General, you should try
	/// to avoid using this function and let the thread return
	/// on its own.
	////////////////////////////////////////////////////////////
	void TerminateA();

	////////////////////////////////////////////////////////////
	/// \brief Forcefully ends the second thread.
	///
	/// Forcefully ends the thread. In General, you should try
	/// to avoid using this function and let the thread return
	/// on its own.
	////////////////////////////////////////////////////////////
	void TerminateB();

	////////////////////////////////////////////////////////////
	/// \brief The first thread's main method.
	///
	/// This abstract method is overridden by any child class and
	/// used as the thread's main method.
	////////////////////////////////////////////////////////////
	virtual void ThreadMainA();

	////////////////////////////////////////////////////////////
	/// \brief The second thread's main method.
	///
	/// This abstract method is overridden by any child class and
	/// used as the thread's main method.
	////////////////////////////////////////////////////////////
	virtual void ThreadMainB();


private:
	////////////////////////////////////////////////////////////
	/// \brief The first main thread starter function.
	///
	/// This function is called whenever a new thread is created.
	/// A pointer to this class is passed through the lpVoid parameter.
	////////////////////////////////////////////////////////////
	static DWORD WINAPI ThreadFunctionA( LPVOID lpVoid );

	////////////////////////////////////////////////////////////
	/// \brief The second main thread starter function.
	///
	/// This function is called whenever a new thread is created.
	/// A pointer to this class is passed through the lpVoid parameter.
	////////////////////////////////////////////////////////////
	static DWORD WINAPI ThreadFunctionB( LPVOID lpVoid );

	// MEMBERS
	//====================================================================================================
	DWORD _threadIdA;		//!< The first thread's ID.
	DWORD _threadIdB;		//!< The second thread's ID.
	HANDLE _threadHandleA;	//!< The first thread's handle.
	HANDLE _threadHandleB;	//!< The second thread's handle.
};

#endif // COMMONS_THREADING_DUAL_THREAD_CONTAINER_H