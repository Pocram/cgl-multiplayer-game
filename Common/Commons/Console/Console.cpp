﻿#include "Console.hpp"
#include <sstream>

Debug::Console::Console()
{
#ifdef WIN32
	AllocConsole();

	_consoleOutHandle = ::GetStdHandle( STD_OUTPUT_HANDLE );
#endif
#ifdef __APPLE__
	initscr();
	refresh();

	keypad( stdscr, TRUE );
#endif

	_defaultConsoleColor = GetCurrentColor();
}

Debug::Console::~Console()
{
#ifdef WIN32
	FreeConsole();
#endif
#ifdef __APPLE__
	endwin();
#endif
}

Debug::Console& Debug::Console::GetInstance()
{
	static Console instance;
	return instance;
}

void Debug::Console::WriteLine( std::string message )
{
#ifdef WIN32
	printf( "%s\n", message.c_str() );
#endif
#ifdef __APPLE__
	addstr( string.c_str() );
#endif
}

void Debug::Console::WriteLine( std::string message, const char* functionName, const char* fileName, const int lineNumber )
{
	ResetColor();
	printf( "%s ", message.c_str() );
	SetColor( Debug::ConsoleColor::DarkGray, Debug::ConsoleColor::Black );
	printf( "(from " );
	SetColor( Debug::ConsoleColor::Gray, Debug::ConsoleColor::Black );
	printf( "%s ", functionName );
	SetColor( Debug::ConsoleColor::DarkGray, Debug::ConsoleColor::Black );
	printf( "on line " );
	SetColor( Debug::ConsoleColor::Gray, Debug::ConsoleColor::Black );
	printf( "%d ", lineNumber );
	SetColor( Debug::ConsoleColor::DarkGray, Debug::ConsoleColor::Black );
	printf( "in " );
	SetColor( Debug::ConsoleColor::Gray, Debug::ConsoleColor::Black );
	printf( "%s", fileName );
	SetColor( Debug::ConsoleColor::DarkGray, Debug::ConsoleColor::Black );
	printf( ")\n" );
	ResetColor();
}

void Debug::Console::WriteWarning( std::string message )
{
	SetColor( Debug::ConsoleColor::Yellow, Debug::ConsoleColor::Black );
	WriteLine( "WARNING: " + message );
	ResetColor();
}

void Debug::Console::WriteWarning( std::string message, const char* functionName, const char* fileName, const int lineNumber )
{
	SetColor( Debug::ConsoleColor::Yellow, Debug::ConsoleColor::Black );
	printf( "%s ", message.c_str() );
	SetColor( Debug::ConsoleColor::DarkGray, Debug::ConsoleColor::Black );
	printf( "(from " );
	SetColor( Debug::ConsoleColor::Gray, Debug::ConsoleColor::Black );
	printf( "%s ", functionName );
	SetColor( Debug::ConsoleColor::DarkGray, Debug::ConsoleColor::Black );
	printf( "on line " );
	SetColor( Debug::ConsoleColor::Gray, Debug::ConsoleColor::Black );
	printf( "%d ", lineNumber );
	SetColor( Debug::ConsoleColor::DarkGray, Debug::ConsoleColor::Black );
	printf( "in " );
	SetColor( Debug::ConsoleColor::Gray, Debug::ConsoleColor::Black );
	printf( "%s", fileName );
	SetColor( Debug::ConsoleColor::DarkGray, Debug::ConsoleColor::Black );
	printf( ")\n" );
	ResetColor();
}

void Debug::Console::WriteError( std::string message )
{
	SetColor( Debug::ConsoleColor::Red, Debug::ConsoleColor::Black );
	WriteLine( "ERROR: " + message );
	ResetColor();
}

void Debug::Console::WriteError( std::string message, const char* functionName, const char* fileName, const int lineNumber )
{
	SetColor( Debug::ConsoleColor::Red, Debug::ConsoleColor::Black );
	printf( "%s ", message.c_str() );
	SetColor( Debug::ConsoleColor::DarkGray, Debug::ConsoleColor::Black );
	printf( "(from " );
	SetColor( Debug::ConsoleColor::Gray, Debug::ConsoleColor::Black );
	printf( "%s ", functionName );
	SetColor( Debug::ConsoleColor::DarkGray, Debug::ConsoleColor::Black );
	printf( "on line " );
	SetColor( Debug::ConsoleColor::Gray, Debug::ConsoleColor::Black );
	printf( "%d ", lineNumber );
	SetColor( Debug::ConsoleColor::DarkGray, Debug::ConsoleColor::Black );
	printf( "in " );
	SetColor( Debug::ConsoleColor::Gray, Debug::ConsoleColor::Black );
	printf( "%s", fileName );
	SetColor( Debug::ConsoleColor::DarkGray, Debug::ConsoleColor::Black );
	printf( ")\n" );
	ResetColor();
}

int Debug::Console::GetAmountOfCharsInConsoleBuffer()
{
	COORD maxCoordOfConsole = GetLargestConsoleWindowSize( _consoleOutHandle );
	return maxCoordOfConsole.X * maxCoordOfConsole.Y;
}

void Debug::Console::ClearConsole()
{
	FillConsole( ' ' );
}

void Debug::Console::FillConsole( char fillChar )
{
	int amountOfCharactersToFill = GetAmountOfCharsInConsoleBuffer();
	unsigned long *numberOfCharsWritten = static_cast<unsigned long*>( malloc( sizeof( unsigned long ) ) );
	FillConsoleOutputCharacter( _consoleOutHandle, fillChar, amountOfCharactersToFill, { 0,0 }, numberOfCharsWritten );
	free( numberOfCharsWritten );
}

void Debug::Console::SetColor( Debug::ConsoleColorPair color )
{
	SetConsoleTextAttribute( _consoleOutHandle, color.GetColorAttributeValue() );
}

void Debug::Console::SetColor( Debug::ConsoleColor foregroundColor, Debug::ConsoleColor backgroundColor )
{
	ConsoleColorPair newColor( foregroundColor, backgroundColor );
	SetColor( newColor );
}

void Debug::Console::ResetColor()
{
	SetColor( _defaultConsoleColor );
}

Debug::ConsoleColorPair Debug::Console::GetCurrentColor()
{
	CONSOLE_SCREEN_BUFFER_INFO consoleScreenBufferInfo;

	if ( GetConsoleScreenBufferInfo( _consoleOutHandle, &consoleScreenBufferInfo ) )
	{
		int attr = consoleScreenBufferInfo.wAttributes;
		Debug::ConsoleColor backgroundColor = static_cast<Debug::ConsoleColor>( attr & 0xF0 );
		Debug::ConsoleColor foregroundColor = static_cast<Debug::ConsoleColor>( attr & 0x0F );

		return Debug::ConsoleColorPair( foregroundColor, backgroundColor );
	}

	return Debug::ConsoleColorPair( Debug::ConsoleColor::Black, Debug::ConsoleColor::Black );
}
