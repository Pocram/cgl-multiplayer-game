﻿#ifndef COMMONS_CONSOLE_CONSOLE_H
#define COMMONS_CONSOLE_CONSOLE_H

#include <Commons/Console/ConsoleColorPair.hpp>
#include <string>

#ifdef WIN32
#include <windows.h>
#endif
#ifdef __APPLE__
#include <curses.h>
#endif

#define __FILENAME__ (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)

#define WriteLineWithCaller(message)						\
{															\
	Debug::Console::GetInstance().							\
	WriteLine( message, __func__, __FILENAME__, __LINE__ );	\
}

#define WriteWarningWithCaller(message)							\
{																\
	Debug::Console::GetInstance().								\
	WriteWarning( message, __func__, __FILENAME__, __LINE__ );	\
}

#define WriteErrorWithCaller(message)							\
{																\
	Debug::Console::GetInstance().								\
	WriteError( message, __func__, __FILENAME__, __LINE__ );	\
}

////////////////////////////////////////////////////////////
/// \brief Debugging and console related methods.
////////////////////////////////////////////////////////////
namespace Debug
{
	////////////////////////////////////////////////////////////
	/// \brief An interface for using the output console.
	///
	/// This class is used as a singleton. To get the instance use Debug::Console::GetInstance().
	////////////////////////////////////////////////////////////
	class Console
	{
	public:
		Console( Console const& ) = delete;
		void operator = ( Console const& ) = delete;

		// METHODS
		//====================================================================================================
		////////////////////////////////////////////////////////////
		/// \brief Gets a reference to the console instance.
		///
		/// Use this method to get a reference to the console before using
		/// other methods.
		////////////////////////////////////////////////////////////
		static Console& GetInstance();

		////////////////////////////////////////////////////////////
		/// \brief Writes a line of text to the console.
		/// 
		/// A linebreak character is automatically added to the end of the message.
		///
		/// \param message The message to be sent to the console window.
		////////////////////////////////////////////////////////////
		void WriteLine( std::string message );

		////////////////////////////////////////////////////////////
		/// \brief Writes a line of text to the console.
		/// 
		/// A linebreak character is automatically added to the end of the message.
		///
		/// \param message		The message to be sent to the console window.
		/// \param functionName	The name of the function that called this. Used by WriteLineWithCaller().
		/// \param fileName		The name of the file that called this. Used by WriteLineWithCaller().
		/// \param lineNumber	The line number of where the method was called from. Used by WriteLineWithCaller().
		////////////////////////////////////////////////////////////
		void WriteLine( std::string message, const char* functionName, const char* fileName, const int lineNumber );

		////////////////////////////////////////////////////////////
		/// \brief Writes a line of text to the console.
		/// 
		/// A warning message is automatically highlighted in a yellow tint.
		/// A linebreak character is automatically added to the end of the message.
		///
		/// \param message The message to be sent to the console window.
		////////////////////////////////////////////////////////////
		void WriteWarning( std::string message );

		////////////////////////////////////////////////////////////
		/// \brief Writes a line of text to the console.
		/// 
		/// A warning message is automatically highlighted in a yellow tint.
		/// A linebreak character is automatically added to the end of the message.
		///
		/// \param message		The message to be sent to the console window.
		/// \param functionName	The name of the function that called this. Used by WriteWarningWithCaller().
		/// \param fileName		The name of the file that called this. Used by WriteWarningWithCaller().
		/// \param lineNumber	The line number of where the method was called from. Used by WriteWarningWithCaller().
		////////////////////////////////////////////////////////////
		void WriteWarning( std::string message, const char* functionName, const char* fileName, const int lineNumber );

		////////////////////////////////////////////////////////////
		/// \brief Writes a line of text to the console.
		/// 
		/// A warning message is automatically highlighted in a red tint.
		/// A linebreak character is automatically added to the end of the message.
		///
		/// \param message The message to be sent to the console window.
		////////////////////////////////////////////////////////////
		void WriteError( std::string message );

		////////////////////////////////////////////////////////////
		/// \brief Writes a line of text to the console.
		/// 
		/// A warning message is automatically highlighted in a red tint.
		/// A linebreak character is automatically added to the end of the message.
		///
		/// \param message The message to be sent to the console window.
		/// \param functionName	The name of the function that called this. Used by WriteErrorWithCaller().
		/// \param fileName		The name of the file that called this. Used by WriteErrorWithCaller().
		/// \param lineNumber	The line number of where the method was called from. Used by WriteErrorWithCaller().
		////////////////////////////////////////////////////////////
		void WriteError( std::string message, const char* functionName, const char* fileName, const int lineNumber );

		////////////////////////////////////////////////////////////
		/// \brief Clears the console window.
		////////////////////////////////////////////////////////////
		void ClearConsole();

		////////////////////////////////////////////////////////////
		/// \brief Fills the console with characters.
		///
		/// Fills the console window with the same character in every cell.
		/// Like. All of them.
		///
		/// \param fillChar The character to fill the console with.
		////////////////////////////////////////////////////////////
		void FillConsole( char fillChar );

		////////////////////////////////////////////////////////////
		/// \brief Gets the console's current color.
		///
		/// \returns Returns the foreground and background color of the console.
		////////////////////////////////////////////////////////////
		Debug::ConsoleColorPair GetCurrentColor();

		////////////////////////////////////////////////////////////
		/// \brief Sets the writing color of the console.
		///
		/// Keep in mind that this function does not change the whole console's color.
		/// It does however change the color of the next written text.
		///
		/// \param color The foreground and background color of the console.
		////////////////////////////////////////////////////////////
		void SetColor( Debug::ConsoleColorPair color );

		////////////////////////////////////////////////////////////
		/// \brief Sets the writing color of the console.
		///
		/// Keep in mind that this function does not change the whole console's color.
		/// It does however change the color of the next written text.
		///
		/// \param foregroundColor The foreground color of the console.
		/// \param backgroundColor The background color of the console.
		////////////////////////////////////////////////////////////
		void SetColor( Debug::ConsoleColor foregroundColor, Debug::ConsoleColor backgroundColor );

		////////////////////////////////////////////////////////////
		/// \brief Resets the console color to its default value.
		////////////////////////////////////////////////////////////
		void ResetColor();

	private:
		// CONSTRUCTORS
		//====================================================================================================
		Console();
		~Console();

		// METHODS
		//====================================================================================================
		int GetAmountOfCharsInConsoleBuffer();

		// MEMBERS
		//====================================================================================================
		Debug::ConsoleColorPair _defaultConsoleColor;	//!< The console's default color. Determined when initialized.
#ifdef WIN32
		HANDLE _consoleOutHandle;						//!< The output handle of the console.
#endif
	};
}

#endif // COMMONS_CONSOLE_CONSOLE_H