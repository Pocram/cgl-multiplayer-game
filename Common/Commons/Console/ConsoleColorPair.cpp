﻿#include "ConsoleColorPair.hpp"

Debug::ConsoleColorPair::ConsoleColorPair()
{
	_colorAttributeValue = 0;
}

Debug::ConsoleColorPair::ConsoleColorPair( Debug::ConsoleColor foregroundColor, Debug::ConsoleColor backgroundColor )
{
	int newColor = ( ( backgroundColor & 0x0F ) << 4 ) | ( foregroundColor & 0x0F );
	_colorAttributeValue = newColor;
}

Debug::ConsoleColorPair::~ConsoleColorPair()
{
}

int Debug::ConsoleColorPair::GetColorAttributeValue()
{
	return _colorAttributeValue;
}