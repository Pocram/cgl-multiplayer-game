﻿#ifndef COMMONS_CONSOLE_CONSOLE_COLOR_PAIR_H
#define COMMONS_CONSOLE_CONSOLE_COLOR_PAIR_H

namespace Debug
{
	////////////////////////////////////////////////////////////
	/// \brief An enum that associates windows console colour IDs with their names.
	////////////////////////////////////////////////////////////
	enum ConsoleColor
	{
		Black		= 0,
		DarkBlue	= 1,
		DarkGreen	= 2,
		DarkCyan	= 3,
		DarkRed		= 4,
		DarkMagenta	= 5,
		DarkYellow	= 6,
		Gray		= 7,
		DarkGray	= 8,
		Blue		= 9,
		Green		= 10,
		Cyan		= 11,
		Red			= 12,
		Magenta		= 13,
		Yellow		= 14,
		White		= 15
	};

	////////////////////////////////////////////////////////////
	/// \brief A pair of windows console colours.
	///
	/// The wiondows console uses a single value that contains both the background and foreground colour.
	/// The background colour is stored in the lower byte and the foreground colour in the upper one.
	///
	/// \see Debug::ConsoleColor
	////////////////////////////////////////////////////////////
	class ConsoleColorPair
	{
	public:
		// CONSTRUCTORS
		//====================================================================================================
		////////////////////////////////////////////////////////////
		/// \brief Default constructor.
		///
		/// If you can, use Debug::ConsoleColorPair::ConsoleColorPair( Debug::ConsoleColor foregroundColor, Debug::ConsoleColor backgroundColor ) instead.
		////////////////////////////////////////////////////////////
		ConsoleColorPair();

		////////////////////////////////////////////////////////////
		/// \brief Main constructor. Calculates the final colour value.
		///
		/// \param foregroundColor The console color id of the foreground color. \see Debug::ConsoleColor
		/// \param backgroundColor The console color id of the background color. \see Debug::ConsoleColor
		////////////////////////////////////////////////////////////
		ConsoleColorPair( Debug::ConsoleColor foregroundColor, Debug::ConsoleColor backgroundColor );

		////////////////////////////////////////////////////////////
		/// \brief Default destructor.
		////////////////////////////////////////////////////////////
		~ConsoleColorPair();

		// METHODS
		//====================================================================================================
		////////////////////////////////////////////////////////////
		/// \brief Get the combined colour value for the console.
		///
		/// The wiondows console uses a single value that contains both the background and foreground colour.
		/// The background colour is stored in the lower byte and the foreground colour in the upper one.
		///
		/// \returns The combined colour value for the console.
		////////////////////////////////////////////////////////////
		int GetColorAttributeValue();


	private:
		// MEMBERS
		//====================================================================================================
		int _colorAttributeValue;	//!< The calculated colour attribute.
	};
}

#endif // COMMONS_CONSOLE_CONSOLE_COLOR_PAIR_H