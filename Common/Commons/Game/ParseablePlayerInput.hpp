﻿#ifndef COMMONS_GAME_PARSEABLE_PLAYER_INPUT_H
#define COMMONS_GAME_PARSEABLE_PLAYER_INPUT_H

#include <string>

#include <SFML/Graphics.hpp>

class ParseablePlayerInput
{
public:
	ParseablePlayerInput();
	ParseablePlayerInput( sf::Vector2f movementInputLeft, sf::Vector2f movementInputRight, bool isSprintingLeft, bool isSprintingRight );

	sf::Vector2f MovementInputLeft;
	sf::Vector2f MovementInputRight;
	bool IsSprintingLeft;
	bool IsSprintingRight;

	std::string ParseToString() const;
	static bool ParseFromString( const std::string& input, ParseablePlayerInput& outPlayerInput );

	bool IsEmpty() const;


	friend bool operator==( const ParseablePlayerInput& lhs, const ParseablePlayerInput& rhs );
	friend bool operator!=( const ParseablePlayerInput& lhs, const ParseablePlayerInput& rhs );

	friend ParseablePlayerInput operator +( const ParseablePlayerInput& lhs, const ParseablePlayerInput& rhs );
};

#endif // COMMONS_GAME_PARSEABLE_PLAYER_INPUT_H