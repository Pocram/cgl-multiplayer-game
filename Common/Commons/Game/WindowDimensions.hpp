﻿#ifndef COMMONS_GAME_WINDOW_DIMENSIONS_H
#define COMMONS_GAME_WINDOW_DIMENSIONS_H

struct WindowDimensions
{
	int WindowWidth;
	int WindowHeight;
};

#endif // COMMONS_GAME_WINDOW_DIMENSIONS_H