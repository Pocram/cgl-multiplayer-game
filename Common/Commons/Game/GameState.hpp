﻿#ifndef COMMONS_GAME_GAME_STATE_H
#define COMMONS_GAME_GAME_STATE_H

#include <Commons/Game/Interfaces/IUpdateable.hpp>

class GameState : public IUpdateable
{
public:
	GameState();
	~GameState();

	virtual void Update( sf::Time elapsedTime ) override;

protected:
	virtual void OnStateEnter();
	virtual void OnStateExit();
};

#endif // COMMONS_GAME_GAME_STATE_H