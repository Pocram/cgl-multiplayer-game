﻿#include "ParseablePlayerInput.hpp"
#include <sstream>
#include <Commons/Utility/Conversions.hpp>
#include <Commons/Utility/Parsing.hpp>
#include <Commons/Math/SfmlVectorMath.hpp>

ParseablePlayerInput::ParseablePlayerInput()
{
	MovementInputLeft = sf::Vector2f( 0.0f, 0.0f );
	MovementInputRight = sf::Vector2f( 0.0f, 0.0f );
	IsSprintingLeft = false;
	IsSprintingRight = false;
}

ParseablePlayerInput::ParseablePlayerInput( sf::Vector2f movementInputLeft, sf::Vector2f movementInputRight, bool isSprintingLeft, bool isSprintingRight )
{
	MovementInputLeft = movementInputLeft;
	IsSprintingLeft = isSprintingLeft;

	MovementInputRight = movementInputRight;
	IsSprintingRight = isSprintingRight;
}

std::string ParseablePlayerInput::ParseToString() const
{
	// Parsed output example: playerInput{1.0;0.125;-0.25;0;1}

	std::stringstream output;
	output << "playerInput{" << MovementInputLeft.x << ";" << MovementInputLeft.y << ";" << MovementInputRight.x << ";" << MovementInputRight.y << ";" << IsSprintingLeft << ";" << IsSprintingRight << "}";
	return output.str();
}

bool ParseablePlayerInput::ParseFromString( const std::string& input, ParseablePlayerInput& outPlayerInput )
{
	std::vector<std::string> listOfElements = Parsing::FromStringToVector( input, "playerInput" );
	if( listOfElements.size() <= 0 )
	{
		return false;
	}

	float movementInputLeftX = Conversions::StringToNumber<float>( listOfElements[0] );
	float movementInputLeftY = Conversions::StringToNumber<float>( listOfElements[1] );
	float movementInputRightX = Conversions::StringToNumber<float>( listOfElements[2] );
	float movementInputRightY = Conversions::StringToNumber<float>( listOfElements[3] );
	bool isSprintingLeft = Conversions::StringToNumber<bool>( listOfElements[4] );
	bool isSprintingRight = Conversions::StringToNumber<bool>( listOfElements[5] );

	outPlayerInput.MovementInputLeft = sf::Vector2f( movementInputLeftX, movementInputLeftY );
	outPlayerInput.MovementInputRight = sf::Vector2f( movementInputRightX, movementInputRightY );
	outPlayerInput.IsSprintingLeft = isSprintingLeft;
	outPlayerInput.IsSprintingRight = isSprintingRight;

	return true;
}

bool ParseablePlayerInput::IsEmpty() const
{
	return MovementInputLeft == sf::Vector2f( 0.0f, 0.0f ) 
		&& MovementInputRight == MovementInputLeft 
		&& IsSprintingLeft == false 
		&& IsSprintingRight == false;
}

bool operator==( const ParseablePlayerInput& lhs, const ParseablePlayerInput& rhs )
{
	return lhs.MovementInputLeft == rhs.MovementInputLeft
			&& lhs.MovementInputRight == rhs.MovementInputRight
			&& lhs.IsSprintingLeft == rhs.IsSprintingLeft
			&& lhs.IsSprintingRight == rhs.IsSprintingRight;
}

bool operator!=( const ParseablePlayerInput& lhs, const ParseablePlayerInput& rhs )
{
	return !( lhs == rhs );
}

ParseablePlayerInput operator+( const ParseablePlayerInput& lhs, const ParseablePlayerInput& rhs )
{
	ParseablePlayerInput output;
	output.MovementInputLeft = lhs.MovementInputLeft + rhs.MovementInputLeft;
	output.MovementInputLeft = sf::VectorMath::Magnitude2f( output.MovementInputLeft ) > 1.0f ? sf::VectorMath::GetNormalized( output.MovementInputLeft ) : output.MovementInputLeft;
	output.MovementInputRight = lhs.MovementInputRight + rhs.MovementInputRight;
	output.MovementInputRight = sf::VectorMath::Magnitude2f( output.MovementInputRight ) > 1.0f ? sf::VectorMath::GetNormalized( output.MovementInputRight ) : output.MovementInputRight;
	output.IsSprintingLeft = lhs.IsSprintingLeft || rhs.IsSprintingLeft;
	output.IsSprintingRight = lhs.IsSprintingRight || rhs.IsSprintingRight;
	return output;
}
