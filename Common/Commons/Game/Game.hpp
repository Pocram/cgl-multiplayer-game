﻿#ifndef COMMONS_GAME_GAME_H
#define COMMONS_GAME_GAME_H

#include <Windows.h>
#include <string>

#include <SFML/Graphics.hpp>

#include "GameState.hpp"
#include <Commons/Game/WindowDimensions.hpp>
#include "GameScene.hpp"

////////////////////////////////////////////////////////////
/// \brief The game's main class.
///
/// This class contains the game's core logic.
/// In order to start the game, call Game::Start().
////////////////////////////////////////////////////////////
class Game
{
public:
	Game( Game const& ) = delete;
	void operator = ( Game const& ) = delete;

	// METHODS
	//====================================================================================================
	////////////////////////////////////////////////////////////
	/// \brief Gets a reference to the game instance.
	///
	/// Use this method to get a reference to the game before using
	/// other methods.
	////////////////////////////////////////////////////////////
	static Game& GetInstance();

	////////////////////////////////////////////////////////////
	/// \brief Starts the game.
	///
	/// Shows the game's main window and console.
	/// Starts the game loop.
	////////////////////////////////////////////////////////////
	void Start();

	////////////////////////////////////////////////////////////
	/// \brief Quits the game.
	///
	/// Closes the game windows and stops the game loop.
	////////////////////////////////////////////////////////////
	void Quit();

	////////////////////////////////////////////////////////////
	/// \brief Gets a const reference to the game's main window.
	////////////////////////////////////////////////////////////
	const sf::RenderWindow& GetGameWindow();

	////////////////////////////////////////////////////////////
	/// \brief Gets a const reference to whether the game is running or not.
	////////////////////////////////////////////////////////////
	const bool& GetGameIsRunning();

	////////////////////////////////////////////////////////////
	/// \brief Gets a const reference to the game's current state.
	////////////////////////////////////////////////////////////
	const GameState& GetCurrentGameState();

	////////////////////////////////////////////////////////////
	/// \brief Sets the game's target framerate.
	///
	/// \param framerate The target framerate in frames per second.
	////////////////////////////////////////////////////////////
	void SetTargetFramerate( int framerate );

	////////////////////////////////////////////////////////////
	/// \brief Sets whether the game's vsyncv is enable or not.
	///
	/// Enabling vsynch limits the game's frames per second to the
	/// maximum supported speed of the monitor.
	///
	/// \param enabled Whether to enable vsynch or not.
	////////////////////////////////////////////////////////////
	void SetEnableVSynch( bool enabled );

	////////////////////////////////////////////////////////////
	/// \brief Gets the current framerate the game is running at.
	///
	/// Not to be confused with the value used in Game::SetTargetFrameRate().
	/// This method returns the framerate the game is actually running at, not the target.
	///
	/// \returns The current framerate in frames per second.
	////////////////////////////////////////////////////////////
	float GetCurrentFramerate();
	
	////////////////////////////////////////////////////////////
	/// \brief Sets the game's name.
	///
	/// The game's name will appear in the main window's title as well
	/// as the console's title.
	///
	/// \param The new name.
	////////////////////////////////////////////////////////////
	void SetGameName( std::string newName );

	////////////////////////////////////////////////////////////
	/// \brief Sets the main window's dimensions.
	///
	/// \param width	The new width in pixels.
	/// \param height	The new height in pixels.
	////////////////////////////////////////////////////////////
	void SetWindowDimensions( int width, int height );

	////////////////////////////////////////////////////////////
	/// \brief Gets the window dimensions in pixels.
	////////////////////////////////////////////////////////////
	WindowDimensions GetWindowDimensions();

	////////////////////////////////////////////////////////////
	/// \brief Sets the main window's dimensions.
	///
	/// The new height will be determined by the width and aspect ratio.
	///
	/// \param width		The new width in pixels.
	/// \param aspectRatio	The new aspect ratio multiplier. E.g.: `16.0f/9.0f`, or `1.778`.
	////////////////////////////////////////////////////////////
	void SetWindowDimensionsByWidth( int width, float aspectRatio );

	////////////////////////////////////////////////////////////
	/// \brief Sets the main window's dimensions.
	///
	/// The new width will be determined by the height and aspect ratio.
	///
	/// \param height		The new height in pixels.
	/// \param aspectRatio	The new aspect ratio multiplier. E.g.: `16.0f/9.0f`, or `1.778`.
	////////////////////////////////////////////////////////////
	void SetWindowDimensionsByHeight( int height, float aspectRatio );

	////////////////////////////////////////////////////////////
	/// \brief Loads a new scene.
	///
	/// Loads a new scene after the current game loop iteration has ended.
	///
	/// \param newScene	A pointer to the new scene.
	////////////////////////////////////////////////////////////
	void LoadScene( GameScene* newScene );

	////////////////////////////////////////////////////////////
	/// \brief Returns a pointer to the current scene.
	////////////////////////////////////////////////////////////
	GameScene* GetCurrentScene();

private:
	// CONSTRUCTORS
	//====================================================================================================
	////////////////////////////////////////////////////////////
	/// \brief Default constructor.
	////////////////////////////////////////////////////////////
	Game();

	// METHODS
	//====================================================================================================
	////////////////////////////////////////////////////////////
	/// \brief Updates and draws the game over one iteration.
	///
	/// Updates the game up to five times and draws the current
	/// state to the screen.
	////////////////////////////////////////////////////////////
	void DoGameLoopIteration();
	
	void UpdateGameState( sf::Time elapsedTime );
	void UpdateGame( sf::Time elapsedTime );
	void DrawGameToWindow( sf::RenderWindow& window );
	void UpdateWindowTitle();

	void LoadNextScene();

	// MEMBERS
	//====================================================================================================
	std::string _gameName;					//!< The name of the game. Appears in the title of the window.

	bool _gameIsRunning;					//!< Whether the game is currently running or not.
	int _targetFramerate;					//!< The target framerate in frames per second.

	sf::Clock _gameLoopClock;				//!< The clock that is responsible for measuring the delta time between game frames.
	sf::Clock _gameStateClock;				//!< The clock that is responsible for measuring the delta time between game frames for game states.
	sf::Clock _renderClock;					//!< The clock that is responsible for measuring the game's frames per second.

	sf::RenderWindow _gameWindow;			//!< The game's main output window.
	WindowDimensions _gameWindowDimensions;	//!< The game's window's dimensions.

	GameState _currentGameState;			//!< The game's current state.

	GameScene* _currentScene;				//!< A pointer to the currently loaded scene.
	GameScene* _nextSceneToLoad;			//!< A pointer to the scene that will be loaded after the current update loop has ended.
};

#endif // COMMONS_GAME_GAME_H