﻿#include "BoxCollider.hpp"
#include "CircleCollider.hpp"
#include "ColliderIntersections.hpp"

#include <Commons/Game/GameObject.hpp>

#include <math.h>
#include <Commons/Math/SfmlVectorMath.hpp>

#include <Commons/Math/Line.hpp>

#include <Commons/Game/Camera.hpp>

BoxCollider::BoxCollider()
{
	_vertices = { sf::Vector2f( 0.0f, 0.0f ), sf::Vector2f( 0.0f, 0.0f ), sf::Vector2f( 0.0f, 0.0f ), sf::Vector2f( 0.0f, 0.0f ) };
	SetOrigin( sf::Vector2f( 0.0f, 0.0f ) );
	_bounds = sf::FloatRect( 0.0f, 0.0f, 0.0f, 0.0f );
}

BoxCollider::BoxCollider( float size, GameObject* associatedGameObject )
{
	_vertices = { sf::Vector2f( 0.0f, 0.0f ), sf::Vector2f( 0.0f, 0.0f ), sf::Vector2f( 0.0f, 0.0f ), sf::Vector2f( 0.0f, 0.0f ) };
	SetSize( size );
	SetOrigin( sf::Vector2f( 0.0f, 0.0f ) );
	SetParentGameObject( associatedGameObject );
	_bounds = sf::FloatRect( 0.0f, 0.0f, 0.0f, 0.0f );
}

BoxCollider::BoxCollider( float size, sf::Vector2f offset, GameObject* associatedGameObject )
{
	_vertices = { sf::Vector2f( 0.0f, 0.0f ), sf::Vector2f( 0.0f, 0.0f ), sf::Vector2f( 0.0f, 0.0f ), sf::Vector2f( 0.0f, 0.0f ) };

	SetSize( size );
	SetOrigin( offset );
	SetParentGameObject( associatedGameObject );
	_bounds = sf::FloatRect( 0.0f, 0.0f, 0.0f, 0.0f );
}

BoxCollider::BoxCollider( float width, float height, GameObject* associatedGameObject )
{
	_vertices = { sf::Vector2f( 0.0f, 0.0f ), sf::Vector2f( 0.0f, 0.0f ), sf::Vector2f( 0.0f, 0.0f ), sf::Vector2f( 0.0f, 0.0f ) };

	SetSize( width, height );
	SetOrigin( sf::Vector2f( 0.0f, 0.0f ) );
	SetParentGameObject( associatedGameObject );
	_bounds = sf::FloatRect( 0.0f, 0.0f, 0.0f, 0.0f );
}

BoxCollider::BoxCollider( float width, float height, sf::Vector2f offset, GameObject* associatedGameObject )
{
	_vertices = { sf::Vector2f( 0.0f, 0.0f ), sf::Vector2f( 0.0f, 0.0f ), sf::Vector2f( 0.0f, 0.0f ), sf::Vector2f( 0.0f, 0.0f ) };

	SetSize( width, height );
	SetOrigin( offset );
	SetParentGameObject( associatedGameObject );
	_bounds = sf::FloatRect( 0.0f, 0.0f, 0.0f, 0.0f );
}

BoxCollider::~BoxCollider()
{
}

void BoxCollider::SetSize( float size )
{
	_dimensions.x = size;
	_dimensions.y = size;
}

void BoxCollider::SetSize( float width, float height )
{
	_dimensions.x = width;
	_dimensions.y = height;

	_squaredHalfDimensions.x = pow( width / 2.0f, 2 );
	_squaredHalfDimensions.y = pow( height / 2.0f, 2 );

	RecalculateBounds();
}

sf::Vector2f BoxCollider::GetSize() const
{
	return _dimensions;
}

sf::Vector2f BoxCollider::GetPointAPosition() const
{
	return _vertices[0];
}

sf::Vector2f BoxCollider::GetPointBPosition() const
{
	return _vertices[1];
}

sf::Vector2f BoxCollider::GetPointCPosition() const
{
	return _vertices[2];
}

sf::Vector2f BoxCollider::GetPointDPosition() const
{
	return _vertices[3];
}

const std::array<sf::Vector2f, 4>& BoxCollider::GetVertices() const
{
	return _vertices;
}

std::array<sf::Vector2f, 4>BoxCollider::GetVerticesLocal() const
{
	std::array<sf::Vector2f, 4> vertices = _vertices;
	for ( int i = 0; i < vertices.size(); i++ )
	{
		vertices[i] -= GetOriginWorld();
	}
	return vertices;
}

const std::array<Line, 4>& BoxCollider::GetEdges() const
{
	std::array<Line, 4> edges;

	edges[0] = Line( GetPointAPosition(), GetPointBPosition() );
	edges[1] = Line( GetPointBPosition(), GetPointCPosition() );
	edges[2] = Line( GetPointCPosition(), GetPointDPosition() );
	edges[3] = Line( GetPointDPosition(), GetPointAPosition() );

	return edges;
}

const std::array<Line, 4>& BoxCollider::GetEdgesLocal() const
{
	std::array<Line, 4> edges;

	edges[0] = Line( GetPointAPosition() - GetOriginWorld(), GetPointBPosition() - GetOriginWorld() );
	edges[1] = Line( GetPointBPosition() - GetOriginWorld(), GetPointCPosition() - GetOriginWorld() );
	edges[2] = Line( GetPointCPosition() - GetOriginWorld(), GetPointDPosition() - GetOriginWorld() );
	edges[3] = Line( GetPointDPosition() - GetOriginWorld(), GetPointAPosition() - GetOriginWorld() );

	return edges;
}

void BoxCollider::RecalculateBounds()
{
	RecalculateVertices();
	sf::FloatRect newBounds( 0.0f, 0.0f, 0.0f, 0.0f );

	for ( int i = 0; i < _vertices.size(); i++ )
	{
		if( _vertices[i].x < newBounds.left )
		{
			newBounds.left = _vertices[i].x;
		}
		else if ( _vertices[i].x > ( newBounds.left + newBounds.width ) )
		{
			newBounds.width = _vertices[i].x - newBounds.left;
		}

		if ( _vertices[i].y < newBounds.top )
		{
			newBounds.top = _vertices[i].y;
		}
		else if ( _vertices[i].y >( newBounds.top + newBounds.height ) )
		{
			newBounds.height = _vertices[i].y - newBounds.top;
		}
	}

	_bounds = newBounds;
}

bool BoxCollider::Intersects( Collider& otherCollider, Collision& outCollision ) const
{
	return CheckIntersectionAtPosition( otherCollider, GetOriginWorld(), outCollision );
}

bool BoxCollider::CheckIntersectionAtPosition( Collider& otherCollider, const sf::Vector2f& position, Collision& outCollision ) const
{
	outCollision.OtherCollider = &otherCollider;

	if ( typeid( otherCollider ) == typeid( CircleCollider ) )
	{
		return ColliderIntersections::IntersectCircleBox( static_cast<const CircleCollider&>( otherCollider ), otherCollider.GetOriginWorld(), *this, position, outCollision );
	}

	if ( typeid( otherCollider ) == typeid( BoxCollider ) )
	{
		return ColliderIntersections::IntersectBoxBox( static_cast<const BoxCollider&>( otherCollider ), otherCollider.GetOriginWorld(), *this, position, outCollision );
	}

	return false;
}

void BoxCollider::RecalculateVertices()
{
	if( GetParentGameObject() == nullptr )
	{
		return;
	}

	sf::Vector2f center = GetOriginWorld();
	float angle = GetParentGameObject()->GetTransform().GetRotation();

	sf::Vector2f boxUp = sf::VectorMath::GetNormalized( sf::VectorMath::GetVectorFromAngle( -angle ) );
	sf::Vector2f boxRight = sf::VectorMath::GetRotation90DegreesClockWise( boxUp );
	
	_vertices[0] = center + ( boxUp * ( _dimensions.y / 2.0f ) ) - ( boxRight * ( _dimensions.x / 2.0f ) );
	_vertices[1] = center + ( boxUp * ( _dimensions.y / 2.0f ) ) + ( boxRight * ( _dimensions.x / 2.0f ) );
	_vertices[2] = center - ( boxUp * ( _dimensions.y / 2.0f ) ) + ( boxRight * ( _dimensions.x / 2.0f ) );
	_vertices[3] = center - ( boxUp * ( _dimensions.y / 2.0f ) ) - ( boxRight * ( _dimensions.x / 2.0f ) );
}

void BoxCollider::DrawToWindow( sf::RenderWindow& targetWindow, Camera* camera )
{
	if ( GetParentGameObject() == nullptr || camera == nullptr )
	{
		return;
	}

	std::array<sf::Vector2f, 4> vertices = GetVerticesLocal();

	sf::ConvexShape box;
	box.setPointCount( vertices.size() );
	for ( int i = 0; i < vertices.size(); i++ )
	{
		box.setPoint( i, vertices[i] );
	}
	box.setFillColor( GetFillColor() );
	box.setOutlineColor( GetOutlineColor() );
	box.setOutlineThickness( 1.0f );

	Transform drawTransform = GetParentGameObject()->GetTransform();
	drawTransform.SetRotation( 0.0f );
	drawTransform.SetOrigin( sf::Vector2f( 0.0f, 0.0f ) );
	drawTransform = camera->LocalTransformToCameraTransform( drawTransform );
	targetWindow.draw( box, drawTransform.GetSfmlTransform() );
}
