﻿#ifndef COMMONS_GAME_PHYSICS_COLLIDER_H
#define COMMONS_GAME_PHYSICS_COLLIDER_H

#include <SFML/Graphics.hpp>
#include <Commons/Game/Interfaces/IDrawable.hpp>
#include <Commons/Game/Physics/Collision.h>

class GameScene;
class GameObject;
class Transform;

////////////////////////////////////////////////////////////
/// \brief A collider describes the physical outline of an object.
////////////////////////////////////////////////////////////
class Collider : public IDrawable
{
	friend GameScene;
	friend Transform;

public:
	////////////////////////////////////////////////////////////
	/// \brief The Destructor.
	////////////////////////////////////////////////////////////
	virtual ~Collider();

	////////////////////////////////////////////////////////////
	/// \brief Recalculates the Collider's bounds.
	///
	/// The Collider's bounds are used to check whether two colliders
	/// can intersect.
	////////////////////////////////////////////////////////////
	virtual void RecalculateBounds() = 0;

	////////////////////////////////////////////////////////////
	/// \brief Returns the Collider's bounds.
	///
	/// \returns The bounds.
	////////////////////////////////////////////////////////////
	sf::FloatRect GetBounds() const;

	////////////////////////////////////////////////////////////
	/// \brief Sets the Collider's point of origin.
	///
	/// \param newOrigin The new origin in local space relative to the parent GameObject's position.
	///
	/// \see GameObject
	////////////////////////////////////////////////////////////
	void SetOrigin( const sf::Vector2f& newOrigin );

	////////////////////////////////////////////////////////////
	/// \brief Returns the Collider's point of origin in local space.
	///
	/// The origin is in local space relative to the parent GameObject's position.
	///
	/// \returns The Collider's point of origin in local space.
	///
	/// \see GameObject
	/// \see GetOriginWorld()
	////////////////////////////////////////////////////////////
	const sf::Vector2f& GetOrigin() const;

	////////////////////////////////////////////////////////////
	/// \brief Returns the Collider's point of origin in world space.
	///
	/// The origin is in woreld space.
	///
	/// \returns The Collider's point of origin in world space.
	///
	/// \see GameObject
	/// \see GetOrigin()
	////////////////////////////////////////////////////////////
	sf::Vector2f GetOriginWorld() const;

	////////////////////////////////////////////////////////////
	/// \brief Checks whether two colliders could possibly intersect.
	///
	/// Checks whether this Collider's bounds intersect with the other's.
	/// Calculating whether two colliders intersect can sometimes be cost-intensive.
	/// Therefore, checking whether they are even close to intersecting first can save time.
	///
	/// \param otherCollider The other Collider.
	///
	/// \returns Whether the two bounds intersect.
	///
	/// \see Collider::GetBounds()
	/// \see Collider::RecalculateBounds()
	/// \see Collider::Intersects()
	////////////////////////////////////////////////////////////
	bool CanIntersectWithCollider( const Collider& otherCollider ) const;

	////////////////////////////////////////////////////////////
	/// \brief Checks whether two colliders intersect.
	///
	/// \returns Whether the two colliders intersect.
	///
	/// \see Collider:CanIntersectWithCollider()
	////////////////////////////////////////////////////////////
	virtual bool Intersects( Collider& otherCollider, Collision& outCollision ) const = 0;

	virtual bool CheckIntersectionAtPosition( Collider& otherCollider, const sf::Vector2f& position, Collision& outCollision ) const = 0;

	////////////////////////////////////////////////////////////
	/// \brief Sets the Collider's parent GameObject.
	///
	/// The parent GameObject is used to determine the Collider's position
	/// in space as well as inform the parent when collisions take place.
	///
	/// \param parent A pointer to the GameObject.
	////////////////////////////////////////////////////////////
	void SetParentGameObject( GameObject* parent );

	////////////////////////////////////////////////////////////
	/// \brief Returns a pointer to the parent GameObject.
	///
	/// The parent GameObject is used to determine the Collider's position
	/// in space as well as inform the parent when collisions take place.
	///
	/// \returns A pointer to the parent GameObject.
	////////////////////////////////////////////////////////////
	GameObject* GetParentGameObject() const;

	////////////////////////////////////////////////////////////
	/// \brief Sets whether the Collider is a trigger or not.
	///
	/// A Collider that is set as a trigger is not solid and can pass through other colliders.
	///
	/// \param isTrigger Whether the Collider should be a trigger or not.
	////////////////////////////////////////////////////////////
	void SetTrigger( bool isTrigger );

	////////////////////////////////////////////////////////////
	/// \brief Gets whether the Collider is a trigger or not.
	///
	/// A Collider that is set as a trigger is not solid and can pass through other colliders.
	///
	/// \returns Whether the Collider is a trigger or not.
	////////////////////////////////////////////////////////////
	bool IsTrigger() const;

	////////////////////////////////////////////////////////////
	/// \brief Draws the Collider to a given window.
	///
	/// \param targetWindow	The window to draw to.
	/// \param camera		The camera that renders the object.
	////////////////////////////////////////////////////////////
	void DrawToWindow( sf::RenderWindow& targetWindow, Camera* camera ) override;

	sf::Color GetFillColor() const;
	sf::Color GetOutlineColor() const;

protected:
	////////////////////////////////////////////////////////////
	/// \brief The default constructor.
	///
	/// This constructor is protected, meaning that no instance of the
	/// Collider base class can be created.
	////////////////////////////////////////////////////////////
	Collider();

	sf::FloatRect _bounds;			//!< The Collider's bounds are used to check whether two colliders can intersect.

private:
	void InformParentOfCollisions();

	GameObject* _parentGameObject;	//!< The parent gameObject.
	sf::Vector2f _origin;			//!< The collider's origin.
									
	bool _isTrigger;				//!< Whether the collider is a trigger or not. Triggers are not solid.

	std::vector<Collision> _currentCollisions;
	std::vector<Collision> _previousCollisions;

};

#endif // COMMONS_GAME_PHYSICS_COLLIDER_H