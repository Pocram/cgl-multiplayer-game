﻿#ifndef COMMONS_GAME_PHYSICS_CIRCLE_COLLIDER_H
#define COMMONS_GAME_PHYSICS_CIRCLE_COLLIDER_H

#include <Commons/Game/Physics/Collider.hpp>

////////////////////////////////////////////////////////////
/// \brief A circle that describes the physical outline of a GameObject.
////////////////////////////////////////////////////////////
class CircleCollider : public Collider
{
public:
	////////////////////////////////////////////////////////////
	/// \brief The default constructor.
	///
	/// You should consider calling one of the other two constructors instead.
	////////////////////////////////////////////////////////////
	CircleCollider();

	////////////////////////////////////////////////////////////
	/// \brief Sets up the circle and associates it to a GameObject.
	///
	/// \param radius			The radius in world space.
	/// \param parentGameObject	The GameObject the collider belongs to.
	////////////////////////////////////////////////////////////
	CircleCollider( float radius, GameObject* parentGameObject );

	////////////////////////////////////////////////////////////
	/// \brief Sets up the circle and associates it to a GameObject.
	///
	/// \param radius			The radius in world space.
	/// \param offset			The collider's offset in local space.
	/// \param parentGameObject	The GameObject the collider belongs to.
	////////////////////////////////////////////////////////////
	CircleCollider( float radius, sf::Vector2f offset, GameObject* parentGameObject );

	////////////////////////////////////////////////////////////
	/// \brief The destructor.
	////////////////////////////////////////////////////////////
	virtual ~CircleCollider();

	////////////////////////////////////////////////////////////
	/// \brief Recalculates the Collider's bounds.
	///
	/// The Collider's bounds are used to check whether two colliders
	/// can intersect.
	////////////////////////////////////////////////////////////
	void RecalculateBounds() override;

	////////////////////////////////////////////////////////////
	/// \brief Checks whether two colliders intersect.
	///
	/// \returns Whether the two colliders intersect.
	///
	/// \see Collider:CanIntersectWithCollider()
	////////////////////////////////////////////////////////////
	bool Intersects( Collider& otherCollider, Collision& outCollision ) const override;
	
	bool CheckIntersectionAtPosition( Collider& otherCollider, const sf::Vector2f& position, Collision& outCollision ) const override;

	float GetRadius() const;
	void SetRadius( float newRadius );

	void DrawToWindow( sf::RenderWindow& targetWindow, Camera* camera ) override;
private:
	float _radius;	//!< The circle's radius in world space.
};

#endif // COMMONS_GAME_PHYSICS_CIRCLE_COLLIDER_H
