﻿#include "Collider.hpp"

#include <Commons/Game/GameObject.hpp>
#include <algorithm>
#include <Commons/Console/Console.hpp>

Collider::~Collider()
{
}

Collider::Collider()
{
	_bounds = sf::FloatRect( 0.0f, 0.0f, 0.0f, 0.0f );
	_parentGameObject = nullptr;
	_isTrigger = false;
}

void Collider::InformParentOfCollisions()
{
	if ( GetParentGameObject() == nullptr )
	{
		return;
	}

	std::vector<Collision> allCollisions = _currentCollisions;
	allCollisions.insert( allCollisions.end(), _previousCollisions.begin(), _previousCollisions.end() );
	allCollisions.erase( std::unique( allCollisions.begin(), allCollisions.end() ), allCollisions.end() );

	for ( int i = 0; i < allCollisions.size(); i++ )
	{
		Collider* otherCollider = allCollisions[i].OtherCollider;

		bool isCurrentlyCollidingWithCollider = std::find( _currentCollisions.begin(), _currentCollisions.end(), allCollisions[i] ) != _currentCollisions.end();
		bool wasPreviouslyCollidingWithCollider = std::find( _previousCollisions.begin(), _previousCollisions.end(), allCollisions[i] ) != _previousCollisions.end();

		if ( isCurrentlyCollidingWithCollider && wasPreviouslyCollidingWithCollider == false )
		{
			GetParentGameObject()->OnCollisionEnter( allCollisions[i] );
		}
		else if ( isCurrentlyCollidingWithCollider && wasPreviouslyCollidingWithCollider )
		{
			GetParentGameObject()->OnCollisionStay( allCollisions[i] );
		}
		else if ( isCurrentlyCollidingWithCollider == false && wasPreviouslyCollidingWithCollider )
		{
			GetParentGameObject()->OnCollisionExit( allCollisions[i] );
		}
	}
}

sf::FloatRect Collider::GetBounds() const
{
	return _bounds;
}

void Collider::SetOrigin( const sf::Vector2f& newOrigin )
{
	_origin = newOrigin;
}

const sf::Vector2f& Collider::GetOrigin() const
{
	return _origin;
}

sf::Vector2f Collider::GetOriginWorld() const
{
	sf::Vector2f parentPosition = GetParentGameObject()->GetTransform().GetPosition();
	return parentPosition + GetOrigin();
}

bool Collider::CanIntersectWithCollider( const Collider& otherCollider ) const
{
	return GetBounds().intersects( otherCollider.GetBounds() );
}

void Collider::SetParentGameObject( GameObject* parent )
{
	_parentGameObject = parent;
}

GameObject* Collider::GetParentGameObject() const
{
	return _parentGameObject;
}

void Collider::SetTrigger( bool isTrigger )
{
	_isTrigger = isTrigger;
}

bool Collider::IsTrigger() const
{
	return _isTrigger;
}

void Collider::DrawToWindow( sf::RenderWindow& targetWindow, Camera* camera )
{
}

sf::Color Collider::GetFillColor() const
{
	sf::Color color( 127, 255, 0, 10 );
	if ( IsTrigger() )
	{
		color.a = 5;
	}
	return color;
}

sf::Color Collider::GetOutlineColor() const
{
	sf::Color color( 127, 255, 0, 63 );
	if ( IsTrigger() )
	{
		color.a = 31;
	}
	return color;
}
