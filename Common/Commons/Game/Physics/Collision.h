﻿#ifndef COMMONS_GAME_PHYSICS_COLLISION_H
#define COMMONS_GAME_PHYSICS_COLLISION_H

#include <SFML/Graphics.hpp>
class Collider;

struct Collision
{
	Collider*		OtherCollider;
	sf::Vector2f	PositionInWorld;
	sf::Vector2f	Normal;

	bool operator== ( const Collision& rhs ) const;
	bool operator!= ( const Collision& rhs ) const;
};

#endif // COMMONS_GAME_PHYSICS_COLLISION_H
