﻿#ifndef COMMONS_GAME_PHYSICS_COLLIDER_INTERSECTIONS_H
#define COMMONS_GAME_PHYSICS_COLLIDER_INTERSECTIONS_H

#include <SFML/Graphics.hpp>

class CircleCollider;
class BoxCollider;
class Collision;

namespace ColliderIntersections
{
	////////////////////////////////////////////////////////////
	/// \brief Checks whether two CircleColliders intersect.
	///
	/// \returns Whether the two colliders intersect.
	///
	/// \see Collider:CanIntersectWithCollider()
	////////////////////////////////////////////////////////////
	bool IntersectCircleCircle( const CircleCollider& a, const CircleCollider& b, Collision& outCollision );

	bool IntersectCircleCircle( const CircleCollider& a, const sf::Vector2f& centerPositionA, const CircleCollider& b, const sf::Vector2f& centerPositionB, Collision& outCollision );
	
	////////////////////////////////////////////////////////////
	/// \brief Checks whether two colliders intersect.
	///
	/// \returns Whether the two colliders intersect.
	///
	/// \see Collider:CanIntersectWithCollider()
	////////////////////////////////////////////////////////////
	bool IntersectCircleBox( const CircleCollider& circle, const BoxCollider& box, Collision& outCollision );

	bool IntersectCircleBox( const CircleCollider& circle, const sf::Vector2f& circleCenterPosition, const BoxCollider& box, const sf::Vector2f& boxCenterPosition, Collision& outCollision );
	
	////////////////////////////////////////////////////////////
	/// \brief Checks whether two colliders intersect.
	///
	/// \returns Whether the two colliders intersect.
	///
	/// \see Collider:CanIntersectWithCollider()
	////////////////////////////////////////////////////////////
	bool IntersectBoxBox( const BoxCollider& a, const BoxCollider& b, Collision& outCollision );

	bool IntersectBoxBox( const BoxCollider& a, const sf::Vector2f& centerPositionA, const BoxCollider& b, const sf::Vector2f& centerPositionB, Collision& outCollision );
}

#endif // COMMONS_GAME_PHYSICS_COLLIDER_INTERSECTIONS_H