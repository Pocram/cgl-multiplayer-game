﻿#include "Collision.h"
#include <Commons/Game/Physics/Collider.hpp>

bool Collision::operator==( const Collision& rhs ) const
{
	return ( OtherCollider == rhs.OtherCollider ) && ( PositionInWorld == rhs.PositionInWorld );
}

bool Collision::operator!=( const Collision& rhs ) const
{
	return !( *this == rhs );
}
