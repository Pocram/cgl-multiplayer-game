﻿#include "CircleCollider.hpp"
#include "BoxCollider.hpp"

#include <Commons/Game/GameObject.hpp>
#include <Commons/Math/SfmlVectorMath.hpp>
#include "ColliderIntersections.hpp"

#include <Commons/Game/Camera.hpp>

#include <Commons/Console/Console.hpp>
#include <sstream>

CircleCollider::CircleCollider()
{
	_radius = 0.0f;
}

CircleCollider::CircleCollider( float radius, GameObject* parentGameObject )
{
	SetParentGameObject( parentGameObject );
	SetOrigin( sf::Vector2f( 0.0f, 0.0f ) );
	_radius = radius;
}

CircleCollider::CircleCollider( float radius, sf::Vector2f offset, GameObject* parentGameObject )
{
	SetParentGameObject( parentGameObject );
	SetOrigin( offset );
	_radius = radius;
}

CircleCollider::~CircleCollider()
{
}

void CircleCollider::RecalculateBounds()
{
	if ( GetParentGameObject() == nullptr )
	{
		return;
	}

	_bounds = sf::FloatRect( GetOriginWorld().x - _radius, GetOriginWorld().y - _radius, 2.0f * _radius, 2.0f * _radius );
}

bool CircleCollider::Intersects( Collider& otherCollider, Collision& outCollision ) const
{
	return CheckIntersectionAtPosition( otherCollider, GetOriginWorld(), outCollision );
}

bool CircleCollider::CheckIntersectionAtPosition( Collider& otherCollider, const sf::Vector2f& position, Collision& outCollision ) const
{
	outCollision.OtherCollider = &otherCollider;

	if ( typeid( otherCollider ) == typeid( CircleCollider ) )
	{
		return ColliderIntersections::IntersectCircleCircle( *this, position, static_cast<const CircleCollider&>( otherCollider ), otherCollider.GetOriginWorld(), outCollision );
	}

	if ( typeid( otherCollider ) == typeid( BoxCollider ) )
	{
		return ColliderIntersections::IntersectCircleBox( *this, position, static_cast<const BoxCollider&>( otherCollider ), otherCollider.GetOriginWorld(), outCollision );
	}

	return false;
}

float CircleCollider::GetRadius() const
{
	return _radius;
}

void CircleCollider::SetRadius( float newRadius )
{
	_radius = newRadius;
}

void CircleCollider::DrawToWindow( sf::RenderWindow& targetWindow, Camera* camera )
{
	if ( GetParentGameObject() == nullptr || camera == nullptr )
	{
		return;
	}

	Transform drawTransform = GetParentGameObject()->GetTransform();

	sf::CircleShape circle( GetRadius() );
	circle.setFillColor( GetFillColor() );
	circle.setOutlineColor( GetOutlineColor() );
	circle.setOutlineThickness( 1.0f );
	circle.setOrigin( GetRadius(), GetRadius() );

	drawTransform.SetOrigin( GetOrigin() );
	drawTransform = camera->LocalTransformToCameraTransform( drawTransform );
	targetWindow.draw( circle, drawTransform.GetSfmlTransform() );
}
