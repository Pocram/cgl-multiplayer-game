﻿#include "ColliderIntersections.hpp"

#include <Commons/Game/Game.hpp>
#include <Commons/Game/Physics/CircleCollider.hpp>
#include <Commons/Game/Physics/BoxCollider.hpp>
#include <Commons/Math/SfmlVectorMath.hpp>
#include <Commons/Math/Line.hpp>

#include <array>
#include <sstream>
#include <Commons/Console/Console.hpp>

bool ColliderIntersections::IntersectCircleCircle( const CircleCollider& a, const CircleCollider& b, Collision& outCollision )
{
	return IntersectCircleCircle( a, b.GetOriginWorld(), b, b.GetOriginWorld(), outCollision );
}

bool ColliderIntersections::IntersectCircleCircle( const CircleCollider& a, const sf::Vector2f& centerPositionA, const CircleCollider& b, const sf::Vector2f& centerPositionB, Collision& outCollision )
{
	float distanceBetweenCenters = sf::VectorMath::Distance( centerPositionA, centerPositionB );
	float distanceBetweenCircles = distanceBetweenCenters - ( a.GetRadius() + b.GetRadius() );

	// If the circles intersect, populate outCollision.
	if ( distanceBetweenCircles <= 0.0f )
	{
		sf::Vector2f centerAToB = sf::VectorMath::GetNormalized( centerPositionB - centerPositionA );
		sf::Vector2f positionOnRimA = centerPositionA + centerAToB * a.GetRadius();
		sf::Vector2f positionOnRimB = centerPositionB - centerAToB * b.GetRadius();
		sf::Vector2f positionBetweenRims = ( positionOnRimA + positionOnRimB ) / 2.0f;

		outCollision.PositionInWorld = positionBetweenRims;
		outCollision.Normal = centerAToB;

		return true;
	}

	return false;
}

bool ColliderIntersections::IntersectCircleBox( const CircleCollider& circle, const BoxCollider& box, Collision& outCollision )
{
	return IntersectCircleBox( circle, circle.GetOriginWorld(), box, box.GetOriginWorld(), outCollision );
}

bool ColliderIntersections::IntersectCircleBox( const CircleCollider& circle, const sf::Vector2f& circleCenterPosition, const BoxCollider& box, const sf::Vector2f& boxCenterPosition, Collision& outCollision )
{
	std::array<Line, 4> edges = box.GetEdgesLocal();

	// Check if any of the box's vertices intersects with the circle.
	for ( int i = 0; i < edges.size(); i++ )
	{
		sf::Vector2f edgeStart = edges[i].GetStart() + boxCenterPosition;
		sf::Vector2f edgeEnd = edges[i].GetEnd() + boxCenterPosition;

		sf::Vector2f closesPointOnLine;
		float distanceToCircle = sf::VectorMath::DistanceOfPointToLine( edgeStart, edgeEnd, circleCenterPosition, closesPointOnLine );
		if ( distanceToCircle <= circle.GetRadius() )
		{
			outCollision.PositionInWorld = closesPointOnLine;
			
			sf::Vector2f normal = sf::VectorMath::GetRotation90DegreesCounterClockWise( edgeEnd - edgeStart );
			if ( sf::VectorMath::DotProduct( normal, boxCenterPosition - closesPointOnLine ) > 0.0f )
			{
				normal *= -1.0f;
			}
			outCollision.Normal = normal;

			return true;
		}
	}

	return false;
}

bool ColliderIntersections::IntersectBoxBox( const BoxCollider& a, const BoxCollider& b, Collision& outCollision )
{
	return IntersectBoxBox( a, a.GetOriginWorld(), b, b.GetOriginWorld(), outCollision );
}

bool ColliderIntersections::IntersectBoxBox( const BoxCollider& a, const sf::Vector2f& centerPositionA, const BoxCollider& b, const sf::Vector2f& centerPositionB, Collision& outCollision )
{
	std::array<Line, 4> boxAEdges = a.GetEdgesLocal();
	std::array<Line, 4> boxBEdges = b.GetEdgesLocal();

	for ( int i = 0; i < boxAEdges.size(); i++ )
	{
		for ( int j = 0; j < boxBEdges.size(); j++ )
		{
			Line lineA( boxAEdges[i].GetStart() + centerPositionA, boxAEdges[i].GetEnd() + centerPositionA );
			Line lineB( boxBEdges[i].GetStart() + centerPositionB, boxBEdges[i].GetEnd() + centerPositionB );
			
			sf::Vector2f intersectionPoint;
			if ( sf::VectorMath::DoLinesIntersect( lineA, lineB, intersectionPoint ) )
			{
				outCollision.PositionInWorld = intersectionPoint;
				outCollision.Normal = sf::VectorMath::GetMean( lineA.GetDirection(), lineB.GetDirection() );
				return true;
			}
		}
	}

	return false;
}
