﻿#ifndef COMMONS_GAME_GAME_SCENE_H
#define COMMONS_GAME_GAME_SCENE_H

#include <vector>
#include <string>

#include <SFML/Graphics.hpp>
#include <Commons/Game/GameObject.hpp>
#include <Commons/Game/Camera.hpp>

#include <Commons/Game/Interfaces/IUpdateable.hpp>
#include <Commons/Game/Interfaces/IDrawable.hpp>

class Game;

class GameScene : public IUpdateable
{
	friend Game;

public:
	GameScene();
	virtual ~GameScene();
	
	void UpdateGameObjects( sf::Time elapsedTime );
	void UpdatePhysics( sf::Time elapsedTime );

	virtual void Update( sf::Time elapsedTime ) override;
	virtual void OnLoad();

	void InstantiateGameObject( GameObject* gameObject );
	void DeleteGameObject( GameObject* gameObject );

	Camera& GetCamera();

	const std::vector<GameObject*>& GetGameObjects();

	const std::string& GetName();
	void SetName( const std::string& name );

	void RequestGameObjectReordering();

private:
	void ReorderGameObjects();
	void SetRandomSceneName();

	bool _doReorderGameObjects;

	std::string _name;
	std::vector<GameObject*> _gameObjectsInScene;
	Camera _mainCamera;
};

#endif // COMMONS_GAME_GAME_SCENE_H