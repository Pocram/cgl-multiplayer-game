﻿#ifndef COMMONS_GAME_INTERFACES_I_DRAWABLE_H
#define COMMONS_GAME_INTERFACES_I_DRAWABLE_H

#include <SFML/Graphics.hpp>
//#include <Commons/Game/Camera.hpp>

class Camera;

class IDrawable
{
public:
	virtual void DrawToWindow( sf::RenderWindow& targetWindow, Camera* camera ) = 0;
};

#endif // COMMONS_GAME_INTERFACES_I_DRAWABLE_H