﻿#ifndef COMMONS_GAME_INTERFACES_I_UPDATEABLE_H
#define COMMONS_GAME_INTERFACES_I_UPDATEABLE_H

#include <SFML/Graphics.hpp>

class IUpdateable
{
public:
	virtual void Update( sf::Time elapsedTime ) = 0;
};

#endif // COMMONS_GAME_INTERFACES_I_UPDATEABLE_H