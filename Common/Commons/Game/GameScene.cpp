﻿#include "GameScene.hpp"
#include <Commons/Console/Console.hpp>

#include <algorithm>
#include <sstream>

GameScene::GameScene()
{
	SetRandomSceneName();
}

GameScene::~GameScene()
{
	for ( std::vector<GameObject*>::const_iterator it = _gameObjectsInScene.begin(); it != _gameObjectsInScene.end(); ++it )
	{
		GameObject* gameObject = *it;
		delete( gameObject );
	}
}

void GameScene::UpdateGameObjects( sf::Time elapsedTime )
{
	for ( std::vector<GameObject*>::const_iterator it = _gameObjectsInScene.begin(); it != _gameObjectsInScene.end(); ++it )
	{
		GameObject* gameObject = *it;
		if ( gameObject == nullptr )
			continue;

		// Flush collisions first, as translating a GameObject may already cause a collision.
		if ( gameObject->HasCollider() )
		{
			gameObject->GetCollider()->_previousCollisions = gameObject->GetCollider()->_currentCollisions;
			gameObject->GetCollider()->_currentCollisions.clear();
		}

		gameObject->Update( elapsedTime );
	}
}

void GameScene::UpdatePhysics( sf::Time elapsedTime )
{
	int gameObjectCount = GetGameObjects().size();

	for ( int i = 0; i < gameObjectCount; i++ )
	{
		GameObject* gameObjectA = GetGameObjects()[i];
		if ( gameObjectA->HasCollider() == false )
		{
			continue;
		}
		Collider* colliderA = gameObjectA->GetCollider();

		for ( int j = i + 1; j < gameObjectCount; j++ )
		{
			GameObject* gameObjectB = GetGameObjects()[j];
			if ( gameObjectB->HasCollider() == false || gameObjectA == gameObjectB )
			{
				continue;
			}
			Collider* colliderB = gameObjectB->GetCollider();

			// We want to check for trigger intersections only. Solid collisions are checked by the Transform class.
			if ( colliderA->IsTrigger() == false && colliderB->IsTrigger() == false )
			{
				continue;
			}

			if ( colliderA->CanIntersectWithCollider( *colliderB ) )
			{
				Collision collisionA;
				bool intersects = colliderA->Intersects( *colliderB, collisionA );
				if ( intersects )
				{
					Collision collisionB;
					collisionB.OtherCollider = colliderA;
					collisionB.PositionInWorld = collisionA.PositionInWorld;
					collisionB.Normal = collisionA.Normal;

					colliderA->_currentCollisions.push_back( collisionA );
					colliderB->_currentCollisions.push_back( collisionB );
				}
			}
		}
	}

	for ( int i = 0; i < GetGameObjects().size(); i++ )
	{
		if( GetGameObjects()[i]->HasCollider() )
		{
			GetGameObjects()[i]->GetCollider()->InformParentOfCollisions();
		}
	}
}

void GameScene::Update( sf::Time elapsedTime )
{
}

void GameScene::OnLoad()
{
	_mainCamera.CalculateBoundingBox();
	for ( int i = 0; i < _gameObjectsInScene.size(); i++ )
	{
		if(_gameObjectsInScene[i] != nullptr)
		{
			_gameObjectsInScene[i]->OnSceneStart();
		}
	}
}

void GameScene::InstantiateGameObject( GameObject* gameObject )
{
	if ( gameObject == nullptr )
	{
		return;
	}

	// An object may only exist once per scene.
	if( std::find(_gameObjectsInScene.begin(), _gameObjectsInScene.end(), gameObject) == _gameObjectsInScene.end() )
	{
		Debug::Console::GetInstance().WriteLine( "GameObject '" + gameObject->GetName() + "' has been added to scene '" + GetName() + "'!" );
		gameObject->CalculateBoundingBox();
		_gameObjectsInScene.push_back( gameObject );
		ReorderGameObjects();
		
		gameObject->OnInstantiate();
	}
	else
	{
		WriteWarningWithCaller( "The GameObject '" + gameObject->GetName() + "' is already part of scene '" + GetName() + "'!" );
	}
}

void GameScene::DeleteGameObject( GameObject* gameObject )
{
	// You can only delete GameObjects that are in the current scene.
	if( std::find(_gameObjectsInScene.begin(), _gameObjectsInScene.end(), gameObject) == _gameObjectsInScene.end() )
	{
		return;
	}

	gameObject->OnDestroy();
	_gameObjectsInScene.erase( std::remove( _gameObjectsInScene.begin(), _gameObjectsInScene.end(), gameObject ), _gameObjectsInScene.end() );
	delete( gameObject );
}

Camera& GameScene::GetCamera()
{
	return _mainCamera;
}

const std::vector<GameObject*>& GameScene::GetGameObjects()
{
	return _gameObjectsInScene;
}

const std::string& GameScene::GetName()
{
	return _name;
}

void GameScene::SetName( const std::string& name )
{
	_name = name;
}

void GameScene::RequestGameObjectReordering()
{
	_doReorderGameObjects = true;
}

void GameScene::ReorderGameObjects()
{
	std::sort( _gameObjectsInScene.begin(), _gameObjectsInScene.end(), GameObject::CompareDrawOrder );
}

void GameScene::SetRandomSceneName()
{
	int randomId = ( rand() % 100 ) + 1;
	std::stringstream ss;
	ss << "UnnamedScene " << randomId;
	SetName( ss.str() );
}
