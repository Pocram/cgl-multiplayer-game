﻿#include "SimpleTextObject.hpp"
#include <Commons/Game/Camera.hpp>
#include <Commons/Console/Console.hpp>

SimpleTextObject::SimpleTextObject()
{
}

SimpleTextObject::SimpleTextObject( sf::Font* font, std::string text, unsigned int size, sf::Color color )
{
	SetFont( font );
	SetText( text );
	SetSize( size );
	SetColor( color );
	Recenter();
}

SimpleTextObject::~SimpleTextObject()
{
}

void SimpleTextObject::SetFont( sf::Font* font )
{
	if ( font != nullptr )
	{
		_sfText.setFont( *font );
	}

	CalculateBoundingBox();
	Recenter();
}

void SimpleTextObject::SetText( std::string text )
{
	_sfText.setString( text );
	CalculateBoundingBox();
	Recenter();
}

void SimpleTextObject::SetSize( unsigned int size )
{
	_sfText.setCharacterSize( size );
	CalculateBoundingBox();
	Recenter();
}

void SimpleTextObject::SetColor( sf::Color color )
{
	_sfText.setColor( color );
}

void SimpleTextObject::DrawToWindow( sf::RenderWindow& targetWindow, Camera* camera )
{
	Transform drawTransform = camera->LocalTransformToCameraTransform( GetTransform() );
	targetWindow.draw( _sfText, drawTransform.GetSfmlTransform() );
}

void SimpleTextObject::CalculateBoundingBox()
{
	sf::Text text = _sfText;
	text.setPosition( GetTransform().GetPosition() );
	text.setOrigin( GetTransform().GetOrigin() );
	text.setRotation( GetTransform().GetRotation() );
	text.setScale( GetTransform().GetScale() );

	SetBoundingBox( text.getGlobalBounds() );
}

void SimpleTextObject::Recenter()
{
	GetTransform().SetOrigin( sf::Vector2f( GetBounds().width / 2.0f, GetBounds().height / 2.0f ) );
}
