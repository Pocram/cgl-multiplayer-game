﻿#ifndef COMMONS_GAME_GAME_OBJECTS_SIMPLE_BOX_COLLIDER_OBJECT_H
#define COMMONS_GAME_GAME_OBJECTS_SIMPLE_BOX_COLLIDER_OBJECT_H

#include <Commons/Game/GameObject.hpp>

class BoxCollider;

class SimpleBoxColliderObject : public GameObject
{
public:
	SimpleBoxColliderObject();
	SimpleBoxColliderObject( sf::Vector2f position, float width, float height );
	
	void CalculateBoundingBox() override;


private:
	BoxCollider* _boxCollider;
};

#endif // COMMONS_GAME_GAME_OBJECTS_SIMPLE_BOX_COLLIDER_OBJECT_H