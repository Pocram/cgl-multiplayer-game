﻿#include "SimpleBoxColliderObject.hpp"
#include <Commons/Game/Physics/BoxCollider.hpp>

SimpleBoxColliderObject::SimpleBoxColliderObject()
{
	_boxCollider = new BoxCollider( 0.0f, nullptr );
}

SimpleBoxColliderObject::SimpleBoxColliderObject( sf::Vector2f position, float width, float height )
{
	_boxCollider = new BoxCollider( width, height, this );
	SetCollider( _boxCollider );
	GetTransform().SetPosition( position );
	GetTransform().SetOrigin( sf::Vector2f( width, height ) / 2.0f );
}

void SimpleBoxColliderObject::CalculateBoundingBox()
{
	if ( _boxCollider != nullptr )
	{
		SetBoundingBox( _boxCollider->GetBounds() );
	}
}
