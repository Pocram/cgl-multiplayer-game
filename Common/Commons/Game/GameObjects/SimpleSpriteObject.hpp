﻿#ifndef COMMONS_GAME_GAME_OBJECTS_SIMPLE_SPRITE_OBJECT_H
#define COMMONS_GAME_GAME_OBJECTS_SIMPLE_SPRITE_OBJECT_H

#include <Commons/Game/GameObject.hpp>

class SimpleSpriteObject : public GameObject
{
public:
	SimpleSpriteObject();
	SimpleSpriteObject( sf::Texture* texture, sf::Vector2f origin );
	~SimpleSpriteObject();

	void SetTexture( sf::Texture* texture );
	void SetOrigin( sf::Vector2f origin );

	void DrawToWindow( sf::RenderWindow& targetWindow, Camera* camera ) override;
	void CalculateBoundingBox() override;


private:
	sf::Sprite _sprite;
};

#endif // #ifndef COMMONS_GAME_GAME_OBJECTS_SIMPLE_SPRITE_OBJECT_H