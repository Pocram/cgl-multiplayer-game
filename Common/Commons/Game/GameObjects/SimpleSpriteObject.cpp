﻿#include "SimpleSpriteObject.hpp"

#include <Commons/Game/Camera.hpp>

SimpleSpriteObject::SimpleSpriteObject()
{
}

SimpleSpriteObject::SimpleSpriteObject( sf::Texture* texture, sf::Vector2f origin )
{
	SetTexture( texture );
	SetOrigin( origin );
}

SimpleSpriteObject::~SimpleSpriteObject()
{
}

void SimpleSpriteObject::SetTexture( sf::Texture* texture )
{
	if ( texture != nullptr )
	{
		_sprite.setTexture( *texture );
	}
}

void SimpleSpriteObject::SetOrigin( sf::Vector2f origin )
{
	_sprite.setOrigin( origin );
}

void SimpleSpriteObject::DrawToWindow( sf::RenderWindow& targetWindow, Camera* camera )
{
	Transform drawTransform = camera->LocalTransformToCameraTransform( GetTransform() );
	targetWindow.draw( _sprite, drawTransform.GetSfmlTransform() );
}

void SimpleSpriteObject::CalculateBoundingBox()
{
	CalculateBoundingBoxFromSprite( _sprite );
}
