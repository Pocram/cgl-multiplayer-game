﻿#ifndef COMMONS_GAME_GAME_OBJECTS_SIMPLE_TEXT_OBJECT_H
#define COMMONS_GAME_GAME_OBJECTS_SIMPLE_TEXT_OBJECT_H

#include <Commons/Game/GameObject.hpp>

class SimpleTextObject : public GameObject
{
public:
	SimpleTextObject();
	SimpleTextObject( sf::Font* font, std::string text, unsigned int size, sf::Color color );
	~SimpleTextObject();

	void SetFont( sf::Font* font );
	void SetText( std::string text );
	void SetSize( unsigned int size );
	void SetColor( sf::Color color );

	void DrawToWindow( sf::RenderWindow& targetWindow, Camera* camera ) override;
	void CalculateBoundingBox() override;

private:
	void Recenter();
	sf::Text _sfText;
};

#endif // COMMONS_GAME_GAME_OBJECTS_SIMPLE_TEXT_OBJECT_H