﻿#include "Game.hpp"

#include <math.h>
#include <sstream>
#include <Commons/Console/Console.hpp>

Game::Game()
{
	_gameIsRunning = false;

	_gameWindow.create( sf::VideoMode( 10, 10 ), "Game", sf::Style::Titlebar | sf::Style::Close );
	_gameWindow.setVisible( false );

	SetTargetFramerate( 144 );
	SetWindowDimensionsByWidth( 500, 16.0f / 9.0f );

	_currentScene = nullptr;
	_nextSceneToLoad = nullptr;
}

Game& Game::GetInstance()
{
	static Game instance;
	return instance;
}

void Game::Start()
{
	_gameWindow.setVisible( true );

	while ( _gameWindow.isOpen() )
	{
		sf::Event event;
		while ( _gameWindow.pollEvent( event ) )
		{
			if ( event.type == sf::Event::Closed || sf::Keyboard::isKeyPressed( sf::Keyboard::Escape ) )
			{
				_gameWindow.close();
				_gameIsRunning = false;
			}
		}

		DoGameLoopIteration();
	}
}

void Game::Quit()
{
	_gameIsRunning = false;
}

const sf::RenderWindow& Game::GetGameWindow()
{
	return _gameWindow;
}

const bool& Game::GetGameIsRunning()
{
	return _gameIsRunning;
}

const GameState& Game::GetCurrentGameState()
{
	return _currentGameState;
}

void Game::DoGameLoopIteration()
{
	LoadNextScene();

	_renderClock.restart();

	const sf::Int64 frameTime = 1000000 / _targetFramerate;
	sf::Clock c;
	sf::Time t = c.getElapsedTime();
	sf::Int64 nextFrameTime = t.asMicroseconds() + frameTime;

	int loops = 0;
	while ( t.asMicroseconds() < nextFrameTime && loops < 5 )
	{
		UpdateGameState( _gameStateClock.restart() );
		UpdateGame( _gameLoopClock.restart() );
		t = c.getElapsedTime();
		loops++;
	}

	DrawGameToWindow( _gameWindow );
	UpdateWindowTitle();
}

void Game::UpdateGameState( sf::Time elapsedTime )
{
	_currentGameState.Update( elapsedTime );
}

void Game::UpdateGame( sf::Time elapsedTime )
{
	if ( _currentScene == nullptr )
	{
		return;
	}

	if ( _currentScene->_doReorderGameObjects )
	{
		_currentScene->ReorderGameObjects();
		_currentScene->_doReorderGameObjects = false;
	}
	_currentScene->UpdateGameObjects( elapsedTime );
	_currentScene->Update( elapsedTime );
	_currentScene->UpdatePhysics( elapsedTime );
	_currentScene->GetCamera().Update( elapsedTime );
}

void Game::DrawGameToWindow( sf::RenderWindow& window )
{
	window.clear();
	GetCurrentScene()->GetCamera().Render( window );
	window.display();
}

void Game::UpdateWindowTitle()
{
	float roundedFps = roundf( GetCurrentFramerate() * 100 ) / 100;

	std::stringstream newTitle;
	newTitle << _gameName << " (" << roundedFps << " fps)";
	_gameWindow.setTitle( newTitle.str() );
}

void Game::LoadNextScene()
{
	if( _nextSceneToLoad == nullptr )
	{
		return;
	}

	if ( _nextSceneToLoad == _currentScene )
	{
		_nextSceneToLoad = nullptr;
		return;
	}

	delete( _currentScene );
	_currentScene = _nextSceneToLoad;
	_nextSceneToLoad = nullptr;

	_currentScene->OnLoad();
}

float Game::GetCurrentFramerate()
{
	float currentFps = 1000000.0f / _renderClock.getElapsedTime().asMicroseconds();
	return currentFps;
}

void Game::SetGameName( std::string newName )
{
	_gameName = newName;

	std::wstringstream newConsoleTitle;
	newConsoleTitle << newName.c_str() << " Console";
	SetConsoleTitle( newConsoleTitle.str().c_str() );
}

void Game::SetWindowDimensions( int width, int height )
{
	_gameWindowDimensions.WindowWidth = width;
	_gameWindowDimensions.WindowHeight = height;

	_gameWindow.setSize( sf::Vector2u( _gameWindowDimensions.WindowWidth, _gameWindowDimensions.WindowHeight ) );
	_gameWindow.setView( sf::View( sf::FloatRect( 0, 0, width, height ) ) );

	// Center window on screen.
	sf::VideoMode desktopVideoMode = sf::VideoMode::getDesktopMode();
	sf::Vector2i newPosition( desktopVideoMode.width / 2 - width / 2, desktopVideoMode.height / 2 - height / 2 );
	_gameWindow.setPosition( newPosition );
}

WindowDimensions Game::GetWindowDimensions()
{
	return _gameWindowDimensions;
}

void Game::SetWindowDimensionsByWidth( int width, float aspectRatio )
{
	float windowHeight = width / aspectRatio;
	SetWindowDimensions( width, round( windowHeight ) );
}

void Game::SetWindowDimensionsByHeight( int height, float aspectRatio )
{
	float windowWidth = height * aspectRatio;
	SetWindowDimensions( round( windowWidth ), height );
}

void Game::LoadScene( GameScene* newScene )
{
	Debug::Console::GetInstance().WriteLine( "Loading scene ..." );

	if ( _currentScene == nullptr )
	{
		_currentScene = newScene;
		_currentScene->OnLoad();
		return;
	}

	// You can't load the current scene twice.
	if ( newScene == _currentScene )
	{
		WriteErrorWithCaller( "You can't load the same scene twice!" );
		return;
	}

	if ( _nextSceneToLoad != nullptr )
	{
		delete( _nextSceneToLoad );
	}

	_nextSceneToLoad = newScene;
}

GameScene* Game::GetCurrentScene()
{
	return _currentScene;
}

void Game::SetTargetFramerate( int framerate )
{
	_targetFramerate = framerate;
	_gameWindow.setFramerateLimit( framerate );
}

void Game::SetEnableVSynch( bool enabled )
{
	_gameWindow.setVerticalSyncEnabled( enabled );
}
