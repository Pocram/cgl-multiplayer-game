﻿#include "GameObject.hpp"

#include <algorithm>
#include <sstream>

#include "GameScene.hpp"
#include "Game.hpp"

GameObject::GameObject()
{
	SetRandomObjectName();
	_isActive = true;

	_transform = Transform( this );
	_drawOrderIndex = 0;

	SetCollider( nullptr );
}

GameObject::~GameObject()
{
}

void GameObject::Update( sf::Time elapsedTime )
{
}

void GameObject::DrawToWindow( sf::RenderWindow& targetWindow, Camera* camera )
{
	
}

Transform& GameObject::GetTransform()
{
	return _transform;
}

const std::string& GameObject::GetName() const
{
	return _name;
}

void GameObject::SetName( const std::string& name )
{
	_name = name;
}

sf::FloatRect GameObject::GetBounds() const
{
	return _boundRect;
}

void GameObject::SetActive( bool isActive )
{
	_isActive = isActive;
}

bool GameObject::IsActive() const
{
	return _isActive;
}

void GameObject::SetDrawOrderIndex( int newDrawOrderIndex )
{
	_drawOrderIndex = newDrawOrderIndex;

	GameScene* currentScene = Game::GetInstance().GetCurrentScene();
	if ( currentScene != nullptr )
	{
		currentScene->RequestGameObjectReordering();
	}
	//currentScene->ReorderGameObjects();
}

int GameObject::GetDrawOrderIndex() const
{
	return _drawOrderIndex;
}

void GameObject::CalculateBoundingBox()
{
}

Collider* GameObject::GetCollider() const
{
	return _collider;
}

bool GameObject::HasCollider() const
{
	return GetCollider() != nullptr;
}

void GameObject::AddTag( const std::string& tag )
{
	// Tags are unique.
	if( HasTag( tag ) )
	{
		return;
	}

	_tags.push_back( tag );
}

void GameObject::RemoveTag( const std::string& tag )
{
	_tags.erase( std::remove( _tags.begin(), _tags.end(), tag ), _tags.end() );
}

bool GameObject::HasTag( const std::string& tag )
{
	return std::find( _tags.begin(), _tags.end(), tag ) != _tags.end();
}

void GameObject::CalculateBoundingBoxFromSprite( sf::Sprite sprite )
{
	sprite.setOrigin( _transform.GetOrigin() );
	sprite.setPosition( _transform.GetPosition() );
	sprite.setScale( _transform.GetScale() );
	sprite.setRotation( _transform.GetRotation() );

	_boundRect = sprite.getGlobalBounds();
}

void GameObject::SetBoundingBox( sf::FloatRect bounds )
{
	_boundRect = bounds;
}

void GameObject::SetCollider( Collider* collider )
{
	_collider = collider;
}

void GameObject::OnCollisionEnter( Collision collision )
{
}

void GameObject::OnCollisionStay( Collision collision )
{
}

void GameObject::OnCollisionExit( Collision collision )
{
}

void GameObject::OnInstantiate()
{
}

void GameObject::OnSceneStart()
{
}

void GameObject::OnDestroy()
{
}

bool GameObject::CompareDrawOrder( GameObject* lhs, GameObject* rhs )
{
	return lhs->GetDrawOrderIndex() < rhs->GetDrawOrderIndex();
}

void GameObject::SetRandomObjectName()
{
	int randomId = ( rand() % 100 ) + 1;
	std::stringstream newName;
	newName << "UnnamedGameObject " << randomId;
	SetName( newName.str() );
}
