﻿#include "Transform.hpp"
#include "GameObject.hpp"
#include "Game.hpp"
#include <Commons/Console/Console.hpp>

Transform::Transform()
{
	_associatedGameObject = nullptr;
}

Transform::Transform( GameObject* owningObject )
{
	_associatedGameObject = owningObject;
}

Transform::~Transform()
{
}

sf::Vector2f Transform::GetPosition() const
{
	return _sfmlTransform.getPosition();
}

float Transform::GetRotation() const
{
	return _sfmlTransform.getRotation();
}

sf::Vector2f Transform::GetScale() const
{
	return _sfmlTransform.getScale();
}

sf::Vector2f Transform::GetOrigin() const
{
	return _sfmlTransform.getOrigin();
}

void Transform::SetPosition( const sf::Vector2f& newPosition )
{
	_sfmlTransform.setPosition( newPosition );
	UpdateParentGameObject();
}

void Transform::SetRotation( const float& newRotation )
{
	if ( CheckForSolidCollisionAtPosition( GetPosition() ) )
	{
		return;
	}

	_sfmlTransform.setRotation( newRotation );
	UpdateParentGameObject();
}

void Transform::SetScale( const sf::Vector2f& newScale )
{
	_sfmlTransform.setScale( newScale );
	UpdateParentGameObject();
}

void Transform::SetOrigin( const sf::Vector2f& newOrigin )
{
	_sfmlTransform.setOrigin( newOrigin );
	UpdateParentGameObject();
}

void Transform::Translate( const sf::Vector2f& direction )
{
	sf::Vector2f newPosition = GetPosition() + direction;
	if( CheckForSolidCollisionAtPosition( newPosition ) )
	{
		return;
	}

	SetPosition( newPosition );
}

bool Transform::CheckForIntersectionAtPosition( const sf::Vector2f& position, bool ignoreTriggers, std::vector<Collision>& outCollisions ) const
{

	if ( _associatedGameObject == nullptr || _associatedGameObject->HasCollider() == false )
	{
		return false;
	}

	Collider* thisCollider = _associatedGameObject->GetCollider();
	if ( thisCollider->IsTrigger() )
	{
		return false;
	}

	GameScene* scene = Game::GetInstance().GetCurrentScene();
	std::vector<GameObject*> gameObjects = scene->GetGameObjects();

	bool collisionFound = false;

	for ( int i = 0; i < gameObjects.size(); i++ )
	{
		GameObject* gameObject = gameObjects[i];
		if ( gameObject == _associatedGameObject || gameObject->HasCollider() == false )
		{
			continue;
		}

		Collider* collider = gameObject->GetCollider();
		if ( ignoreTriggers && collider->IsTrigger() )
		{
			continue;
		}

		Collision collision;
		if ( thisCollider->CheckIntersectionAtPosition( *collider, position, collision ) )
		{
			if ( collider->IsTrigger() == false )
			{
				outCollisions.push_back( collision );
			}
			collisionFound = true;
		}
	}

	return collisionFound;
}

sf::Transformable Transform::GetSfmlTransformable() const
{
	return _sfmlTransform;
}

sf::Transform Transform::GetSfmlTransform() const
{
	return _sfmlTransform.getTransform();
}

bool Transform::CheckForSolidCollisionAtPosition( const sf::Vector2f& position ) const
{
	if ( _associatedGameObject == nullptr || _associatedGameObject->HasCollider() == false )
	{
		return false;
	}

	Collider* collider = _associatedGameObject->GetCollider();

	std::vector<Collision> collisions;
	if ( CheckForIntersectionAtPosition( position, false, collisions ) )
	{
		_associatedGameObject->GetCollider()->_currentCollisions.insert( _associatedGameObject->GetCollider()->_currentCollisions.end(), collisions.begin(), collisions.end() );

		// Check if any are solid.
		for ( int i = 0; i < collisions.size(); i++ )
		{
			if( collisions[i].OtherCollider->IsTrigger() == false )
			{
				return true;
			}
		}
	}

	return false;
}

void Transform::UpdateParentGameObject() const
{
	if(_associatedGameObject != nullptr )
	{
		_associatedGameObject->CalculateBoundingBox();

		if ( _associatedGameObject->HasCollider() )
		{
			_associatedGameObject->GetCollider()->RecalculateBounds();
		}
	}
}

bool operator==( const Transform& lhs, const Transform& rhs )
{
	if (lhs._sfmlTransform.getPosition() != rhs._sfmlTransform.getPosition())
	{
		return false;
	}

	if (lhs._sfmlTransform.getRotation() != rhs._sfmlTransform.getRotation())
	{
		return false;
	}

	if (lhs._sfmlTransform.getScale() != rhs._sfmlTransform.getScale())
	{
		return false;
	}

	if (lhs._sfmlTransform.getOrigin() != rhs._sfmlTransform.getOrigin())
	{
		return false;
	}

	return true;
}

bool operator!=( const Transform& lhs, const Transform& rhs )
{
	return !( lhs == rhs );
}
