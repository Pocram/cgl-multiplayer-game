﻿#ifndef COMMONS_GAME_TRANSFORM_H
#define COMMONS_GAME_TRANSFORM_H

#include <SFML/Graphics.hpp>
#include "Physics/Collider.hpp"

class GameObject;

////////////////////////////////////////////////////////////
/// \brief The Transform component is responsible for managing a GameObject's positioning, orientation and scale.
////////////////////////////////////////////////////////////
class Transform
{
public:
	////////////////////////////////////////////////////////////
	/// \brief The default constructor.
	///
	/// If possible, use Transform( GameObject* owiningObject ) instead.
	////////////////////////////////////////////////////////////
	Transform();

	////////////////////////////////////////////////////////////
	/// \brief Sets up the Transform component an dlinks it up with a GameObject.
	///
	/// \param owningObject The GameObject this component belongs to.
	///
	/// \see SetParentGameObject() // TODO: Add this method
	////////////////////////////////////////////////////////////
	Transform( GameObject* owningObject );

	////////////////////////////////////////////////////////////
	/// \brief The Deconstructor.
	////////////////////////////////////////////////////////////
	~Transform();

	////////////////////////////////////////////////////////////
	/// \brief Returns the current position in world space.
	///
	/// \returns The current position in world space.
	////////////////////////////////////////////////////////////
	sf::Vector2f GetPosition() const;

	////////////////////////////////////////////////////////////
	/// \brief Returns the current rotation in degrees.
	///
	/// \returns The current rotation in degrees.
	////////////////////////////////////////////////////////////
	float GetRotation() const;

	////////////////////////////////////////////////////////////
	/// \brief Returns the current scale.
	///
	/// \returns The current scale.
	////////////////////////////////////////////////////////////
	sf::Vector2f GetScale() const;

	////////////////////////////////////////////////////////////
	/// \brief Returns the origin in local space.
	///
	/// The Transform's origin works like an offset in local space.
	/// Offsetting an object by half its size effectively sets the object's visual center as its position.	
	///
	/// \returns The origin in local space.
	////////////////////////////////////////////////////////////
	sf::Vector2f GetOrigin() const;

	////////////////////////////////////////////////////////////
	/// \brief Sets the current position in world space.
	///
	/// \param newPosition The new position in world space.
	////////////////////////////////////////////////////////////
	void SetPosition( const sf::Vector2f& newPosition );

	////////////////////////////////////////////////////////////
	/// \brief Sets the current rotation.
	///
	/// \param newRotation The new rotation in degrees.
	////////////////////////////////////////////////////////////
	void SetRotation( const float& newRotation );

	////////////////////////////////////////////////////////////
	/// \brief Sets the current scale.
	///
	/// \param newScale The new scale.
	////////////////////////////////////////////////////////////
	void SetScale( const sf::Vector2f& newScale );

	////////////////////////////////////////////////////////////
	/// \brief Sets the current origin.
	///
	/// The Transform's origin works like an offset in local space.
	/// Offsetting an object by half its size effectively sets the object's visual center as its position.
	///
	/// \param newOrigin The new origin in local space.
	////////////////////////////////////////////////////////////
	void SetOrigin( const sf::Vector2f& newOrigin );

	////////////////////////////////////////////////////////////
	/// \brief Moves the object in a direction.
	///
	/// \param direction The direction to move in.
	////////////////////////////////////////////////////////////
	void Translate( const sf::Vector2f& direction );

	////////////////////////////////////////////////////////////
	/// \brief Checks whether the GameObject would collide with any Collider at the given position.
	///
	/// \param position The GameObject's position to check. World Space.
	////////////////////////////////////////////////////////////
	bool CheckForIntersectionAtPosition( const sf::Vector2f& position, bool ignoreTriggers, std::vector<Collision>& outCollisions ) const;

	////////////////////////////////////////////////////////////
	/// \brief Returns the underlying sf::transformable component.
	///
	/// \returns The underlying sf::transformable component.
	////////////////////////////////////////////////////////////
	sf::Transformable GetSfmlTransformable() const;

	////////////////////////////////////////////////////////////
	/// \brief Retuns the sf::transform component of the sf::transformable.
	///
	/// This method wraps Transform::GetSfmlTransformable()::getTransform().
	///
	/// \returns The sf::transformable's sf::transform component.
	////////////////////////////////////////////////////////////
	sf::Transform GetSfmlTransform() const;

	friend bool operator==( const Transform& lhs, const Transform& rhs );

	friend bool operator!=( const Transform& lhs, const Transform& rhs );

private:
	bool CheckForSolidCollisionAtPosition( const sf::Vector2f& position ) const;
	void UpdateParentGameObject() const;

	sf::Transformable _sfmlTransform;	//!< The underlying SFML transform component.
	GameObject* _associatedGameObject;	//!< The GameObject that owns this transform.
};

#endif // COMMONS_GAME_TRANSFORM_H