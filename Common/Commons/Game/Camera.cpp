﻿#ifndef CAMERA_DRAW_COLLIDERS
#define CAMERA_DRAW_COLLIDERS
#endif // CAMERA_DRAW_COLLIDERS

#include "Camera.hpp"

#include <Commons/Game/Game.hpp>
#include <Commons/Console/Console.hpp>

#include <sstream>

class GameScene;

Camera::Camera()
{
	WindowDimensions gameWindowDimensions = Game::GetInstance().GetWindowDimensions();
	GetTransform().SetOrigin( sf::Vector2f( gameWindowDimensions.WindowWidth / 2.0f, gameWindowDimensions.WindowHeight / 2.0f ) );
}

Camera::~Camera()
{
}

void Camera::Update( sf::Time elapsedTime )
{
}

void Camera::Render( sf::RenderWindow& targetWindow )
{
	GameScene* currentScene = Game::GetInstance().GetCurrentScene();
	if ( currentScene == nullptr )
	{
		return;
	}

	for( std::vector<GameObject*>::const_iterator it = currentScene->GetGameObjects().begin(); it != currentScene->GetGameObjects().end(); ++it )
	{
		GameObject* gameObject = *it;
		if ( gameObject->IsActive() )
		{
			if( GetBounds().intersects( gameObject->GetBounds() ) )
			{
				gameObject->DrawToWindow( targetWindow, this );	
			}
		}
	}

#ifdef CAMERA_DRAW_COLLIDERS
	for ( std::vector<GameObject*>::const_iterator it = currentScene->GetGameObjects().begin(); it != currentScene->GetGameObjects().end(); ++it )
	{
		GameObject* gameObject = *it;
		if ( gameObject->IsActive() )
		{
			if ( GetBounds().intersects( gameObject->GetBounds() ) )
			{
				if( gameObject->HasCollider() )
				{
					gameObject->GetCollider()->DrawToWindow( targetWindow, this );
				}
			}
		}
	}
#endif
}

Transform Camera::LocalTransformToCameraTransform( Transform& localTransform )
{
	Transform outTransform = localTransform;
	sf::Vector2f newPosition = localTransform.GetPosition();
	newPosition.x -= GetTransform().GetPosition().x;
	newPosition.y -= GetTransform().GetPosition().y;
	outTransform.SetPosition( newPosition );
	return outTransform;
}

void Camera::CalculateBoundingBox()
{
	WindowDimensions gameWindowDimensions = Game::GetInstance().GetWindowDimensions();
	sf::FloatRect newBounds;
	newBounds.width = gameWindowDimensions.WindowWidth;
	newBounds.height = gameWindowDimensions.WindowHeight;
	newBounds.left = GetTransform().GetPosition().x;
	newBounds.top = GetTransform().GetPosition().y;
	SetBoundingBox( newBounds );
}
