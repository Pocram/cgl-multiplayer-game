﻿#ifndef COMMONS_GAME_CAMERA_H
#define COMMONS_GAME_CAMERA_H

#include <Commons/Game/GameObject.hpp>

class Camera : public GameObject
{
public:
	Camera();
	~Camera();

	void Update( sf::Time elapsedTime ) override;
	void Render( sf::RenderWindow& targetWindow );

	Transform LocalTransformToCameraTransform( Transform& localTransform );
	
	void CalculateBoundingBox() override;
};

#endif // COMMONS_GAME_CAMERA_H