﻿#ifndef COMMONS_GAME_GAME_OBJECT_H
#define COMMONS_GAME_GAME_OBJECT_H

#include <SFML/Graphics.hpp>

#include <Commons/Game/Interfaces/IUpdateable.hpp>
#include <Commons/Game/Interfaces/IDrawable.hpp>
#include <Commons/Game/Transform.hpp>
#include <Commons/Game/Physics/Collider.hpp>

#include <string>
#include <vector>

////////////////////////////////////////////////////////////
/// \brief An object in a scene.
///
/// A GameObject is updated and can be drawn every frame.
/// It can represent a character, an object or a logic container.
////////////////////////////////////////////////////////////
class GameObject : public IUpdateable, public IDrawable
{
	friend GameScene;
	friend Collider;

public:
	// CONSTRUCTORS
	//====================================================================================================
	////////////////////////////////////////////////////////////
	/// \brief Default Constructor.
	////////////////////////////////////////////////////////////
	GameObject();

	////////////////////////////////////////////////////////////
	/// \brief Destructor.
	////////////////////////////////////////////////////////////
	virtual ~GameObject();

	// METHODS
	//====================================================================================================
	////////////////////////////////////////////////////////////
	/// \brief Updates the object.
	///
	/// Update() is called every frame.
	///
	/// \param elapsedTime The elapsed time since the last update.
	////////////////////////////////////////////////////////////
	virtual void Update( sf::Time elapsedTime ) override;

	////////////////////////////////////////////////////////////
	/// \brief Draws the object to a window.
	///
	/// DrawToWindow() is called after every Update() cycle.
	///
	/// \param targetWindow	The window the object should be drawn to.
	/// \param camera		The camera that draws the object.
	////////////////////////////////////////////////////////////
	virtual void DrawToWindow( sf::RenderWindow& targetWindow, Camera* camera ) override;

	////////////////////////////////////////////////////////////
	/// \brief Gets the GameObject's Transform component.
	///
	/// The Transform component handles positioning, rotation and scale of the GameObject.
	////////////////////////////////////////////////////////////
	Transform& GetTransform();

	////////////////////////////////////////////////////////////
	/// \brief Gets the GameObject's name.
	///
	/// A name can be used to identify the GameObject.
	///
	/// \returns A constant reference to the GameObject's name.
	////////////////////////////////////////////////////////////
	const std::string& GetName() const;

	////////////////////////////////////////////////////////////
	/// \brief Sets the GameObject's name.
	///
	/// A name can be used to identify the GameObject.
	///
	/// \param name The new name of the GameObject.
	////////////////////////////////////////////////////////////
	void SetName( const std::string& name );

	////////////////////////////////////////////////////////////
	/// \brief Gets the GameObject's bounds.
	///
	/// The bounds are used when drawing the GameObject.
	/// The Camera only renders a GameObject when their bounds intersect.
	///
	/// \returns The bounds.
	///
	/// \see GameObject::SetBounds()
	////////////////////////////////////////////////////////////
	sf::FloatRect GetBounds() const;

	////////////////////////////////////////////////////////////
	/// \brief Sets whether the GameObject is active or not.
	///
	/// An inactive GameObject will not be updated or drawn to the window.
	///
	/// \param isActive Whether the GameObject should be active or not.
	////////////////////////////////////////////////////////////
	void SetActive( bool isActive );

	////////////////////////////////////////////////////////////
	/// \brief Gets whether the GameObject is active or not.
	///
	/// An inactive GameObject will not be updated or drawn to the window.
	////////////////////////////////////////////////////////////
	bool IsActive() const;

	////////////////////////////////////////////////////////////
	/// \brief Sets the draw order of the GameObject.
	///
	/// Objects with a higher draw order index will be drawn on top of others.
	///
	/// \param newDrawOrderIndex The new index of the GameObject's draw order.
	////////////////////////////////////////////////////////////
	void SetDrawOrderIndex( int newDrawOrderIndex );

	////////////////////////////////////////////////////////////
	/// \brief Gets the draw order of the GameObject.
	///
	/// Objects with a higher draw order index will be drawn on top of others.
	///
	/// \returns The draw order index.
	////////////////////////////////////////////////////////////
	int GetDrawOrderIndex() const;

	////////////////////////////////////////////////////////////
	/// \brief Recalculates the GameObject's bounding box.
	///
	/// The bounding box is recalculated every time the GameObject's Transform changes.
	///
	/// \see GetTransform()
	////////////////////////////////////////////////////////////
	virtual void CalculateBoundingBox();

	////////////////////////////////////////////////////////////
	/// \brief Gets the GameObject's collider.
	///
	/// Colliders are used for detecting collisions between GameObjects.
	/// Each GameObject may only hold one Collider.
	///
	/// \returns A pointer to the GameObject's collider.
	///
	/// \see GameObject::HasCollider()
	/// \see GameObject::SetCollider()
	////////////////////////////////////////////////////////////
	Collider* GetCollider() const;

	////////////////////////////////////////////////////////////
	/// \brief Checks whether the GameObject has a Collider or not.
	///
	/// \see GameObject::GetCollider()
	/// \see GameObject::SetCollider()
	////////////////////////////////////////////////////////////
	bool HasCollider() const;

	////////////////////////////////////////////////////////////
	/// \brief Adds a tag to a GameObject.
	///
	/// Tags can help identifying a GameObject's properties.
	/// A tag is unique per GameObject. Adding an already existing tag to a GameObject simply does not work.
	///
	/// \parameter tag The tag to add.
	///
	/// \see GameObject::HasTag()
	/// \see GameObject::RemoveTag()
	////////////////////////////////////////////////////////////
	void AddTag( const std::string& tag );

	////////////////////////////////////////////////////////////
	/// \brief Removes a tag from a GameObject.
	///
	/// You do not need to call GameObject::HasTag() before calling this method.
	///
	/// \parameter tag The specific tag to remove.
	///
	/// \see GameObject::AddTag()
	/// \see GameObject::HasTag()
	////////////////////////////////////////////////////////////
	void RemoveTag( const std::string& tag );

	////////////////////////////////////////////////////////////
	/// \brief Checks whether the GameObject has a specific tag.
	///
	/// \parameter tag The specific tag to check for.
	///
	/// \returns Whether the GameObject has the specified tag.
	///
	/// \see GameObject::AddTag()
	/// \see GameObject::RemoveTag()
	////////////////////////////////////////////////////////////
	bool HasTag( const std::string& tag );

	static bool CompareDrawOrder( GameObject* lhs, GameObject* rhs );


protected:
	// METHODS
	//====================================================================================================
	////////////////////////////////////////////////////////////
	/// \brief Calculates the GameObject's bounds to fit a sprite.
	///
	/// \param sprite The sprite to fit to.
	////////////////////////////////////////////////////////////
	void CalculateBoundingBoxFromSprite( sf::Sprite sprite );

	////////////////////////////////////////////////////////////
	/// \brief Sets the GameObject's bodunds.
	///
	/// The bounds are used when drawing the GameObject.
	/// The Camera only renders a GameObject when their bounds intersect.
	///
	/// \param bounds The bounds.
	///
	/// \see GameObject::GetBounds()
	////////////////////////////////////////////////////////////
	void SetBoundingBox( sf::FloatRect bounds );

	////////////////////////////////////////////////////////////
	/// \brief Gets the GameObject's collider.
	///
	/// Colliders are used for detecting collisions between GameObjects.
	/// Each GameObject may only hold one Collider.
	///
	/// \param collider The pointer to the collider.
	///
	/// \see GameObject::GetCollider()
	/// \see GameObject::HasCollider()
	////////////////////////////////////////////////////////////
	void SetCollider( Collider* collider );

	virtual void OnCollisionEnter( Collision collision );
	virtual void OnCollisionStay( Collision collision );
	virtual void OnCollisionExit( Collision collision );

	virtual void OnInstantiate();
	virtual void OnSceneStart();
	virtual void OnDestroy();


private:
	// METHODS
	//====================================================================================================
	void SetRandomObjectName();

	// MEMBERS
	//====================================================================================================
	bool _isActive;					//!< Whether the GameObject is active or not.
	std::string _name;				//!< The GameObject's name.
	Transform _transform;			//!< The GameObject's Transform component. It is rensponsible for positioning, rotation and scaling.
	std::vector<std::string> _tags;	//!< The GameObject's tags. Each tag is unique.
	sf::FloatRect _boundRect;		//!< The GameObject's bounds that encapsulate it. Used for rendering.
	int _drawOrderIndex;			//!< The GameObject's draw order index. Higher indeces are rendered on top of others.
	Collider* _collider;			//!< The GameObject's collider. Used for physics.
};

#endif // COMMONS_GAME_GAME_OBJECT_H