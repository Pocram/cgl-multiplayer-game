﻿#define _USE_MATH_DEFINES

#include "SfmlVectorMath.hpp"
#include <Commons/Math/Line.hpp>

#include <math.h>

float sf::VectorMath::Magnitude2f( const sf::Vector2f & vector )
{
	return sqrt( SquaredMagnitude2f( vector ) );
}

float sf::VectorMath::SquaredMagnitude2f( const sf::Vector2f & vector )
{
	return pow( vector.x, 2 ) + pow( vector.y, 2 );
}

float sf::VectorMath::Distance( const sf::Vector2f& a, const sf::Vector2f& b )
{
	sf::Vector2f d = a - b;
	return Magnitude2f( d );
}

sf::Vector2f sf::VectorMath::GetNormalized( const sf::Vector2f& vector )
{
	sf::Vector2f v( vector );
	float length = Magnitude2f( vector );

	if ( length == 0.0f )
	{
		return vector;
	}

	v.x = vector.x / length;
	v.y = vector.y / length;
	return v;
}

sf::Vector2f sf::VectorMath::GetVectorFromAngle( float angle )
{
	float timeValue = ( angle / 360.0f ) * 2.0f * M_PI;
	return sf::Vector2f( sin( timeValue ), cos( timeValue ) );
}

sf::Vector2f sf::VectorMath::ReflectAlongSurface( const sf::Vector2f& incomingVector, const sf::Vector2f& surfaceNormal )
{
	sf::Vector2f normal = sf::VectorMath::GetNormalized( surfaceNormal );
	return incomingVector - 2.0f * ( sf::VectorMath::DotProduct( incomingVector, normal ) * normal );
}

sf::Vector2f sf::VectorMath::GetRotation90DegreesClockWise( const sf::Vector2f& vector )
{
	return sf::Vector2f( vector.y, -vector.x );
}

sf::Vector2f sf::VectorMath::GetRotation90DegreesCounterClockWise( const sf::Vector2f& vector )
{
	return sf::Vector2f( -vector.y, vector.x );
}

sf::Vector2f sf::VectorMath::GetMean( const sf::Vector2f& a, const sf::Vector2f& b )
{
	return ( a + b ) / 2.0f;
}

float sf::VectorMath::DotProduct( const sf::Vector2f& a, const sf::Vector2f& b )
{
	return a.x * b.x + a.y * b.y;
}

float sf::VectorMath::DistanceOfPointToLine( const sf::Vector2f& lineStart, const sf::Vector2f& lineEnd, const sf::Vector2f& point, sf::Vector2f& outClosestPointOnLine )
{
	// Algorithm courtesy of http://geomalgorithms.com/a02-_lines.html

	sf::Vector2f v = lineEnd - lineStart;
	sf::Vector2f w = point - lineStart;

	float c1 = sf::VectorMath::DotProduct( w, v );
	if ( c1 <= 0.0f )
	{
		return sf::VectorMath::Distance( point, lineStart );
	}

	float c2 = sf::VectorMath::DotProduct( v, v );
	if ( c2 <= c1 )
	{
		return sf::VectorMath::Distance( point, lineEnd );
	}

	float b = c1 / c2;
	sf::Vector2f pb = lineStart + b * v;
	outClosestPointOnLine = pb;
	return sf::VectorMath::Distance( point, pb );
}

bool sf::VectorMath::DoLinesIntersect( const sf::Vector2f& lineAStart, const sf::Vector2f& lineAEnd, const sf::Vector2f& lineBStart, const sf::Vector2f& lineBEnd, sf::Vector2f& outIntersectionPoint )
{
	// Courtesy of http://gamedev.stackexchange.com/a/24394

	sf::Vector2f s1 = lineAEnd - lineAStart;
	sf::Vector2f s2 = lineBEnd - lineBStart;

	sf::Vector2f u = lineAStart - lineBStart;

	float ip = 1.f / ( -s2.x * s1.y + s1.x * s2.y );

	float s = ( -s1.y * u.x + s1.x * u.y ) * ip;
	float t = ( s2.x * u.y - s2.y * u.x ) * ip;

	if ( s >= 0 && s <= 1 && t >= 0 && t <= 1 )
	{
		outIntersectionPoint = lineAEnd + ( s1 * t );
		return true;
	}

	return false;
}

bool sf::VectorMath::DoLinesIntersect( const Line& lineA, const Line& lineB, sf::Vector2f& outIntersectionPoint )
{
	return DoLinesIntersect( lineA.GetStart(), lineA.GetEnd(), lineB.GetStart(), lineB.GetEnd(), outIntersectionPoint );
}
