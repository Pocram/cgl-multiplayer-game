﻿#include "Line.hpp"
#include "SfmlVectorMath.hpp"

Line::Line()
{
	_startPoint = sf::Vector2f( 0.0f, 0.0f );
	_endPoint = sf::Vector2f( 0.0f, 0.0f );
}

Line::Line( sf::Vector2f start, sf::Vector2f end )
{
	SetStart( start );
	SetEnd( end );
}

Line::~Line()
{
}

void Line::SetStart( const sf::Vector2f& start )
{
	_startPoint = start;
}

void Line::SetEnd( const sf::Vector2f& end )
{
	_endPoint = end;
}

void Line::SetLength( float length )
{
	sf::Vector2f directionToEnd = sf::VectorMath::GetNormalized( GetEnd() - GetStart() );
	SetEnd( GetStart() + directionToEnd * length );
}

const sf::Vector2f& Line::GetStart() const
{
	return _startPoint;
}

const sf::Vector2f& Line::GetEnd() const
{
	return _endPoint;
}

float Line::GetLength() const
{
	return sf::VectorMath::Magnitude2f( GetEnd() - GetStart() );
}

sf::Vector2f Line::GetDirection() const
{
	return _endPoint - _startPoint;
}
