﻿#ifndef COMMONS_MATH_LINE_H

#include <SFML/Graphics.hpp>

class Line
{
public:
	Line();
	Line( sf::Vector2f start, sf::Vector2f end );
	~Line();

	void SetStart( const sf::Vector2f& start );
	void SetEnd( const sf::Vector2f& end );

	void SetLength( float length );

	const sf::Vector2f& GetStart() const;
	const sf::Vector2f& GetEnd() const;

	float GetLength() const;

	sf::Vector2f GetDirection() const;


private:
	sf::Vector2f _startPoint;
	sf::Vector2f _endPoint;
};

#endif // COMMONS_MATH_LINE_H