﻿#ifndef COMMONS_MATH_MATH_H
#define COMMONS_MATH_MATH_H

#include <type_traits>

namespace Math
{
	template<typename T, typename = typename std::enable_if<std::is_arithmetic<T>::value, T>::type>
	T Clamp01( const T& value );
}


template<typename T, typename = typename std::enable_if<std::is_arithmetic<T>::value, T>::type>
T Math::Clamp01( const T& value )
{
	if( value < 0 )
	{
		return 0;
	}
	else if ( value > 1 )
	{
		return 1;
	}

	return value;
}

#endif // COMMONS_MATH_MATH_H
