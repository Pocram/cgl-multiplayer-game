﻿#ifndef COMMONS_MATH_SFML_VECTOR_MATH_H
#define COMMONS_MATH_SFML_VECTOR_MATH_H

#include <SFML/Graphics.hpp>

class Line;

namespace sf
{
	namespace VectorMath
	{
		float Magnitude2f( const sf::Vector2f& vector );
		float SquaredMagnitude2f( const sf::Vector2f& vector );

		float Distance( const sf::Vector2f& a, const sf::Vector2f& b );

		sf::Vector2f GetNormalized( const sf::Vector2f& vector );

		sf::Vector2f GetVectorFromAngle( float angle );

		sf::Vector2f ReflectAlongSurface( const sf::Vector2f& incomingVector, const sf::Vector2f& surfaceNormal );

		sf::Vector2f GetRotation90DegreesClockWise( const sf::Vector2f& vector );
		sf::Vector2f GetRotation90DegreesCounterClockWise( const sf::Vector2f& vector );

		sf::Vector2f GetMean( const sf::Vector2f& a, const sf::Vector2f& b );

		float DotProduct( const sf::Vector2f& a, const sf::Vector2f& b );

		float DistanceOfPointToLine( const sf::Vector2f& lineStart, const sf::Vector2f& lineEnd, const sf::Vector2f& point, sf::Vector2f& outClosestPointOnLine );

		bool DoLinesIntersect( const sf::Vector2f& lineAStart, const sf::Vector2f& lineAEnd, const sf::Vector2f& lineBStart, const sf::Vector2f& lineBEnd, sf::Vector2f& outIntersectionPoint );
		bool DoLinesIntersect( const Line& lineA, const Line& lineB, sf::Vector2f& outIntersectionPoint );
	}
}

#endif // COMMONS_MATH_SFML_VECTOR_MATH_H