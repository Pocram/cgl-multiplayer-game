﻿#ifndef COMMONS_NETWORKING_TCP_CLIENT_H
#define COMMONS_NETWORKING_TCP_CLIENT_H

#define WIN32_LEAN_AND_MEAN

#include <WinSock2.h>
#include <WS2tcpip.h>

#include <stdio.h>
#include <sstream>

#include <Commons/Networking/NetworkThreads/TcpClientConnectThread.hpp>
#include <Commons/Networking/SocketConnection.hpp>
#include <Commons/Networking/Exceptions/ClientException.hpp>
#include <Commons/Errors/ErrorMessageUtility.h>
#include <Commons/Console/Console.hpp>

struct TcpClientInfo
{
	std::string ServerAddress;			//!< The server's IP address.
	int PortNumber;						//!< The port's number.
	struct addrinfo* AddressInfoOutput;	//!< The addressInfo used for the client.
	SOCKET ClientSocket;				//!< The socket used to connect to a server.
};

template <class ClientType>
class TcpClient
{
	// Limit ClientType to subclasses of SocketConnection
	static_assert( std::is_base_of<SocketConnection, ClientType>::value, "ClientType must be a descendant of SocketConnection!" );

public:
	////////////////////////////////////////////////////////////
	/// \brief Default constructor.
	////////////////////////////////////////////////////////////
	TcpClient();

	////////////////////////////////////////////////////////////
	/// \brief Default destructor.
	////////////////////////////////////////////////////////////
	virtual ~TcpClient();


	// METHODS
	//====================================================================================================
	////////////////////////////////////////////////////////////
	/// \brief Sets up the client for a specific port.
	///
	/// Call this method before using TcpClient::StartClient()!
	///
	/// \param portNumber The portnumber. For example "20000". Can probably also be a name?
	////////////////////////////////////////////////////////////
	void SetUp( std::string serverAddress, int portNumber );
	
	////////////////////////////////////////////////////////////
	/// \brief Starts the client's connection thread.
	///
	/// Starts a new thread that looks for a matching server.
	/// Call TcpClient::SetUp() first!
	////////////////////////////////////////////////////////////
	void StartClient();

	void SendMessageToServer( const std::string& message );


protected:
	virtual void OnMessageReceive( std::string message, SocketConnection& socketConnection );


private:
	// METHODS
	//====================================================================================================
	void OnServerConnect();
	
	void SetUpAddressInfo();
	void CreateSocket();

	// MEMBERS
	//====================================================================================================
	bool _isSetUp;									//!< Whether the client has been SetUp() successfully.
	TcpClientInfo _clientInfo;						//!< Detailed information about the client.
	TcpClientConnectThread _tcpClientConnectThread;	//!< The thread used to find a connection to a server.

	ClientType _connectionToServer;

};

template <class ClientType>
TcpClient<ClientType>::TcpClient()
{
	_isSetUp = false;

	_clientInfo.ServerAddress = "127.0.0.1";
	_clientInfo.PortNumber = 0;
}

template <class ClientType>
TcpClient<ClientType>::~TcpClient()
{
	_connectionToServer.UnsubscribeFromMessageReceiveCallback();

	freeaddrinfo( _clientInfo.AddressInfoOutput );
	closesocket( _clientInfo.ClientSocket );
}

template <class ClientType>
void TcpClient<ClientType>::SetUp( std::string serverAddress, int portNumber )
{
	_clientInfo.ServerAddress = serverAddress;
	_clientInfo.PortNumber = portNumber;

	SetUpAddressInfo();

	try
	{
		CreateSocket();
	}
	catch ( ClientException e )
	{
		printf( "ERROR: %s.\n", e.what() );
		return;
	}

	_isSetUp = true;
}

template <class ClientType>
void TcpClient<ClientType>::StartClient()
{
	_tcpClientConnectThread.Create( &_clientInfo, std::bind( &TcpClient::OnServerConnect, this ) );
	_tcpClientConnectThread.StartConnecting();
}

template <class ClientType>
void TcpClient<ClientType>::SendMessageToServer( const std::string& message )
{
	//_connectionToServer.EnableSending();
	_connectionToServer.SendMessageToServer( message );
}

template <class ClientType>
void TcpClient<ClientType>::OnMessageReceive( std::string message, SocketConnection& socketConnection )
{
}

template <class ClientType>
void TcpClient<ClientType>::OnServerConnect()
{
	_connectionToServer = SocketConnection( _clientInfo.ClientSocket );
	_connectionToServer.SubscribeToMessageReceiveCallback( std::bind( &TcpClient::OnMessageReceive, this, std::placeholders::_1, std::placeholders::_2 ) );
	_connectionToServer.StartConnection();
	_connectionToServer.EnableSending();
}

template <class ClientType>
void TcpClient<ClientType>::SetUpAddressInfo()
{
	struct addrinfo addressInfoInput;

	ZeroMemory( &addressInfoInput, sizeof( addressInfoInput ) );
	ZeroMemory( &_clientInfo.AddressInfoOutput, sizeof( _clientInfo.AddressInfoOutput ) );

	addressInfoInput.ai_family = AF_INET;
	addressInfoInput.ai_socktype = SOCK_STREAM;
	addressInfoInput.ai_protocol = IPPROTO_TCP;

	std::string portNumber = std::to_string( _clientInfo.PortNumber );
	getaddrinfo( nullptr, portNumber.c_str(), &addressInfoInput, &_clientInfo.AddressInfoOutput );
}

template <class ClientType>
void TcpClient<ClientType>::CreateSocket()
{
	_clientInfo.ClientSocket = socket( AF_INET, SOCK_STREAM, IPPROTO_TCP );
	if ( _clientInfo.ClientSocket == INVALID_SOCKET )
	{
		freeaddrinfo( _clientInfo.AddressInfoOutput );

		std::stringstream errorStream;
		errorStream << "Could not create socket for TcpClient at line " << __LINE__ << " in " << __FILENAME__;
		throw ClientException( errorStream.str() );
	}
}

#endif // COMMONS_NETWORKING_TCP_CLIENT_H
