﻿#include "SocketConnection.hpp"

#include <algorithm>
#include <vector>
#include <Commons/Networking/TcpServer.hpp>
#include <iostream>
#include <Commons/Console/Console.hpp>

SocketConnection::SocketConnection()
{
	_isDoingReceiveLoop = false;
	_isDoingSendLoop = false;
	_assiociatedSocket = 0;
	_receiveBufferSize = 1024;
	_receiveCallInterval = 5;
	_sendCallInterval = 5;

	_mainReceiveBuffer.reserve( 4096 * 2 );

	_onMessageReceiveCallback = nullptr;
}

SocketConnection::SocketConnection( SOCKET associatedSocket )
{
	_isDoingReceiveLoop = false;
	_isDoingSendLoop = false;
	_assiociatedSocket = 0;
	_receiveBufferSize = 1024;
	_receiveCallInterval = 5;
	_sendCallInterval = 5;

	_mainReceiveBuffer.reserve( 4096 * 2 );

	_onMessageReceiveCallback = nullptr;

	Create( associatedSocket );
}

SocketConnection::~SocketConnection()
{
	_isDoingReceiveLoop = false;
}

void SocketConnection::StartConnection()
{
	_isDoingReceiveLoop = true;
	StartThread();
}

void SocketConnection::EnableSending()
{
	_isDoingSendLoop = true;
	//StartThreadB();
}

void SocketConnection::Create( SOCKET associatedSocket )
{
	_assiociatedSocket = associatedSocket;
}

void SocketConnection::SetReceiverBufferSize( int newBufferSize )
{
	_receiveBufferSize = newBufferSize;
}

void SocketConnection::SetReceiveInterval( int millisecondsBetweenReceiveCalls )
{
	_receiveCallInterval = millisecondsBetweenReceiveCalls;
}

void SocketConnection::SetSendInterval( int millisecondsBetweenSendCalls )
{
	_sendCallInterval = millisecondsBetweenSendCalls;
}

bool SocketConnection::PopOldestReceivedMessage( std::string &outMessage )
{
	// If there is no message in the queue, return an empty string.
	if ( _receivedMessages.empty() )
	{
		return false;
	}

	// Has a message! Send it back and pop it.
	outMessage = _receivedMessages.front();
	_receivedMessages.pop();
	
	return true;
}

const SOCKET* SocketConnection::GetAssociatedSocket()
{
	return &_assiociatedSocket;
}

std::string SocketConnection::GetSocketAddress()
{
	SOCKADDR_IN clientInfo = { 0 };
	int clientInfoSize = sizeof( clientInfo );

	int getResult = getsockname( _assiociatedSocket, reinterpret_cast<struct sockaddr* >(&clientInfo), &clientInfoSize );
	if ( getResult != -1 )
	{
		std::string ip = inet_ntoa( clientInfo.sin_addr );
		return ip;
	}

	return "";
}

std::string SocketConnection::GetPeerAddress()
{
	SOCKADDR_IN clientInfo = { 0 };
	int clientInfoSize = sizeof( clientInfo );

	int getResult = getpeername( _assiociatedSocket, reinterpret_cast<struct sockaddr* >( &clientInfo ), &clientInfoSize );
	if ( getResult != -1 )
	{
		std::string ip = inet_ntoa( clientInfo.sin_addr );
		return ip;
	}

	return "";
}

void SocketConnection::ThreadMain()
{
	while ( _isDoingReceiveLoop )
	{
		// Create buffer of dynamic size.
		std::vector<char> buffer;
		buffer.resize( _receiveBufferSize );
		
		// Receive a message from the client.
		int receivedByteAmount = recv( _assiociatedSocket, &buffer[0], buffer.size(), 0 );
		if( receivedByteAmount > 0 )
		{
			// Convert the vector to a string for ease of use.
			std::string receivedMessage( buffer.begin(), buffer.end() );
			// Remove all null terminators.
			int positionOfFirstNullTerminator = receivedMessage.find( '\0' );
			if( positionOfFirstNullTerminator != std::string::npos )
			{
				receivedMessage = receivedMessage.substr( 0, positionOfFirstNullTerminator );
			}

			// Queue the received message into the message queue and let any child classes know.
			_mainReceiveBuffer.append( receivedMessage );
			MoveFinishedMessagesFromBufferToQueue();
		}

		// Briefly pause the thread to reduce CPU load.
		Sleep( _receiveCallInterval );
	}
}

//void SocketConnection::ThreadMainB()
//{
//	while ( _isDoingSendLoop )
//	{
//		// Only send something if there is something to send.
//		if( _sendMessageQueue.empty() == false )
//		{
//			// Get the oldest message and try to send it.
//			const char* messageToSend = _sendMessageQueue.front().c_str();
//			int messageSize = strlen( messageToSend );
//			int sentByteAmount = send( _assiociatedSocket, messageToSend, messageSize, 0 );
//
//			// If the message could not be sent, don't pop it.
//			if( sentByteAmount != SOCKET_ERROR && _sendMessageQueue.empty() == false )
//			{
//				_sendMessageQueue.pop();
//			}
//		}
//
//		// Briefly pause the thread to reduce CPU load.
//		Sleep( _sendCallInterval );
//	}
//}

void SocketConnection::OnReceiveMessage( std::string message )
{
}

void SocketConnection::MoveFinishedMessagesFromBufferToQueue()
{
	while ( true )
	{
		int positionOfFirstDelimiter = _mainReceiveBuffer.find( '}' );
		if ( positionOfFirstDelimiter == std::string::npos )
		{
			break;
		}

		std::string fullMessage = _mainReceiveBuffer.substr( 0, positionOfFirstDelimiter + 1 );
		_mainReceiveBuffer = _mainReceiveBuffer.substr( positionOfFirstDelimiter + 1 );

		//std::stringstream debugMsg;
		//debugMsg << "-- Incoming network message: " << fullMessage.c_str() << " from " << _assiociatedSocket << " at " << GetPeerAddress() << "\n";
		//std::cout << debugMsg.str();

		_receivedMessages.push( fullMessage );
		OnReceiveMessage( fullMessage );
		if ( _onMessageReceiveCallback != nullptr )
		{
			_onMessageReceiveCallback( fullMessage, *this );
		}
	}
}

bool SocketConnection::SendMessageToServer( std::string message ) const
{
	const char* messageToSend = message.c_str();
	int messageLength = strlen( messageToSend );

	int totalBytesSent = 0;
	int bytesLeft = messageLength;

	int sendResult = 0;
	while( totalBytesSent < messageLength )
	{
		sendResult = send( _assiociatedSocket, messageToSend + totalBytesSent, bytesLeft, 0 );
		if ( sendResult == -1 )
		{
			break;
		}

		totalBytesSent += sendResult;
		bytesLeft -= sendResult;
	}

	return sendResult == -1 ? false : true;
}

void SocketConnection::SubscribeToMessageReceiveCallback( std::function<void(std::string message, SocketConnection& socketConnection)> callback )
{
	_onMessageReceiveCallback = callback;
}

void SocketConnection::UnsubscribeFromMessageReceiveCallback()
{
	_onMessageReceiveCallback = nullptr;
}
