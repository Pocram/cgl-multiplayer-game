#include "TcpServerListenThread.hpp"

#include <Commons/Networking/TcpServer.hpp>

#include <stdio.h>
#include <WinSock2.h>

TcpServerListenThread::TcpServerListenThread()
{
	_isDoingListenLoop = false;
	_associatedTcpServerInfo = {};
}

TcpServerListenThread::TcpServerListenThread(TcpServerInfo* associatedTcpServerInfo, std::function<void( SOCKET clientSocket )> onClientConnectCallback )
{
	_isDoingListenLoop = false;
	Create( associatedTcpServerInfo, onClientConnectCallback );
}

TcpServerListenThread::~TcpServerListenThread()
{
	_isDoingListenLoop = false;
}

void TcpServerListenThread::Create( TcpServerInfo* associatedTcpServerInfo, std::function<void( SOCKET client )> onClientConnectCallback )
{
	_associatedTcpServerInfo = associatedTcpServerInfo;
	_onClientConnectCallback = onClientConnectCallback;
}

void TcpServerListenThread::StartListening()
{
	// Start the thread
	_isDoingListenLoop = true;
	StartThread();
}

void TcpServerListenThread::StopListening()
{
	_isDoingListenLoop = false;
}

void TcpServerListenThread::ThreadMain()
{
	while ( _isDoingListenLoop )
	{
		if ( _associatedTcpServerInfo == nullptr )
		{
			break;
		}

		// Wait for incoming signal.
		SOCKET clientSocket = accept( _associatedTcpServerInfo->ListenSocket, nullptr, nullptr );

		if ( _isDoingListenLoop == false )
		{
			break;
		}

		// Check for any connection errors.
		if ( clientSocket == INVALID_SOCKET )
		{
			// If the connection failed, log an error to the console and continue the loop.
			printf( "A TCP server ran into an error when receiving a client. Ignoring connection.\n" );
			closesocket( clientSocket );
			continue;
		}

		// Let server know a client has been detected!
		if ( _onClientConnectCallback )
		{
			_onClientConnectCallback( clientSocket );
		}
		else
		{
			printf( "A client has been detected but the server is not listening to the listen thread!\n" );
		}

		// Pause the thread to reduce CPU load.
		Sleep( 5 );
	}
}
