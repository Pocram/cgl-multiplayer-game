﻿#include "TcpClientConnectThread.hpp"

#include <Commons/Networking/TcpClient.hpp>

#include <stdio.h>
#include <WinSock2.h>

TcpClientConnectThread::TcpClientConnectThread()
{
	_isDoingConnectLoop = false;
	_associatedTcpClientInfo = {};

	_serverConnectRetryInterval = 500;
}

TcpClientConnectThread::TcpClientConnectThread( TcpClientInfo* associatedTcpClientInfo, std::function<void()> onServerConnectCallback )
{
	_isDoingConnectLoop = false;
	_serverConnectRetryInterval = 500;
	Create( associatedTcpClientInfo, onServerConnectCallback );
}

TcpClientConnectThread::~TcpClientConnectThread()
{
	_isDoingConnectLoop = false;
}

void TcpClientConnectThread::Create( TcpClientInfo* associatedTcpClientInfo, std::function<void()> onServerConnectCallback )
{
	_associatedTcpClientInfo = associatedTcpClientInfo;
	_onServerConnectCallback = onServerConnectCallback;
}

void TcpClientConnectThread::StartConnecting()
{
	_isDoingConnectLoop = true;
	StartThread();
}

void TcpClientConnectThread::StopConnecting()
{
	_isDoingConnectLoop = false;
}

void TcpClientConnectThread::ThreadMain()
{
	while ( _isDoingConnectLoop )
	{
		if ( _associatedTcpClientInfo == nullptr )
		{
			break;
		}

		int connectionResult = SOCKET_ERROR;
		while ( connectionResult == SOCKET_ERROR )
		{
			// Try to connect to a server.
			struct sockaddr_in remoteaddr;
			remoteaddr.sin_family = AF_INET;
			remoteaddr.sin_addr.s_addr = inet_addr( _associatedTcpClientInfo->ServerAddress.c_str() );
			remoteaddr.sin_port = htons( _associatedTcpClientInfo->PortNumber );
			connectionResult = connect( _associatedTcpClientInfo->ClientSocket, reinterpret_cast<struct sockaddr *>( &remoteaddr ), sizeof( remoteaddr ) );
			//connectionResult = connect( _associatedTcpClientInfo->ClientSocket, _associatedTcpClientInfo->AddressInfoOutput->ai_addr, static_cast<int>( _associatedTcpClientInfo->AddressInfoOutput->ai_addrlen ) );
			if ( connectionResult != SOCKET_ERROR )
			{
				// Let client know a server has been detected!
				if ( _onServerConnectCallback )
				{
					_onServerConnectCallback();
					_isDoingConnectLoop = false;
				}
				else
				{
					printf( "A client has been detected but the server is not listening to the listen thread!\n" );
				}

				break;
			}
			else
			{
				printf( "Could not find a server, retrying ...\n" );
			}

			Sleep( _serverConnectRetryInterval );
		}
	}
}
