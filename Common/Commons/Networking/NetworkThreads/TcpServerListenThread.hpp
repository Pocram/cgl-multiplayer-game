﻿#ifndef COMMONS_NETWORKING_NETWORK_THREADS_TCP_SERVER_LISTEN_THREAD_H
#define COMMONS_NETWORKING_NETWORK_THREADS_TCP_SERVER_LISTEN_THREAD_H

#define WIN32_LEAN_AND_MEAN

#include <functional>

#include <Commons/Threading/ThreadContainer.hpp>
#include <Commons/Networking/SocketConnection.hpp>

struct TcpServerInfo;

////////////////////////////////////////////////////////////
/// \brief A class that handles listening actions for a TCP Server
///
/// Handles listening actions for a TCP Server. Used by TcpServer.
////////////////////////////////////////////////////////////
class TcpServerListenThread : public ThreadContainer
{
public:

	// CONSTRUCTORS
	//====================================================================================================

	////////////////////////////////////////////////////////////
	/// \brief Default constructor
	///
	/// This constructor does not initialize the thread properly.
	/// Use the other constructor or Create() instead.
	////////////////////////////////////////////////////////////
	TcpServerListenThread();

	////////////////////////////////////////////////////////////
	/// \brief Initializes the listen thread.
	///
	/// Initializes the thread and associates it to a TCP server.
	///
	/// \param associatedTcpServerInfo	A pointer to the information struct of the associated TCP Server.
	/// \param onClientConnectCallback	A callback function that is invoked whenever the thread detects a client connection.
	////////////////////////////////////////////////////////////
	TcpServerListenThread( TcpServerInfo* associatedTcpServerInfo, std::function<void( SOCKET clientSocket )> onClientConnectCallback );

	////////////////////////////////////////////////////////////
	/// \brief Default destructor
	////////////////////////////////////////////////////////////
	virtual ~TcpServerListenThread();


	// METHODS
	//====================================================================================================
	
	////////////////////////////////////////////////////////////
	/// \brief Initializes the listen thread.
	///
	/// Fulfills the same purpose as the 
	/// TcpServerListenThread( TcpServerInfo* associatedTcpServerInfo, std::function<void( SOCKET clientSocket )> onClientConnectCallback ) constructor.
	/// Initializes the thread and associates it to a TCP server.
	///
	/// \param associatedTcpServerInfo	A pointer to the information struct of the associated TCP Server.
	/// \param onClientConnectCallback	A callback function that is invoked whenever the thread detects a client connection.
	////////////////////////////////////////////////////////////
	void Create( TcpServerInfo* associatedTcpServerInfo, std::function<void( SOCKET clientSocket )> onClientConnectCallback );
	
	////////////////////////////////////////////////////////////
	/// \brief Starts listening for incoming client connection requests.
	////////////////////////////////////////////////////////////
	void StartListening();

	////////////////////////////////////////////////////////////
	/// \brief Stops listening to incoming client connection requests.
	////////////////////////////////////////////////////////////
	void StopListening();


protected:
	void ThreadMain() override;


private:
	TcpServerInfo* _associatedTcpServerInfo;								//!< Vital information about the associated TCP server.
	std::function<void( SOCKET clientSocket )> _onClientConnectCallback;	//!< A callback that is invoked whenever the thread detects a client.
	bool _isDoingListenLoop;												//!< Whether the thread shall listen for incoming clients or not. Used to control the loop.
};

#endif // COMMONS_NETWORKING_NETWORK_THREADS_TCP_SERVER_LISTEN_THREAD_H