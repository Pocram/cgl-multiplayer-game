﻿#ifndef COMMONS_NETWORKING_NETWORK_THREADS_TCP_CLIENT_CONNECT_THREAD_H
#define COMMONS_NETWORKING_NETWORK_THREADS_TCP_CLIENT_CONNECT_THREAD_H

#define WIN32_LEAN_AND_MEAN

#include <functional>

#include <Commons/Threading/ThreadContainer.hpp>
#include <Commons/Networking/SocketConnection.hpp>

struct TcpClientInfo;

class TcpClientConnectThread : public ThreadContainer
{
public:

	// CONSTRUCTORS
	//====================================================================================================

	////////////////////////////////////////////////////////////
	/// \brief Default constructor
	///
	/// This constructor does not initialize the thread properly.
	/// Use the other constructor or Create() instead.
	////////////////////////////////////////////////////////////
	TcpClientConnectThread();

	////////////////////////////////////////////////////////////
	/// \brief Initializes the connect thread.
	///
	/// Initializes the thread and associates it to a TCP client.
	///
	/// \param associatedTcpClientInfo	A pointer to the information struct of the associated TCP client.
	/// \param onClientConnectCallback	A callback function that is invoked whenever the thread detects a server connection.
	////////////////////////////////////////////////////////////
	TcpClientConnectThread( TcpClientInfo* associatedTcpClientInfo, std::function<void()> onServerConnectCallback );

	////////////////////////////////////////////////////////////
	/// \brief Default destructor
	////////////////////////////////////////////////////////////
	virtual ~TcpClientConnectThread();


	// METHODS
	//====================================================================================================

	////////////////////////////////////////////////////////////
	/// \brief Initializes the connect thread.
	///
	/// Fulfills the same purpose as the 
	/// TcpClientConnectThread( TcpClientInfo* associatedTcpClientInfo, std::function<void()> onServerConnectCallback ) constructor.
	/// Initializes the thread and associates it to a TCP client.
	///
	/// \param associatedTcpClientInfo	A pointer to the information struct of the associated TCP client.
	/// \param onClientConnectCallback	A callback function that is invoked whenever the thread detects a server connection.
	////////////////////////////////////////////////////////////
	void Create( TcpClientInfo* associatedTcpClientInfo, std::function<void()> onServerConnectCallback );

	////////////////////////////////////////////////////////////
	/// \brief Starts the thread that looks for a matching server.
	////////////////////////////////////////////////////////////
	void StartConnecting();

	////////////////////////////////////////////////////////////
	/// \brief Safely stops the thread that looks for a matching server.
	////////////////////////////////////////////////////////////
	void StopConnecting();


protected:
	void ThreadMain() override;


private:
	TcpClientInfo* _associatedTcpClientInfo;		//!< Vital information about the associated TCP server.
	std::function<void()> _onServerConnectCallback;	//!< A callback that is invoked whenever the thread detects a client.
	
	bool _isDoingConnectLoop;						//!< Whether the thread is actively trying to connect to a server or not.
	int _serverConnectRetryInterval;				//!< The duration in milliseconds between connect() calls.
};

#endif // COMMONS_NETWORKING_NETWORK_THREADS_TCP_CLIENT_CONNECT_THREAD_H