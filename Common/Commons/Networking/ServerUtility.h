#ifndef COMMONS_NETWORKING_SERVER_UTILITY_H
#define COMMONS_NETWORKING_SERVER_UTILITY_H

////////////////////////////////////////////////////////////
/// \brief Determines which clients a server will accept.
////////////////////////////////////////////////////////////
enum ServerAcceptanceMode
{
	Free,			//!< All clients will be accepted to the server, regardless of their address.
	Whitelisted,	//!< Only clients whose addresses are listed in the whitelist may connect to the server.
	Blacklisted		//!< Clients whose addresses are in the blacklist may not connect to the server.
};


#endif // COMMONS_NETWORKING_SERVER_UTILITY_H