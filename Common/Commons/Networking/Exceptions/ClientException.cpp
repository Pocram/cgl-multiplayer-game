﻿#include "ClientException.hpp"

ClientException::ClientException( std::string errorMessage )
{
	_errorMessage = errorMessage.c_str();
}

const char* ClientException::what() const
{
	return _errorMessage.c_str();
}