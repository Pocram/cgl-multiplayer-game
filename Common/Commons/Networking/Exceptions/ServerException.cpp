#include "ServerException.hpp"

ServerException::ServerException(std::string errorMessage)
{
	_errorMessage = errorMessage.c_str();
}

const char* ServerException::what() const
{
	return _errorMessage.c_str();
}