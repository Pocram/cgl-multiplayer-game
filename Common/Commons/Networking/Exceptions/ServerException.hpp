#ifndef COMMONS_NETWORKING_EXCEPTIONS_SERVER_EXCEPTION_H
#define COMMONS_NETWORKING_EXCEPTIONS_SERVER_EXCEPTION_H

#include <exception>
#include <string>

class ServerException : public std::exception
{
public:
	ServerException( std::string errorMessage );;
	
	const char* what() const throw( ) override;

private:
	std::string _errorMessage;
};

#endif // COMMONS_NETWORKING_EXCEPTIONS_SERVER_EXCEPTION_H