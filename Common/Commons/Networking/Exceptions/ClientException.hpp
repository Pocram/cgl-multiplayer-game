﻿#ifndef COMMONS_NETWORKING_EXCEPTIONS_CLIENT_EXCEPTION_H
#define COMMONS_NETWORKING_EXCEPTIONS_CLIENT_EXCEPTION_H

#include <exception>
#include <string>

class ClientException : public std::exception
{
public:
	ClientException( std::string errorMessage );;

	const char* what() const throw( ) override;

private:
	std::string _errorMessage;
};

#endif // COMMONS_NETWORKING_EXCEPTIONS_CLIENT_EXCEPTION_H