﻿#ifndef COMMONS_NETWORKING_TCP_SERVER_TO_CLIENT_CONNECTION_H
#define COMMONS_NETWORKING_TCP_SERVER_TO_CLIENT_CONNECTION_H

#define WIN32_LEAN_AND_MEAN

#include <winsock2.h>

#include <string>
#include <queue>
#include <functional>

#include <Commons/Threading/ThreadContainer.hpp>

////////////////////////////////////////////////////////////
/// \brief A class that handles messages received from a socket
///
/// This class actively receives messages from a given socket.
/// The messages are stored in a queue and can be retrieved with PopOldestReceivedMessage().
/// Every time a message is received the virtual method OnReceiveMessage( std::string message ) is called.
///
/// The receiving loop runs in a thread.
////////////////////////////////////////////////////////////
class SocketConnection : ThreadContainer
{
public:
	// CONSTRUCTORS
	//====================================================================================================
	////////////////////////////////////////////////////////////
	/// \brief The default constructor.
	///
	/// The default constructor. You should generally try to use
	/// SocketConnection( SOCKET sendingSocket ) instead.
	/// If for some reason you are unable to do that, use Create() after
	/// creating the object.
	////////////////////////////////////////////////////////////
	SocketConnection();

	////////////////////////////////////////////////////////////
	/// \brief Creates the connection and associates it with a socket.
	///
	/// Associates the sending socket with this object.
	/// If for some reason you are unable to use this constructor,
	/// call Create() after creating the object.
	///
	/// \param associatedSocket	The socket that is sending the messages.
	////////////////////////////////////////////////////////////
	SocketConnection( SOCKET associatedSocket );

	////////////////////////////////////////////////////////////
	/// \brief The default destructor.
	////////////////////////////////////////////////////////////
	virtual ~SocketConnection();

	// METHODS
	//====================================================================================================
	////////////////////////////////////////////////////////////
	/// \brief Initializes the connection.
	///
	/// Associates the sending socket with this object.
	/// call this method or use the SocketConnection( SOCKET clientSocket )
	/// constructor before calling StartConnection().
	///
	/// \param associatedSocket	The socket that is sending the messages.
	////////////////////////////////////////////////////////////
	void Create( SOCKET associatedSocket );

	////////////////////////////////////////////////////////////
	/// \brief Starts listening for messages.
	///
	/// Starts the thread that listens for the socket's messages.
	/// You should either call Create() or use the SocketConnection( SOCKET sendingSocket )
	/// constructor before calling this method.
	////////////////////////////////////////////////////////////
	void StartConnection();

	void EnableSending();

	////////////////////////////////////////////////////////////
	/// \brief Sets the buffer size for the socket's messages.
	///
	/// The `recv()` function can only accept messages up to a
	/// fixed amount of bytes. This method sets this size.
	///
	/// \param newBufferSize	The new buffer size in bytes.
	////////////////////////////////////////////////////////////
	void SetReceiverBufferSize( int newBufferSize );

	////////////////////////////////////////////////////////////
	/// \brief Sets the time between `recv()` calls.
	///
	/// The `recv()`-listening-thread runs in a continuous loop.
	/// Setting a delay between each iteration greatly increases performance.
	///
	/// \param millisecondsBetweenReceiveCalls	The new interval in milliseconds.
	////////////////////////////////////////////////////////////
	void SetReceiveInterval( int millisecondsBetweenReceiveCalls );

	////////////////////////////////////////////////////////////
	/// \brief Sets the time between `send()` calls.
	///
	/// The `send()`-listening-thread runs in a continuous loop.
	/// Setting a delay between each iteration greatly increases performance.
	///
	/// \param millisecondsBetweenSendCalls	The new interval in milliseconds.
	////////////////////////////////////////////////////////////
	void SetSendInterval( int millisecondsBetweenSendCalls );

	////////////////////////////////////////////////////////////
	/// \brief Gets the oldest unhandled message and removes it from the queue.
	///
	/// \returns Whether a message has been popped or not.
	////////////////////////////////////////////////////////////
	bool PopOldestReceivedMessage( std::string &outMessage );

	////////////////////////////////////////////////////////////
	/// \brief Returns a const pointer to the connection's socket.
	////////////////////////////////////////////////////////////
	const SOCKET* GetAssociatedSocket();

	////////////////////////////////////////////////////////////
	/// \brief Gets the socket's address.
	///
	/// Gets the address of "yourself". 
	/// To get the address of the other end of the connection, 
	/// use SocketConnection::GetPeerAddress() instead.
	///
	/// \returns The address or an empty string if the socket is invalid.
	////////////////////////////////////////////////////////////
	std::string GetSocketAddress();

	////////////////////////////////////////////////////////////
	/// \brief Gets the socket's peer's address.
	///
	/// Gets the address of "them". 
	/// To get the address of the closer end of the connection, 
	/// use SocketConnection::GetSocketAddress() instead.
	///
	/// \returns The address or an empty string if the socket is invalid.
	////////////////////////////////////////////////////////////
	std::string GetPeerAddress();


	// TODO: Document this. Do I even need this?
	bool SendMessageToServer( std::string message ) const;

	void SubscribeToMessageReceiveCallback( std::function<void( std::string message, SocketConnection& socketConnection )> callback );
	void UnsubscribeFromMessageReceiveCallback();


protected:
	// METHODS
	//====================================================================================================
	////////////////////////////////////////////////////////////
	/// \brief Listens for socket messages.
	///
	/// This method is run within its own thread.
	/// It continuously looks for any messages the associated
	/// socket sends to the server. Whenever a message has been
	/// received, it is pushed into a queue and OnReceiveMessage()
	/// is called for use with child classes.
	///
	/// In order to retrieve the oldest message, use PopOldestReceivedMessage().
	////////////////////////////////////////////////////////////
	void ThreadMain() override;

	////////////////////////////////////////////////////////////
	/// \brief Sends messages to the associated socket.
	///
	/// This method is run within its own thread.
	////////////////////////////////////////////////////////////
	//void ThreadMainB() override;

	////////////////////////////////////////////////////////////
	/// \brief This method is called whenever the socket sends a message.
	///
	/// \param message	The message sent by the socket.
	////////////////////////////////////////////////////////////
	virtual void OnReceiveMessage( std::string message );



private:
	// METHODS
	//====================================================================================================
	void MoveFinishedMessagesFromBufferToQueue();

	// MEMBERS
	//====================================================================================================
	SOCKET _assiociatedSocket;					//!< The associated socket. You can send messages to it and receive messages from it.

	bool _isDoingReceiveLoop;					//!< Whether the connection is actively receiving or not. Used to control the threaded loop.
	bool _isDoingSendLoop;						//!< Whether the connection is actively sending or not. Used to control the threaded loop.

	int _receiveBufferSize;						//!< The size of the receive buffer in bytes.

	std::string _mainReceiveBuffer;

	std::queue<std::string> _receivedMessages;	//!< A queue of messages sent by the client.
	std::queue<std::string> _sendMessageQueue;	//!< A queue of messages that will be sent to the client.

	int _receiveCallInterval;					//!< The interval at which the receive loop fires. Alternatively: The delay at the end of each receive loop iteration in ms.
	int _sendCallInterval;						//!< The interval at which the send loop fires. Alternatively: The delay at the end of each receive loop iteration in ms.

	std::function<void( std::string message, SocketConnection& socketConnection )> _onMessageReceiveCallback;

};

#endif // COMMONS_NETWORKING_TCP_SERVER_TO_CLIENT_CONNECTION_H