﻿#ifndef COMMONS_NETWORKING_TCP_SERVER_H
#define COMMONS_NETWORKING_TCP_SERVER_H

#define WIN32_LEAN_AND_MEAN

#include <WinSock2.h>
#include <WS2tcpip.h>

#include <stdio.h>
#include <sstream>
#include <vector>
#include <set>
#include <algorithm>

#include <Commons/Networking/NetworkThreads/TcpServerListenThread.hpp>
#include <Commons/Networking/SocketConnection.hpp>
#include <Commons/Networking/Exceptions/ServerException.hpp>
#include <Commons/Errors/ErrorMessageUtility.h>
#include <Commons/Networking/ServerUtility.h>

////////////////////////////////////////////////////////////
/// \brief Contains information about a TcpServer.
///
/// Contains the port, maximum amount of connections,
/// the addrinfo struct and the listening socket.
////////////////////////////////////////////////////////////
struct TcpServerInfo
{
	const char* PortNumber;				//!< The port's number or name.
	int MaxConnectionCount;				//!< The maximum amount of connections the server accepts.
	struct addrinfo* AddressInfoOutput;	//!< The addressInfo used for the server.
	SOCKET ListenSocket;				//!< The socket used for listening for clients.
};

template <class ClientType>
class TcpServer
{
	// Limit ClientType to subclasses of SocketConnection
	static_assert( std::is_base_of<SocketConnection, ClientType>::value, "ClientType must be a descendant of SocketConnection!" );

public:
	////////////////////////////////////////////////////////////
	/// \brief Default constructor.
	////////////////////////////////////////////////////////////
	TcpServer();

	////////////////////////////////////////////////////////////
	/// \brief Default destructor.
	////////////////////////////////////////////////////////////
	virtual ~TcpServer();


	// METHODS
	//====================================================================================================
	////////////////////////////////////////////////////////////
	/// \brief Sets up the server.
	///
	/// This method creates a socket and binds the server to it.
	/// It is required to call this method before using the server.
	///
	/// \exception ServerException Thrown when the socket could not be created or bound.
	///
	/// \param portNumber							The server's desired port number.
	/// \param clientConnectionMessageBufferSize	The size of the buffer used to receive the clients' messages.
	////////////////////////////////////////////////////////////
	void SetUp(const char* portNumber, int maxConnectionCount = SOMAXCONN);

	////////////////////////////////////////////////////////////
	/// \brief Starts the server.
	///
	/// Starts the server and enables listening for 
	/// incoming client connections.
	////////////////////////////////////////////////////////////
	void StartServer();

	////////////////////////////////////////////////////////////
	/// \brief Starts the server.
	///
	/// Starts the server and enables listening for 
	/// incoming client connections.
	////////////////////////////////////////////////////////////
	void StopServer(bool removeConnections = false);

	////////////////////////////////////////////////////////////
	/// \brief Removes every connection that uses the specified socket.
	///
	/// \param socketToRemove The socket that should be removed from the server.
	/// \returns Whether the socket has been removed or not.
	////////////////////////////////////////////////////////////
	bool RemoveConnectionBySocket( SOCKET socketToRemove );

	////////////////////////////////////////////////////////////
	/// \brief Sets the server's acceptance mode.
	///
	/// \see ServerAcceptanceMode
	////////////////////////////////////////////////////////////
	void SetServerAcceptanceMode( ServerAcceptanceMode newMode );

	////////////////////////////////////////////////////////////
	/// \brief Adds an address to the server's whitelist.
	///
	/// If the server's acceptance mode is set to whitelisted,
	/// connections from this address will be accepted.
	///
	/// \param newAddress The new address. Example: `127.0.0.1`
	///
	/// \see TcpServer::SetServerAcceptanceMode()
	////////////////////////////////////////////////////////////
	void AddAddressToWhitelist( std::string newAddress );

	////////////////////////////////////////////////////////////
	/// \brief Adds an address to the server's blacklist.
	///
	/// If the server's acceptance mode is set to blacklisted,
	/// connections from this address will *not* be accepted.
	///
	/// \param newAddress The new address. Example: `127.0.0.1`
	///
	/// \see TcpServer::SetServerAcceptanceMode()
	////////////////////////////////////////////////////////////
	void AddAddressToBlacklist( std::string newAddress );

	////////////////////////////////////////////////////////////
	/// \brief Adds a set of addresses to the server's whitelist.
	///
	/// If the server's acceptance mode is set to whitelisted,
	/// connections from these addresses will be accepted.
	///
	/// \param newAddress The new addresses. Example of a single one: `127.0.0.1`
	///
	/// \see TcpServer::SetServerAcceptanceMode()
	////////////////////////////////////////////////////////////
	void AddAddressesToWhitelist( std::set<std::string> newAddresses );

	////////////////////////////////////////////////////////////
	/// \brief Adds a set of addresses to the server's blacklist.
	///
	/// If the server's acceptance mode is set to blacklisted,
	/// connections from these addresses will *not* be accepted.
	///
	/// \param newAddress The new addresses. Example of a single one: `127.0.0.1`
	///
	/// \see TcpServer::SetServerAcceptanceMode()
	////////////////////////////////////////////////////////////
	void AddAddressesToBlacklist( std::set<std::string> newAddresses );

	////////////////////////////////////////////////////////////
	/// \brief Removes an address from the server's whitelist.
	///
	/// If the server's acceptance mode is set to whitelisted,
	/// connections from this address will no longer be accepted.
	///
	/// \param address The address to be removed. Example: `127.0.0.1`
	///
	/// \see TcpServer::SetServerAcceptanceMode()
	////////////////////////////////////////////////////////////
	void RemoveAddressFromWhitelist( std::string address );

	////////////////////////////////////////////////////////////
	/// \brief Removes an address from the server's blacklist.
	///
	/// If the server's acceptance mode is set to blacklisted,
	/// connections from this address will then be accepted.
	///
	/// \param address The address to be removed. Example: `127.0.0.1`
	///
	/// \see TcpServer::SetServerAcceptanceMode()
	////////////////////////////////////////////////////////////
	void RemoveAddressFromBlacklist( std::string address );

	////////////////////////////////////////////////////////////
	/// \brief Removes a set of addresses from the server's whitelist.
	///
	/// If the server's acceptance mode is set to whitelisted,
	/// connections from these addresses will no longer be accepted.
	///
	/// \param addresses The addresses to be removed. Example of a single one: `127.0.0.1`
	///
	/// \see TcpServer::SetServerAcceptanceMode()
	////////////////////////////////////////////////////////////
	void RemoveAddressesFromWhitelist( std::set<std::string> addresses );

	////////////////////////////////////////////////////////////
	/// \brief Removes a set of addresses from the server's blacklist.
	///
	/// If the server's acceptance mode is set to blacklisted,
	/// connections from these addresses will then be accepted.
	///
	/// \param addresses The addresses to be removed. Example of a single one: `127.0.0.1`
	///
	/// \see TcpServer::SetServerAcceptanceMode()
	////////////////////////////////////////////////////////////
	void RemoveAddressesFromBlacklist( std::set<std::string> addresses );

	////////////////////////////////////////////////////////////
	/// \brief Checks if the whitelist contains a specific address.
	///
	/// \param addresses The addresses to be found. Example: `127.0.0.1`
	///
	/// \returns Whether the list contains the address or not.
	///
	/// \see TcpServer::SetServerAcceptanceMode()
	////////////////////////////////////////////////////////////
	bool IsAddressWhitelisted( std::string addressToCheck );

	////////////////////////////////////////////////////////////
	/// \brief Checks if the blacklist contains a specific address.
	///
	/// \param addresses The addresses to be found. Example: `127.0.0.1`
	///
	/// \returns Whether the list contains the address or not.
	///
	/// \see TcpServer::SetServerAcceptanceMode()
	////////////////////////////////////////////////////////////
	bool IsAddressBlacklisted( std::string addressToCheck );

	void SendMessageToAllClients(std::string message);

	////////////////////////////////////////////////////////////
	/// \brief Returns the amount of clients that are currently connected to the server.
	////////////////////////////////////////////////////////////
	int GetCurrentConnectionCount();

	////////////////////////////////////////////////////////////
	/// \brief Lists all the connections to the console including their pointer addresses.
	////////////////////////////////////////////////////////////
	void ListAllConnectedSockets();


protected:
	// METHODS
	//====================================================================================================
	////////////////////////////////////////////////////////////
	/// \brief This method is called whenever a client connects to the server.
	///
	/// \param client	A pointer to the client that is connected to the server.
	////////////////////////////////////////////////////////////
	virtual void OnClientConnectEvent( ClientType* client );

	virtual void OnMessageReceive( std::string message, SocketConnection& socketConnection );

	////////////////////////////////////////////////////////////
	/// \brief Removes all clients from the server and clears the list of connected clients.
	////////////////////////////////////////////////////////////
	void RemoveAllClients();

	// MEMBERS
	//====================================================================================================
	std::vector<ClientType*> _connectedClients;				//!< A vector of all clients that are connected to the server.


private:
	// METHODS
	//====================================================================================================
	void OnClientConnect( SOCKET clientSocket );
	
	void SetUpAddressInfo();
	void CreateSocket();
	void BindToSocket();

	// MEMBERS
	//====================================================================================================
	bool _isSetUp;											//!< Whether the server has been SetUp() successfully.
	TcpServerInfo _serverInfo;								//!< Detailed information about the server.
	TcpServerListenThread _serverListenThread;				//!< The thread used to listen for new client connections.
	ServerAcceptanceMode _serverAcceptanceMode;				//!< The acceptance mode of the server.
	std::set<std::string> _blacklistedClientAddresses;		//!< A list of client addresses that are not allowed to connect to the server.
	std::set<std::string> _whitelistedClientAddresses;		//!< A list of the only client addresses that are allowed to connect to the server.
};

template <class ClientType>
TcpServer<ClientType>::TcpServer()
{
	_isSetUp = false;

	_serverAcceptanceMode = ServerAcceptanceMode::Free;
	_serverInfo.PortNumber = "0";
	_serverInfo.MaxConnectionCount = 0;
}

template <class ClientType>
TcpServer<ClientType>::~TcpServer()
{
	freeaddrinfo( _serverInfo.AddressInfoOutput );
	closesocket( _serverInfo.ListenSocket );

	// Delete client array
	RemoveAllClients();
}

template <class ClientType>
void TcpServer<ClientType>::SetUp(const char* portNumber, int maxConnectionCount)
{
	_serverInfo.PortNumber = portNumber;
	_serverInfo.MaxConnectionCount = maxConnectionCount;

	SetUpAddressInfo();

	try
	{
		CreateSocket();
	}
	catch ( ServerException e )
	{
		printf( "ERROR: %s.\n", e.what() );
		return;
	}

	try
	{
		BindToSocket();
	}
	catch ( ServerException e )
	{
		printf( "ERROR: %s.\n", e.what() );
		return;
	}

	_isSetUp = true;
}

template <class ClientType>
void TcpServer<ClientType>::StartServer()
{
	int listenResult = listen( _serverInfo.ListenSocket, _serverInfo.MaxConnectionCount );

	// Check for errors
	if ( listenResult == SOCKET_ERROR )
	{
		closesocket( _serverInfo.ListenSocket );
		freeaddrinfo( _serverInfo.AddressInfoOutput );

		std::stringstream errorStream;
		errorStream << "TcpServer could not start listening at line " << __LINE__ << " in " << __FILENAME__;
		throw ServerException( errorStream.str() );
	}

	// Associate the listen thread with this server and start it.
	_serverListenThread.Create( &_serverInfo, std::bind( &TcpServer::OnClientConnect, this, std::placeholders::_1 ) );
	_serverListenThread.StartListening();
}

template <class ClientType>
void TcpServer<ClientType>::StopServer(bool removeConnections)
{
	_serverListenThread.StopListening();

	if(removeConnections )
	{
		RemoveAllClients();
	}
}

template <class ClientType>
bool TcpServer<ClientType>::RemoveConnectionBySocket( SOCKET socketToRemove )
{
	bool socketHasBeenRemoved = false;

	for(std::vector<SocketConnection*>::iterator it = _connectedClients.begin(); it != _connectedClients.end(); )
	{
		SocketConnection* connection = *it;
		SOCKET socketOfConnection = *( connection->GetAssociatedSocket() );

		if( socketOfConnection == socketToRemove)
		{
			it = _connectedClients.erase( it );
			delete( connection );
			socketHasBeenRemoved = true;
		}
		else
		{
			++it;
		}
	}

	return socketHasBeenRemoved;
}

template <class ClientType>
void TcpServer<ClientType>::SetServerAcceptanceMode( ServerAcceptanceMode newMode )
{
	_serverAcceptanceMode = newMode;
}

template <class ClientType>
void TcpServer<ClientType>::AddAddressToWhitelist( std::string newAddress )
{
	_whitelistedClientAddresses.insert( newAddress );
}

template <class ClientType>
void TcpServer<ClientType>::AddAddressToBlacklist( std::string newAddress )
{
	_blacklistedClientAddresses.insert( newAddress );
}

template <class ClientType>
void TcpServer<ClientType>::AddAddressesToWhitelist( std::set<std::string> newAddresses )
{
	_whitelistedClientAddresses.insert( newAddresses.begin(), newAddresses.end() );
}

template <class ClientType>
void TcpServer<ClientType>::AddAddressesToBlacklist( std::set<std::string> newAddresses )
{
	_blacklistedClientAddresses.insert( newAddresses.begin(), newAddresses.end() );
}

template <class ClientType>
void TcpServer<ClientType>::RemoveAddressFromWhitelist( std::string address )
{
	_whitelistedClientAddresses.erase( std::remove( _whitelistedClientAddresses.begin(), _whitelistedClientAddresses.end(), address ), _whitelistedClientAddresses.end() );
}

template <class ClientType>
void TcpServer<ClientType>::RemoveAddressFromBlacklist( std::string address )
{
	_blacklistedClientAddresses.erase( std::remove( _blacklistedClientAddresses.begin(), _blacklistedClientAddresses.end(), address ), _blacklistedClientAddresses.end() );
}

template <class ClientType>
void TcpServer<ClientType>::RemoveAddressesFromWhitelist( std::set<std::string> addresses )
{
	for( int i = 0; i < addresses.size(); i++ )
	{
		RemoveAddressFromWhitelist( addresses[i] );
	}
}

template <class ClientType>
void TcpServer<ClientType>::RemoveAddressesFromBlacklist( std::set<std::string> addresses )
{
	for ( int i = 0; i < addresses.size(); i++ )
	{
		RemoveAddressFromBlacklist( addresses[i] );
	}
}

template <class ClientType>
bool TcpServer<ClientType>::IsAddressWhitelisted( std::string addressToCheck )
{
	return _whitelistedClientAddresses.find( addressToCheck ) != _whitelistedClientAddresses.end();
}

template <class ClientType>
bool TcpServer<ClientType>::IsAddressBlacklisted( std::string addressToCheck )
{
	return _blacklistedClientAddresses.find( addressToCheck ) != _blacklistedClientAddresses.end();
}

template <class ClientType>
void TcpServer<ClientType>::SendMessageToAllClients( std::string message )
{
	for( int i = 0; i < _connectedClients.size(); i++ )
	{
		if ( _connectedClients[i] == nullptr )
		{
			continue;
		}

		_connectedClients[i]->EnableSending();
		_connectedClients[i]->SendMessageToServer( message );
	}
}

template <class ClientType>
int TcpServer<ClientType>::GetCurrentConnectionCount()
{
	return _connectedClients.size();
}

template <class ClientType>
void TcpServer<ClientType>::ListAllConnectedSockets()
{
	printf( "# All currently connected sockets:\n" );
	for ( int i = 0; i < _connectedClients.size(); i++ )
	{
		printf( "- %d\t\t%p\t\t%s\n", _connectedClients[i]->GetAssociatedSocket(), _connectedClients[i], _connectedClients[i]->GetPeerAddress() );
	}
	printf( "###############\n" );
}

template <class ClientType>
void TcpServer<ClientType>::OnClientConnectEvent( ClientType* client )
{
}

template <class ClientType>
void TcpServer<ClientType>::OnMessageReceive( std::string message, SocketConnection& socketConnection )
{
}

template <class ClientType>
void TcpServer<ClientType>::OnClientConnect( SOCKET clientSocket )
{
	printf( "# A new client (Socket: %d) connected to the server\n", clientSocket );

	// Create new connection.
	// Why use a pointer? The vector of connected clients reallocates itself every time a new connection is established.
	// This causes the objects to lose their values and get confused. A pointer to a fixed place in memory circumvents this.
	SocketConnection* newConnection = new SocketConnection( clientSocket );
	std::string socketAddress = newConnection->GetPeerAddress();

	printf( "# IP: %s\n", socketAddress.c_str() );

	// Check if the addrress is valid.
	if(_serverAcceptanceMode == ServerAcceptanceMode::Whitelisted )
	{
		if( IsAddressWhitelisted( socketAddress ) == false )
		{
			printf( "# The socket's address is not whitelisted and will be ignored!\n" );
			delete( newConnection );
			return;
		}
	} 
	else if ( _serverAcceptanceMode == ServerAcceptanceMode::Blacklisted )
	{
		if ( IsAddressBlacklisted( socketAddress ) )
		{
			printf( "# The socket's address is blacklisted and will be ignored!\n" );
			delete( newConnection );
			return;
		}
	}

	// Add the new client to the list of clients.
	_connectedClients.push_back( newConnection );
	newConnection->SubscribeToMessageReceiveCallback( std::bind( &TcpServer::OnMessageReceive, this, std::placeholders::_1, std::placeholders::_2 ) );

	// List all currently connected sockets for debug purposes.
	// Somehow sockets lose their value whenever a new client connects to the server.
	//ListAllConnectedSockets();
	
	// Inform child classes.
	// Use the pointer to the last index (the one that was just added) for the callback.
	// Just using the parameter works as well, but this feels safer and the overhead is negligible.
	int lastClientIndex = _connectedClients.size() - 1;
	SocketConnection* clientInList = _connectedClients[lastClientIndex];
	OnClientConnectEvent( clientInList );

	printf( "# Adding new client connection at %p to list.\n", clientInList );

	// Start the client connection thread!
	clientInList->StartConnection();
}

template <class ClientType>
void TcpServer<ClientType>::SetUpAddressInfo()
{
	struct addrinfo addressInfoInput;

	ZeroMemory( &addressInfoInput, sizeof( addressInfoInput ) );
	ZeroMemory( &_serverInfo.AddressInfoOutput, sizeof( _serverInfo.AddressInfoOutput ) );
	addressInfoInput.ai_family = AF_INET;
	addressInfoInput.ai_socktype = SOCK_STREAM;	// TCP/IP
	addressInfoInput.ai_protocol = IPPROTO_TCP;
	addressInfoInput.ai_flags = AI_PASSIVE;		// Server

	getaddrinfo( nullptr, _serverInfo.PortNumber, &addressInfoInput, &_serverInfo.AddressInfoOutput );
}

template <class ClientType>
void TcpServer<ClientType>::CreateSocket()
{
	_serverInfo.ListenSocket = socket( _serverInfo.AddressInfoOutput->ai_family, _serverInfo.AddressInfoOutput->ai_socktype, _serverInfo.AddressInfoOutput->ai_protocol );

	// Check for errors
	if ( _serverInfo.ListenSocket == INVALID_SOCKET )
	{
		freeaddrinfo( _serverInfo.AddressInfoOutput );

		std::stringstream errorStream;
		errorStream << "Could not create socket for TcpServer at line " << __LINE__ << " in " << __FILENAME__;
		throw ServerException( errorStream.str() );
	}
}

template <class ClientType>
void TcpServer<ClientType>::BindToSocket()
{
	int bindResult = ::bind( _serverInfo.ListenSocket, _serverInfo.AddressInfoOutput->ai_addr, static_cast<int>( _serverInfo.AddressInfoOutput->ai_addrlen ) );

	// Check for errors
	if ( bindResult == SOCKET_ERROR )
	{
		closesocket( _serverInfo.ListenSocket );
		freeaddrinfo( _serverInfo.AddressInfoOutput );

		std::stringstream errorStream;
		errorStream << "Could not bind to socket in TcpServer at line " << __LINE__ << " in " << __FILENAME__;
		throw ServerException( errorStream.str() );
	}
}

template <class ClientType>
void TcpServer<ClientType>::RemoveAllClients()
{
	while ( !_connectedClients.empty() )
	{
		delete _connectedClients.back();
		_connectedClients.pop_back();
	}
}
#endif // COMMONS_NETWORKING_TCP_SERVER_H
