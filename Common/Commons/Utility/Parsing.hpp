﻿#ifndef COMMONS_UTILITY_PARSING_H
#define COMMONS_UTILITY_PARSING_H

#include <vector>
#include <string>

namespace Parsing
{
	std::vector<std::string> FromStringToVector( const std::string & stringToParse, const std::string & prefix );
}

#endif // COMMONS_UTILITY_PARSING_H