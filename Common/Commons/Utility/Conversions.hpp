﻿#ifndef COMMONS_UTILIRY_CONVERSIONS_H
#define COMMONS_UTILIRY_CONVERSIONS_H

#include <string>
#include <sstream>

namespace Conversions
{
	template <typename T>
	T StringToNumber( const std::string& string )
	{
		std::stringstream ss( string );
		T result;
		return ss >> result ? result : 0;
	}
}

#endif // COMMONS_UTILIRY_CONVERSIONS_H