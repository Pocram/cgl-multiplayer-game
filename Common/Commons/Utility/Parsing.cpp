﻿#include "Parsing.hpp"

#include <iostream>
#include <sstream>

std::vector<std::string> Parsing::FromStringToVector( const std::string & stringToParse, const std::string & prefix )
{
	std::vector<std::string> output;

	// If prefix is not present, the string is invalid.
	if ( stringToParse.find( prefix + "{" ) != 0 )
	{
		return output;
	}

	std::string stringAfterPrefix = stringToParse.substr( prefix.length() + 1 );
	int positionOfClosingBracket = stringAfterPrefix.find( '}' );
	std::string content = stringAfterPrefix.substr( 0, positionOfClosingBracket );

	std::istringstream ss( content );
	
	std::string token;
	while ( std::getline( ss, token, ';' ) )
	{
		output.push_back( token );
	}

	return output;
}
