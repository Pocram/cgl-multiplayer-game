﻿#ifndef COMMONS_SOUND_RANDOM_SOUND_CONTAINER_H
#define COMMONS_SOUND_RANDOM_SOUND_CONTAINER_H

#include <SFML/Audio.hpp>

#include <string>
#include <vector>

class RandomSoundContainer
{
public:
	RandomSoundContainer();
	RandomSoundContainer( std::vector<std::string> soundNames );

	void AddSounds( std::vector<std::string> soundNames );

	void PlayRandomSound();


private:
	std::vector<sf::Sound> _sounds;
};

#endif // COMMONS_SOUND_RANDOM_SOUND_CONTAINER_H