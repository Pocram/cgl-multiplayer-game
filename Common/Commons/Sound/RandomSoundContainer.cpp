﻿#include "RandomSoundContainer.hpp"
#include <Commons/FileManagers/SoundManager.hpp>

RandomSoundContainer::RandomSoundContainer()
{
}

RandomSoundContainer::RandomSoundContainer( std::vector<std::string> soundNames )
{
	AddSounds( soundNames );
}

void RandomSoundContainer::AddSounds( std::vector<std::string> soundNames )
{
	for ( int i = 0; i < soundNames.size(); i++ )
	{
		sf::SoundBuffer* buffer = SoundManager::GetSound( soundNames[i] );
		if ( buffer != nullptr )
		{
			sf::Sound newSound( *buffer );
			_sounds.push_back( newSound );
		}
	}
}

void RandomSoundContainer::PlayRandomSound()
{
	if ( _sounds.size() == 0 )
	{
		return;
	}

	int randomSoundId = rand() % _sounds.size();
	_sounds[randomSoundId].play();
}
