﻿#include "ServerFootballScene.hpp"

#include <Commons/Console/Console.hpp>
#include <sstream>

#include <Commons/FileManagers/TextureManager.hpp>
#include <Commons/FileManagers/FontManager.hpp>

#include <Commons/Game/GameObjects/SimpleSpriteObject.hpp>
#include <Commons/Game/GameObjects/SimpleTextObject.hpp>
#include <Commons/Game/GameObjects/SimpleBoxColliderObject.hpp>
#include <Server/Game/GameObjects/ControllableActor.hpp>
#include <Server/Game/GameObjects/Football.hpp>
#include <Server/Game/GameObjects/GoalArea.hpp>

ServerFootballScene::ServerFootballScene()
{
	_leftTeamScore = 0;
	_rightTeamScore = 0;

	_kickOffTime = 3.0f;
	_remainingTimeUntilKickoff = _kickOffTime;

	// BG
	// -------------------------------------------------------------------------
	_fieldBackground = new SimpleSpriteObject( TextureManager::GetTexture( "Sprites/field_bg.png" ), sf::Vector2f( 0.0f, 0.0f ) );
	_fieldBackground->SetName( "Field Background" );
	_fieldBackground->SetDrawOrderIndex( 0 );
	InstantiateGameObject( _fieldBackground );


	// PLAYERS
	// -------------------------------------------------------------------------
	_playerOneA = new ControllableActor();
	_playerOneA->SetName( "Player 1 A" );
	_playerOneA->SetDrawOrderIndex( 100 );
	InstantiateGameObject( _playerOneA );

	_playerOneB = new ControllableActor();
	_playerOneB->SetName( "Player 1 B" );
	_playerOneB->SetDrawOrderIndex( 150 );
	InstantiateGameObject( _playerOneB );

	//_playerControllerA = new PlayerController( _playerOneA, _playerOneB );
	//_playerControllerA->SetName( "Player Controller A" );
	//InstantiateGameObject( _playerControllerA );


	_playerTwoA = new ControllableActor();
	_playerTwoA->SetName( "Player 2 A" );
	_playerOneA->SetDrawOrderIndex( 100 );
	InstantiateGameObject( _playerTwoA );

	_playerTwoB = new ControllableActor();
	_playerTwoB->SetName( "Player 2 B" );
	_playerOneB->SetDrawOrderIndex( 150 );
	InstantiateGameObject( _playerTwoB );

	//_playerControllerB = new PlayerController( _playerTwoA, _playerTwoB );
	//_playerControllerB->SetName( "Player Controller B" );
	//InstantiateGameObject( _playerControllerB );

	
	// FOOTBALL STUFF
	// -------------------------------------------------------------------------
	_leftGoal = new GoalArea();
	_leftGoal->GetTransform().SetPosition( sf::Vector2f( 0.0f, 421.875f ) );
	_leftGoal->SetName( "Left Goal" );
	_leftGoal->SetDrawOrderIndex( 50 );
	InstantiateGameObject( _leftGoal );
	_leftGoal->SubscribeToOnBallEnter( std::bind( &ServerFootballScene::OnLeftGoalScore, this, std::placeholders::_1 ) );

	_leftGoalTopCollider = new SimpleBoxColliderObject( sf::Vector2f( 50.0f, 421.875f - 150.0f - 2.5f ), 100.0f, 5.0f );
	InstantiateGameObject( _leftGoalTopCollider );

	_leftGoalBottomCollider = new SimpleBoxColliderObject( sf::Vector2f( 50.0f, 421.875f + 150.0f + 2.5f ), 100.0f, 5.0f );
	InstantiateGameObject( _leftGoalBottomCollider );

	_rightGoal = new GoalArea();
	_rightGoal->GetTransform().SetPosition( sf::Vector2f( 1500.0f, 421.875f ) );
	_rightGoal->SetName( "Right Goal" );
	_rightGoal->SetDrawOrderIndex( 51 );
	InstantiateGameObject( _rightGoal );
	_rightGoal->SubscribeToOnBallEnter( std::bind( &ServerFootballScene::OnRightGoalScore, this, std::placeholders::_1 ) );

	_rightGoalTopCollider = new SimpleBoxColliderObject( sf::Vector2f( 1450.0f, 421.875f - 150.0f - 2.5f ), 100.0f, 5.0f );
	InstantiateGameObject( _rightGoalTopCollider );

	_rightGoalBottomCollider = new SimpleBoxColliderObject( sf::Vector2f( 1450.0f, 421.875f + 150.0f + 2.5f ), 100.0f, 5.0f );
	InstantiateGameObject( _rightGoalBottomCollider );

	_football = new Football();
	_football->SetName( "Football" );
	_football->SetDrawOrderIndex( 500 );
	InstantiateGameObject( _football );


	_leftScoreText = new SimpleTextObject( FontManager::GetFont( "Fonts/DroidSans-Bold.ttf" ), "0", 72, sf::Color( 255, 255, 0, 63 ) );
	_leftScoreText->GetTransform().SetPosition( sf::Vector2f( 471.0f, 418.875f ) );
	InstantiateGameObject( _leftScoreText );

	_rightScoreText = new SimpleTextObject( FontManager::GetFont( "Fonts/DroidSans-Bold.ttf" ), "0", 72, sf::Color( 0, 0, 255, 63 ) );
	_rightScoreText->GetTransform().SetPosition( sf::Vector2f( 1500.0f - 471.0f, 418.875f ) );
	InstantiateGameObject( _rightScoreText );


	// COLLIDERS
	// -------------------------------------------------------------------------
	_topCollider = new SimpleBoxColliderObject( sf::Vector2f( 750.0f, -50.0f ), 1500.0f, 100.0f );
	InstantiateGameObject( _topCollider );

	_bottomCollider = new SimpleBoxColliderObject( sf::Vector2f( 750.0f, 843.75 + 50.0f ), 1500.0f, 100.0f );
	InstantiateGameObject( _bottomCollider );

	_leftCollider = new SimpleBoxColliderObject( sf::Vector2f( -50.0f, 418.875f ), 100.0f, 843.75 );
	InstantiateGameObject( _leftCollider );

	_rightCollider = new SimpleBoxColliderObject( sf::Vector2f( 1550.0f, 418.875f ), 100.0f, 843.75 );
	InstantiateGameObject( _rightCollider );


	SimpleTextObject* clientDebugText = new SimpleTextObject( FontManager::GetFont( "Fonts/DroidSans-Bold.ttf" ), "SERVER", 400, sf::Color( 255, 255, 255, 20 ) );
	clientDebugText->GetTransform().SetPosition( sf::Vector2f( 750.0f, 310.0f ) );
	clientDebugText->SetDrawOrderIndex( 1 );
	InstantiateGameObject( clientDebugText );


	// DEBUG
	// -------------------------------------------------------------------------
	_playerOneNameText = new SimpleTextObject( FontManager::GetFont( "Fonts/DroidSans-Bold.ttf" ), "Unnamed", 26, sf::Color( 255, 255, 255, 255 ) );
	_playerOneNameText->GetTransform().SetPosition( sf::Vector2f( 375.0f, 25.0f ) );
	_playerOneNameText->SetDrawOrderIndex( 9000 );
	InstantiateGameObject( _playerOneNameText );

	_playerOneNameTextShadow = new SimpleTextObject( FontManager::GetFont( "Fonts/DroidSans-Bold.ttf" ), "Unnamed", 26, sf::Color( 0, 0, 0, 255 ) );
	_playerOneNameTextShadow->GetTransform().SetPosition( sf::Vector2f( 375.0f, 25.0f ) + sf::Vector2f( 2.0f, 2.0f ) );
	_playerOneNameTextShadow->SetDrawOrderIndex( 8999 );
	InstantiateGameObject( _playerOneNameTextShadow );

	_playerTwoNameText = new SimpleTextObject( FontManager::GetFont( "Fonts/DroidSans-Bold.ttf" ), "Unnamed", 26, sf::Color( 255, 255, 255, 255 ) );
	_playerTwoNameText->GetTransform().SetPosition( sf::Vector2f( 750.0f + 375.0f, 25.0f ) );
	_playerTwoNameText->SetDrawOrderIndex( 9000 );
	InstantiateGameObject( _playerTwoNameText );

	_playerTwoNameTextShadow = new SimpleTextObject( FontManager::GetFont( "Fonts/DroidSans-Bold.ttf" ), "Unnamed", 26, sf::Color( 0, 0, 0, 255 ) );
	_playerTwoNameTextShadow->GetTransform().SetPosition( sf::Vector2f( 750.0f + 375.0f, 25.0f ) + sf::Vector2f( 2.0f, 2.0f ) );
	_playerTwoNameTextShadow->SetDrawOrderIndex( 8999 );
	InstantiateGameObject( _playerTwoNameTextShadow );

	_server = new FootballGameTcpServer( _playerOneA, _playerOneB, _playerTwoA, _playerTwoB, _football, this );
	_server->SetUp( "20000" );
	_server->StartServer();

	_server->SubscribeToOnTeamsRearrangeEvent( std::bind( &ServerFootballScene::OnTeamsRearrange, this ) );
	_football->SubscribeToOnKickEvent( std::bind( &ServerFootballScene::OnPlayerKick, this ) );
	
	ResetPositions();
}

ServerFootballScene::~ServerFootballScene()
{
	delete( _leftGoal );
	delete( _rightGoal );
	delete( _football );
	
	delete( _playerOneA );
	delete( _playerOneB );
	delete( _playerTwoA );
	delete( _playerTwoB );
}

void ServerFootballScene::Update( sf::Time elapsedTime )
{
	_server->Update( elapsedTime );

	const NetworkPlayerInfo* playerOne = _server->GetPlayerOne();
	const NetworkPlayerInfo* playerTwo = _server->GetPlayerTwo();

	if( playerOne != nullptr )
	{
		_playerOneNameText->SetText( playerOne->GetPlayerName() );
		_playerOneNameTextShadow->SetText( playerOne->GetPlayerName() );
	}
	else
	{
		_playerOneNameText->SetText( "No Player" );
		_playerOneNameTextShadow->SetText( "No Player" );
	}

	if ( playerTwo != nullptr )
	{
		_playerTwoNameText->SetText( playerTwo->GetPlayerName() );
		_playerTwoNameTextShadow->SetText( playerTwo->GetPlayerName() );
	}
	else
	{
		_playerTwoNameText->SetText( "No Player" );
		_playerTwoNameTextShadow->SetText( "No Player" );
	}

	if( _remainingTimeUntilKickoff > 0.0f )
	{
		_remainingTimeUntilKickoff -= elapsedTime.asSeconds();
		SendTimeUntilKickoffToClients();
	}
}

const int& ServerFootballScene::GetRemainingTimeUntilKickoff() const
{
	return _remainingTimeUntilKickoff;
}

void ServerFootballScene::OnLeftGoalScore( Football* ball )
{
	ResetPositions();	
	_rightTeamScore++;
	
	OnGoalScore();
}

void ServerFootballScene::OnRightGoalScore( Football* ball )
{
	ResetPositions();
	_leftTeamScore++;

	OnGoalScore();
}

void ServerFootballScene::ResetPositions()
{
	_playerOneA->GetTransform().SetPosition( sf::Vector2f( 374.0f, 211.0f ) );
	_playerOneA->MoveIntoDirection( sf::Vector2f( 0.0f, 0.0f ) );
	_playerOneA->ResetSprintingTimers();
	_playerOneB->GetTransform().SetPosition( sf::Vector2f( 374.0f, 633.0f ) );
	_playerOneB->MoveIntoDirection( sf::Vector2f( 0.0f, 0.0f ) );
	_playerOneB->ResetSprintingTimers();
	_playerTwoA->GetTransform().SetPosition( sf::Vector2f( 1126.0f, 211.0f ) );
	_playerTwoA->MoveIntoDirection( sf::Vector2f( 0.0f, 0.0f ) );
	_playerTwoA->ResetSprintingTimers();
	_playerTwoB->GetTransform().SetPosition( sf::Vector2f( 1126.0f, 633.0f ) );
	_playerTwoB->MoveIntoDirection( sf::Vector2f( 0.0f, 0.0f ) );
	_playerTwoB->ResetSprintingTimers();
	
	_football->Stop();
	_football->GetTransform().SetPosition( sf::Vector2f( 750.0f, 421.875f ) );
}

void ServerFootballScene::OnGoalScore()
{
	std::stringstream ss;
	ss << "Left: " << _leftTeamScore << "\t\tRight: " << _rightTeamScore;
	WriteWarningWithCaller( ss.str() );

	std::stringstream leftText;
	leftText << _leftTeamScore;

	std::stringstream rightText;
	rightText << _rightTeamScore;

	_leftScoreText->SetText( leftText.str() );
	_rightScoreText->SetText( rightText.str() );

	// Let clients know.
	std::stringstream scoreMessage;
	scoreMessage << "score{" << _leftTeamScore << ";" << _rightTeamScore << "}";
	_server->SendMessageToAllClients( scoreMessage.str() );
	_server->SendMessageToAllClients( "goal{}" );

	_remainingTimeUntilKickoff = _kickOffTime;
	SendTimeUntilKickoffToClients();

	ResetPositions();
}

void ServerFootballScene::OnTeamsRearrange()
{
	_remainingTimeUntilKickoff = _kickOffTime;
	SendTimeUntilKickoffToClients();

	ResetPositions();
}

void ServerFootballScene::SendTimeUntilKickoffToClients() const
{
	if( _server != nullptr )
	{
		std::stringstream timeUntilKickoffMessage;
		timeUntilKickoffMessage << "timeUntilKickoff{" << _remainingTimeUntilKickoff << "}";
		_server->SendMessageToAllClients( timeUntilKickoffMessage.str() );
	}
}

void ServerFootballScene::OnPlayerKick() const
{
	_server->SendMessageToAllClients( "footballKick{}" );
}
