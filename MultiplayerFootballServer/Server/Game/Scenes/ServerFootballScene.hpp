﻿#pragma once

#include <Commons/Game/GameScene.hpp>

#include <Server/FootballGameTcpServer.hpp>

class SimpleSpriteObject;
class SimpleTextObject;
class GoalArea;
class ControllableActor;
class PlayerController;
class Football;
class SimpleBoxColliderObject;

class ServerFootballScene : public GameScene
{
public:
	ServerFootballScene();
	~ServerFootballScene() override;

	virtual void Update( sf::Time elapsedTime ) override;

	const int& GetRemainingTimeUntilKickoff() const;

private:
	void OnLeftGoalScore( Football* ball );
	void OnRightGoalScore( Football* ball );

	void ResetPositions();
	void OnGoalScore();

	void OnTeamsRearrange();

	void SendTimeUntilKickoffToClients() const;

	void OnPlayerKick() const;

	FootballGameTcpServer* _server;

	int _leftTeamScore;
	int _rightTeamScore;

	SimpleTextObject* _leftScoreText;
	SimpleTextObject* _rightScoreText;

	SimpleSpriteObject* _fieldBackground;

	GoalArea* _leftGoal;
	SimpleBoxColliderObject* _leftGoalTopCollider;
	SimpleBoxColliderObject* _leftGoalBottomCollider;

	GoalArea* _rightGoal;
	SimpleBoxColliderObject* _rightGoalTopCollider;
	SimpleBoxColliderObject* _rightGoalBottomCollider;
	
	ControllableActor* _playerOneA;
	ControllableActor* _playerOneB;
	ControllableActor* _playerTwoA;
	ControllableActor* _playerTwoB;

	SimpleBoxColliderObject* _topCollider;
	SimpleBoxColliderObject* _bottomCollider;
	SimpleBoxColliderObject* _leftCollider;
	SimpleBoxColliderObject* _rightCollider;

	Football* _football;

	float _remainingTimeUntilKickoff;
	float _kickOffTime;
	
	// Debug thingies
	SimpleTextObject* _playerOneNameText;
	SimpleTextObject* _playerOneNameTextShadow;
	SimpleTextObject* _playerTwoNameText;
	SimpleTextObject* _playerTwoNameTextShadow;
};
