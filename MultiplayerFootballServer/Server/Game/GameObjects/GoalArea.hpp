﻿#ifndef MULTIPLAYER_FOOTBALL_SERVER_GAME_GAME_OBJECTS_GOAL_AREA_H
#define MULTIPLAYER_FOOTBALL_SERVER_GAME_GAME_OBJECTS_GOAL_AREA_H

#include <Commons/Game/GameObject.hpp>
#include <functional>

class Football;

class GoalArea : public GameObject
{
public:
	GoalArea();
	~GoalArea();

	void DrawToWindow( sf::RenderWindow& targetWindow, Camera* camera ) override;
	void CalculateBoundingBox() override;

	void SubscribeToOnBallEnter( std::function<void( Football* )> callback );
	void UnsubscribeFromOnBallEnter();

	
protected:
	void OnCollisionEnter( Collision collision ) override;
	void OnCollisionStay( Collision collision ) override;
	void OnCollisionExit( Collision collision ) override;

private:
	sf::Sprite _sprite;
	
	std::function<void( Football* )> _onBallEnter;
};

#endif // MULTIPLAYER_FOOTBALL_SERVER_GAME_GAME_OBJECTS_GOAL_AREA_H