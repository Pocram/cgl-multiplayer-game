﻿#include "Football.hpp"
#include <Commons/Math/SfmlVectorMath.hpp>
#include <Commons/FileManagers/TextureManager.hpp>
#include <Commons/Game/Physics/CircleCollider.hpp>
#include <Commons/Game/Camera.hpp>

Football::Football()
{
	sf::Texture* texture = TextureManager::GetTexture( "Sprites/ball.png" );
	if(texture != nullptr )
	{
		_sprite.setTexture( *texture );
	}

	GetTransform().SetOrigin( sf::Vector2f( texture->getSize() ) / 2.0f );
	
	SetCollider( new CircleCollider( 30.0f, this ) );

	_brakeSpeedPerSecond = 1.0f;
	_onKickEvent = nullptr;

	AddTag( "Football" );
}

Football::~Football()
{
}

void Football::Update( sf::Time elapsedTime )
{
	if ( sf::VectorMath::Magnitude2f( _velocity ) > 0.0f )
	{
		_velocity -= _velocity * _brakeSpeedPerSecond * elapsedTime.asSeconds();
	}

	GetTransform().Translate( _velocity * elapsedTime.asSeconds() );
}

void Football::DrawToWindow( sf::RenderWindow& targetWindow, Camera* camera )
{
	Transform drawTransform = camera->LocalTransformToCameraTransform( GetTransform() );
	targetWindow.draw( _sprite, drawTransform.GetSfmlTransform() );
}

void Football::CalculateBoundingBox()
{
	CalculateBoundingBoxFromSprite( _sprite );
}

void Football::KickIntoDirection( const sf::Vector2f& direction, float kickVelocity )
{
	_velocity = sf::VectorMath::GetNormalized( direction ) * kickVelocity;

	if ( _onKickEvent != nullptr )
	{
		_onKickEvent();
	}
}

void Football::Stop()
{
	_velocity = sf::Vector2f( 0.0f, 0.0f );
}

sf::Vector2f Football::GetEffectiveVelocity() const
{
	return _velocity;
}

void Football::SubscribeToOnKickEvent( std::function<void()> callback )
{
	_onKickEvent = callback;
}

void Football::UnsubscribeFromOnKickEvent()
{
	_onKickEvent = nullptr;
}

void Football::OnCollisionEnter( Collision collision )
{
	if ( collision.OtherCollider->GetParentGameObject()->HasTag( "Goal" ) )
	{
		return;
	}

	_velocity = sf::VectorMath::ReflectAlongSurface( _velocity, collision.Normal );
}
