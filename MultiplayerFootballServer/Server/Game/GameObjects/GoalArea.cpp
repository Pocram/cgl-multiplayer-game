﻿#include "GoalArea.hpp"
#include <Commons/FileManagers/TextureManager.hpp>

#include <Commons/Game/Camera.hpp>
#include <Commons/Game/Physics/BoxCollider.hpp>
#include <Commons/Console/Console.hpp>

GoalArea::GoalArea()
{
	SetName( "Goal" );
	AddTag( "Goal" );

	sf::Texture* texture = TextureManager::GetTexture( "Oink/debug.png" );
	if( texture != nullptr )
	{
		GetTransform().SetOrigin( sf::Vector2f( texture->getSize() ) / 2.0f );
		_sprite.setTexture( *texture );
	}

	SetCollider( new BoxCollider( 100.0f, 300.0f, this ) );
	GetCollider()->SetTrigger( true );
}

GoalArea::~GoalArea()
{
}

void GoalArea::DrawToWindow( sf::RenderWindow& targetWindow, Camera* camera )
{
	Transform drawTransform = camera->LocalTransformToCameraTransform( GetTransform() );
	targetWindow.draw( _sprite, drawTransform.GetSfmlTransform() );
}

void GoalArea::CalculateBoundingBox()
{
	CalculateBoundingBoxFromSprite( _sprite );
}

void GoalArea::SubscribeToOnBallEnter( std::function<void( Football*)> callback )
{
	_onBallEnter = callback;
}

void GoalArea::UnsubscribeFromOnBallEnter()
{
	_onBallEnter = nullptr;
}

void GoalArea::OnCollisionEnter( Collision collision )
{
	if ( collision.OtherCollider == nullptr ||
		 collision.OtherCollider->GetParentGameObject() == nullptr ||
		 collision.OtherCollider->GetParentGameObject()->HasTag( "Football" ) == false )
	{
		return;
	}

	Football* ball = reinterpret_cast<Football*>( collision.OtherCollider->GetParentGameObject() );

	if ( _onBallEnter != nullptr )
	{
		_onBallEnter( ball );
	}
}

void GoalArea::OnCollisionStay( Collision collision )
{
}

void GoalArea::OnCollisionExit( Collision collision )
{
}
