﻿#ifndef MULTIPLAYER_FOOTBALL_SERVER_GAME_GAME_OBJECTS_PLAYER_CONTROLLER_H
#define MULTIPLAYER_FOOTBALL_SERVER_GAME_GAME_OBJECTS_PLAYER_CONTROLLER_H

#include <Commons/Game/GameObject.hpp>

class ControllableActor;

class PlayerController : public GameObject
{
public:
	PlayerController();
	PlayerController( ControllableActor* firstActor, ControllableActor* secondActor );
	~PlayerController() override;

	void Update( sf::Time elapsedTime ) override;


private:
	ControllableActor* _firstActor;
	ControllableActor* _secondActor;
};

#endif // MULTIPLAYER_FOOTBALL_SERVER_GAME_GAME_OBJECTS_PLAYER_CONTROLLER_H