﻿#ifndef MULTIPLAYER_FOOTBALL_SERVER_GAME_GAME_OBJECTS_CONTROLLABLE_ACTOR_H
#define MULTIPLAYER_FOOTBALL_SERVER_GAME_GAME_OBJECTS_CONTROLLABLE_ACTOR_H

#include <Commons/Game/GameObject.hpp>

class ControllableActor : public GameObject
{
public:
	ControllableActor();
	~ControllableActor();

	void MoveIntoDirection( sf::Vector2f direction );
	void SetSprinting( bool isSprinting );
	void ResetSprintingTimers();

	void Update( sf::Time elapsedTime ) override;
	void DrawToWindow( sf::RenderWindow& targetWindow, Camera* camera ) override;
	void CalculateBoundingBox() override;

	sf::Vector2f GetEffectiveVelocity() const;

protected:
	void OnCollisionEnter( Collision collision ) override;

private:
	sf::Sprite _sprite;

	float _movementSpeed;
	float _kickStrength;

	bool _isSprinting;
	float _sprintingSpeedMultiplier;
	float _maxSprintTime;
	float _remainingSprintTime;
	float _sprintingRefillPerSecond;

	sf::Vector2f _velocity;
	sf::Vector2f _effectiveVelocity;
};

#endif // MULTIPLAYER_FOOTBALL_SERVER_GAME_GAME_OBJECTS_CONTROLLABLE_ACTOR_H