﻿#ifndef MULTIPLAYER_FOOTBALL_SERVER_GAME_GAME_OBJECTS_FOOTBALL_H
#define MULTIPLAYER_FOOTBALL_SERVER_GAME_GAME_OBJECTS_FOOTBALL_H

#include <Commons/Game/GameObject.hpp>
#include <functional>

class Football : public GameObject
{
public:
	Football();
	~Football() override;
	void Update( sf::Time elapsedTime ) override;
	void DrawToWindow( sf::RenderWindow& targetWindow, Camera* camera ) override;
	void CalculateBoundingBox() override;

	void KickIntoDirection( const sf::Vector2f& direction, float kickVelocity );
	void Stop();

	sf::Vector2f GetEffectiveVelocity() const;

	void SubscribeToOnKickEvent( std::function<void()> callback );
	void UnsubscribeFromOnKickEvent();


protected:
	void OnCollisionEnter( Collision collision ) override;


private:
	sf::Sprite _sprite;

	sf::Vector2f _velocity;
	float _brakeSpeedPerSecond;

	std::function<void()> _onKickEvent;
};

#endif // MULTIPLAYER_FOOTBALL_SERVER_GAME_GAME_OBJECTS_FOOTBALL_H