﻿#include "ControllableActor.hpp"

#include <Commons/Game/Camera.hpp>
#include <Commons/Console/Console.hpp>

#include <Commons/FileManagers/TextureManager.hpp>
#include <Commons/Game/Physics/CircleCollider.hpp>
#include <Commons/Math/SfmlVectorMath.hpp>
#include "Football.hpp"

ControllableActor::ControllableActor()
{
	sf::Texture* texture = TextureManager::GetTexture( "Sprites/player.png" );
	_sprite.setTexture( *texture );

	GetTransform().SetOrigin( sf::Vector2f( texture->getSize() ) / 2.0f );

	SetCollider( new CircleCollider( 32.0f, this ) );

	_movementSpeed = 300.0f;
	_kickStrength = 500.0f;
	
	_sprintingSpeedMultiplier = 2.0f;
	_maxSprintTime = 0.5f;
	_remainingSprintTime = _maxSprintTime;
	_sprintingRefillPerSecond = 0.125f;
	_isSprinting = false;
}

ControllableActor::~ControllableActor()
{
}

void ControllableActor::MoveIntoDirection( sf::Vector2f direction )
{
	if(sf::VectorMath::Magnitude2f( direction ) > 1.0f )
	{
		direction = sf::VectorMath::GetNormalized( direction );
	}

	_velocity = direction;
}

void ControllableActor::SetSprinting( bool isSprinting )
{
	_isSprinting = isSprinting;
}

void ControllableActor::ResetSprintingTimers()
{
	_remainingSprintTime = _maxSprintTime;
	_isSprinting = false;
}

void ControllableActor::Update( sf::Time elapsedTime )
{
	sf::Vector2f direction = _velocity * _movementSpeed;

	if ( _isSprinting )
	{
		_remainingSprintTime -= elapsedTime.asSeconds();
		_remainingSprintTime = _remainingSprintTime < 0.0f ? 0.0f : _remainingSprintTime;

		if ( _remainingSprintTime > 0.0f )
		{
			direction *= _sprintingSpeedMultiplier;
		}
	}
	else
	{
		_remainingSprintTime += elapsedTime.asSeconds() * _sprintingRefillPerSecond;
		_remainingSprintTime = _remainingSprintTime > _maxSprintTime ? _maxSprintTime : _remainingSprintTime;
	}

	sf::Vector2f previousPosition = GetTransform().GetPosition();
	GetTransform().Translate( direction * elapsedTime.asSeconds() );
	_effectiveVelocity = ( GetTransform().GetPosition() - previousPosition ) / elapsedTime.asSeconds();
}

void ControllableActor::DrawToWindow( sf::RenderWindow& targetWindow, Camera* camera )
{
	Transform drawTransform = camera->LocalTransformToCameraTransform( GetTransform() );
	targetWindow.draw( _sprite, drawTransform.GetSfmlTransform() );
}

void ControllableActor::CalculateBoundingBox()
{
	CalculateBoundingBoxFromSprite( _sprite );
}

sf::Vector2f ControllableActor::GetEffectiveVelocity() const
{
	return _effectiveVelocity;
}

void ControllableActor::OnCollisionEnter( Collision collision )
{
	GameObject* otherGameObject = collision.OtherCollider->GetParentGameObject();
	if ( otherGameObject == nullptr || otherGameObject->HasTag( "Football" ) == false )
	{
		return;
	}

	// Oh boy this is hacky, but whatever.
	Football* football = static_cast<Football*>(otherGameObject);

	sf::Vector2f kickDirection = football->GetTransform().GetPosition() - GetTransform().GetPosition();

	float movementSpeedPercentage = sf::VectorMath::Magnitude2f( _velocity );
	football->KickIntoDirection( kickDirection, movementSpeedPercentage * _kickStrength );
}
