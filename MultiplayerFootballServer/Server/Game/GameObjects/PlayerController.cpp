﻿#include "PlayerController.hpp"

#include <Server/Game/GameObjects/ControllableActor.hpp>
#include <math.h>

PlayerController::PlayerController()
{
	_firstActor = nullptr;
	_secondActor = nullptr;
}

PlayerController::PlayerController( ControllableActor* firstActor, ControllableActor* secondActor )
{
	_firstActor = firstActor;
	_secondActor = secondActor;
}

PlayerController::~PlayerController()
{
}

void PlayerController::Update( sf::Time elapsedTime )
{
	int controllerId = 0;
	float controllerAxisDeadZone = 20.0f;

	if ( sf::Joystick::isConnected( controllerId ) == false )
	{
		return;
	}

	float xInputLeft = sf::Joystick::getAxisPosition( controllerId, sf::Joystick::X );
	float yInputLeft = sf::Joystick::getAxisPosition( controllerId, sf::Joystick::Y );
	float xInputRight = sf::Joystick::getAxisPosition( controllerId, sf::Joystick::U );
	float yInputRight = sf::Joystick::getAxisPosition( controllerId, sf::Joystick::R );

	bool isLeftShoulderButtonPressed = sf::Joystick::isButtonPressed( controllerId, 4 );
	bool isRightShoulderButtonPressed = sf::Joystick::isButtonPressed( controllerId, 5 );

	xInputLeft = abs( xInputLeft ) >= controllerAxisDeadZone ? xInputLeft : 0.0f;
	yInputLeft = abs( yInputLeft ) >= controllerAxisDeadZone ? yInputLeft : 0.0f;
	xInputRight = abs( xInputRight ) >= controllerAxisDeadZone ? xInputRight : 0.0f;
	yInputRight = abs( yInputRight ) >= controllerAxisDeadZone ? yInputRight : 0.0f;

	sf::Vector2f directionLeft( xInputLeft, yInputLeft );
	directionLeft /= 100.0f;

	sf::Vector2f directionRight( xInputRight, yInputRight );
	directionRight /= 100.0f;

	if ( _firstActor != nullptr )
	{
		_firstActor->MoveIntoDirection( directionLeft );
		_firstActor->SetSprinting( isLeftShoulderButtonPressed );
	}

	if ( _secondActor != nullptr )
	{
		_secondActor->MoveIntoDirection( directionRight );
		_secondActor->SetSprinting( isRightShoulderButtonPressed );
	}
}
