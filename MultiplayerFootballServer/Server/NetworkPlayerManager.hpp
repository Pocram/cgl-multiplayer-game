﻿#ifndef SERVER_NETWORK_PLAYER_MANAGER_H
#define SERVER_NETWORK_PLAYER_MANAGER_H

#include <vector>
#include <string>
#include <functional>

#include <Server/NetworkPlayerInfo.hpp>

class NetworkPlayerManager
{
public:
	NetworkPlayerManager();

	void AddPlayer( NetworkPlayerInfo* playerInfo );
	void AddPlayer( std::string playerName, SocketConnection* connection );

	const NetworkPlayerInfo* GetPlayerOne() const;
	const NetworkPlayerInfo* GetPlayerTwo() const;

	bool RemovePlayerOne();
	bool RemovePlayerTwo();

	// Returns true when a player has been moved. False if not.
	bool MoveFirstSpectatorIntoFirstEmptyTeam();

	std::vector<NetworkPlayerInfo*> GetAllConnectedPlayers() const;
	NetworkPlayerInfo* FindPlayerBySocketConnection( SocketConnection* connection ) const;

	void RemoveTimedOutPlayers( float maximumTimeSinceLastReport );

	void SubscribeToOnTeamsRearrangeEvent( std::function<void()> callback );
	void UnsubscribeFromOnTeamsRearrangeEvent();

private:
	void SendNewTeamMessageToPlayer( NetworkPlayerInfo* player, int teamId ) const;

	NetworkPlayerInfo* _playerOne;
	NetworkPlayerInfo* _playerTwo;

	std::vector<NetworkPlayerInfo*> _connectedPlayers;
	std::function<void()> _onTeamsRearrangeEvent;
};

#endif // SERVER_NETWORK_PLAYER_MANAGER_H
