﻿#ifndef SERVER_GAME_NETWORK_PLAYER_INFO_H
#define SERVER_GAME_NETWORK_PLAYER_INFO_H

#include <string>
#include <SFML/System/Clock.hpp>

class SocketConnection;

class NetworkPlayerInfo
{
public:
	NetworkPlayerInfo();
	NetworkPlayerInfo( std::string playerName, SocketConnection* playerConnection );

	void SetPlayerName( const std::string& playerName );
	void SetConnection( SocketConnection* connection );

	const std::string& GetPlayerName() const;
	SocketConnection* GetConnection() const;

	void ResetActivityClock();
	bool HasValidActivityTime( float maximumTimeSinceLastReport ) const;

private:
	std::string _playerName;
	SocketConnection* _connection;

	sf::Clock _playerActivityClock;
};

#endif // SERVER_GAME_NETWORK_PLAYER_INFO_H