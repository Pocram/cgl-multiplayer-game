﻿#ifndef SERVER_FOOTBALL_GAME_TCP_SERVER_H
#define SERVER_FOOTBALL_GAME_TCP_SERVER_H

#include <Commons/Networking/TcpServer.hpp>
#include <Commons/Networking/SocketConnection.hpp>
#include "NetworkPlayerManager.hpp"

#include <SFML/Graphics.hpp>
#include <functional>

class ServerFootballScene;
class ControllableActor;
class Football;

namespace sf {
	class Time;
}

class FootballGameTcpServer : public TcpServer<SocketConnection>
{
public:
	FootballGameTcpServer();
	FootballGameTcpServer( ControllableActor* playerOneA, ControllableActor* playerOneB, ControllableActor* playerTwoA, ControllableActor* playerTwoB, Football* football, ServerFootballScene* scene );

	void Update( sf::Time elapsedTime );

	const NetworkPlayerInfo* GetPlayerOne() const;
	const NetworkPlayerInfo* GetPlayerTwo() const;

	void SubscribeToOnTeamsRearrangeEvent( std::function<void()> callback );
	void UnsubscribeFromOnTeamsRearrangeEvent();

protected:
	void OnClientConnectEvent( SocketConnection* client ) override;
	void OnMessageReceive( std::string message, SocketConnection& socketConnection ) override;


private:
	std::string GetGreetingMessage() const;

	std::string GetPlayerPositionMessage() const;
	std::string GetPlayerVelocityMessage() const;
	std::string GetFootballInfoMessage() const;
	std::string GetPlayerNamesMessage() const;

	void SendPlayerNamesToClients() const;

	void OnTeamsRearrange();
	std::function<void()> _onTeamsRearrangeEvent;

	ServerFootballScene* _gameScene;

	ControllableActor* _playerOneA;
	ControllableActor* _playerOneB;
	ControllableActor* _playerTwoA;
	ControllableActor* _playerTwoB;
	Football* _football;

	int _numberOfCurrentPlayers;
	int _maxNumberOfPlayers;

	NetworkPlayerManager _playerManager;

	sf::Clock _updateClock;
	float _timeBetweenSceneSendings;
	sf::Clock _clientTimeOutCheckClock;
	float _maxTimeUntilClientTimeOut;
	float _timeBetweenClientTimeOutChecks;
};

#endif // SERVER_FOOTBALL_GAME_TCP_SERVER_H