﻿#include "NetworkPlayerManager.hpp"
#include <Commons/Networking/SocketConnection.hpp>

#include <sstream>
#include <algorithm>
#include <Commons/Console/Console.hpp>

NetworkPlayerManager::NetworkPlayerManager()
{
	_playerOne = nullptr;
	_playerTwo = nullptr;
	_onTeamsRearrangeEvent = nullptr;
}

void NetworkPlayerManager::AddPlayer( NetworkPlayerInfo* playerInfo )
{
	if( playerInfo == nullptr )
	{
		return;
	}

	_connectedPlayers.push_back( playerInfo );
	if( MoveFirstSpectatorIntoFirstEmptyTeam() == false )
	{
		WriteWarningWithCaller( "Player " + playerInfo->GetPlayerName() + " was moved to the player queue ..." );
		SendNewTeamMessageToPlayer( playerInfo, 0 );
	}
}

void NetworkPlayerManager::AddPlayer( std::string playerName, SocketConnection* connection )
{
	NetworkPlayerInfo* newInfo = new NetworkPlayerInfo( playerName, connection );
	AddPlayer( newInfo );
}

const NetworkPlayerInfo* NetworkPlayerManager::GetPlayerOne() const
{
	return _playerOne;
}

const NetworkPlayerInfo* NetworkPlayerManager::GetPlayerTwo() const
{
	return _playerTwo;
}

bool NetworkPlayerManager::RemovePlayerOne()
{
	if( _playerOne == nullptr )
	{
		return false;
	}

	delete( _playerOne );
	_playerOne = nullptr;
	return true;
}

bool NetworkPlayerManager::RemovePlayerTwo()
{
	if ( _playerTwo == nullptr )
	{
		return false;
	}

	delete( _playerTwo );
	_playerTwo = nullptr;
	return true;

}

std::vector<NetworkPlayerInfo*> NetworkPlayerManager::GetAllConnectedPlayers() const
{
	std::vector<NetworkPlayerInfo*> allPlayers = _connectedPlayers;
	allPlayers.push_back( _playerOne );
	allPlayers.push_back( _playerTwo );
	return allPlayers;
}

NetworkPlayerInfo* NetworkPlayerManager::FindPlayerBySocketConnection( SocketConnection* connection ) const
{
	if( _playerOne != nullptr && _playerOne->GetConnection() == connection )
	{
		return _playerOne;
	}

	if ( _playerTwo != nullptr && _playerTwo->GetConnection() == connection )
	{
		return _playerTwo;
	}

	for ( int i = 0; i < _connectedPlayers.size(); i++ )
	{
		if( _connectedPlayers[i] != nullptr && _connectedPlayers[i]->GetConnection() == connection )
		{
			return _connectedPlayers[i];
		}
	}

	return nullptr;
}

void NetworkPlayerManager::RemoveTimedOutPlayers( float maximumTimeSinceLastReport )
{
	std::vector<NetworkPlayerInfo*> timedOutPlayers;
	
	// Find inactive players.
	for ( int i = 0; i < _connectedPlayers.size(); i++ )
	{
		if ( _connectedPlayers[i] == nullptr )
		{
			continue;
		}

		if ( _connectedPlayers[i]->GetConnection() == nullptr || _connectedPlayers[i]->HasValidActivityTime( maximumTimeSinceLastReport ) == false )
		{
			timedOutPlayers.push_back( _connectedPlayers[i] );
		}
	}

	if( _playerOne != nullptr && _playerOne->HasValidActivityTime( maximumTimeSinceLastReport ) == false )
	{
		WriteWarningWithCaller( "Player One has timed out!" );
		RemovePlayerOne();
	}

	if ( _playerTwo != nullptr && _playerTwo->HasValidActivityTime( maximumTimeSinceLastReport ) == false )
	{
		WriteWarningWithCaller( "Player Two has timed out!" );
		RemovePlayerTwo();
	}

	// Remove inactive players.
	for ( int i = 0; i < timedOutPlayers.size(); i++ )
	{
		if ( _connectedPlayers.erase( std::remove( _connectedPlayers.begin(), _connectedPlayers.end(), timedOutPlayers[i] ), _connectedPlayers.end() ) != _connectedPlayers.begin() )
		{
			WriteWarningWithCaller( "Inactive players have been removed from the queue!" );
		}
	}
}

void NetworkPlayerManager::SubscribeToOnTeamsRearrangeEvent( std::function<void()> callback )
{
	_onTeamsRearrangeEvent = callback;
}

void NetworkPlayerManager::UnsubscribeFromOnTeamsRearrangeEvent()
{
	_onTeamsRearrangeEvent = nullptr;
}

void NetworkPlayerManager::SendNewTeamMessageToPlayer( NetworkPlayerInfo* player, int teamId ) const
{
	if(player->GetConnection() != nullptr)
	{
		std::stringstream ss;
		ss << "assignTeam{" << teamId << "}";
		player->GetConnection()->SendMessageToServer( ss.str() );
	}
}

bool NetworkPlayerManager::MoveFirstSpectatorIntoFirstEmptyTeam()
{
	if ( _connectedPlayers.size() == 0 && ( _playerOne == nullptr || _playerTwo == nullptr ) )
	{
		return false;
	}

	WriteLineWithCaller( "Filling up teams ..." );

	// Remove all nullptr players.
	_connectedPlayers.erase( std::remove( _connectedPlayers.begin(), _connectedPlayers.end(), nullptr ), _connectedPlayers.end() );

	bool playerHasBeenMoved = false;
	while ( _playerOne == nullptr || _playerTwo == nullptr )
	{
		if( _connectedPlayers.size() <= 0 )
		{
			break;
		}

		// Pop first player in queue.
		NetworkPlayerInfo* player = _connectedPlayers[0];
		_connectedPlayers.erase( _connectedPlayers.begin() );

		if ( _playerOne == nullptr )
		{
			_playerOne = player;
			SendNewTeamMessageToPlayer( player, 1 );
			playerHasBeenMoved = true;
			WriteLineWithCaller( "Player " + _playerOne->GetPlayerName() + " is now player One!" );
		}
		else if ( _playerTwo == nullptr )
		{
			_playerTwo = player;
			SendNewTeamMessageToPlayer( player, 2 );
			playerHasBeenMoved = true;
			WriteLineWithCaller( "Player " + _playerOne->GetPlayerName() + " is now player Two!" );
		}
	}

	if( playerHasBeenMoved && _onTeamsRearrangeEvent != nullptr )
	{
		_onTeamsRearrangeEvent();
	}

	return playerHasBeenMoved;
}
