﻿#include "FootballGameTcpServer.hpp"

#include <SFML/Graphics.hpp>
#include <Commons/Console/Console.hpp>
#include <Commons/Game/ParseablePlayerInput.hpp>

#include <Server/Game/Scenes/ServerFootballScene.hpp>
#include <Server/Game/GameObjects/ControllableActor.hpp>
#include <Server/Game/GameObjects/Football.hpp>
#include <Commons/Utility/Parsing.hpp>

#include <algorithm>

FootballGameTcpServer::FootballGameTcpServer()
{
	_playerOneA = nullptr;
	_playerOneB = nullptr;
	_playerTwoA = nullptr;
	_playerTwoB = nullptr;

	_football = nullptr;

	_numberOfCurrentPlayers = 0;
	_maxNumberOfPlayers = 2;

	_timeBetweenSceneSendings = 1.0f / 30.0f;

	_maxTimeUntilClientTimeOut = 15.0f;
	_timeBetweenClientTimeOutChecks = 3.0f;

	_onTeamsRearrangeEvent = nullptr;

	_gameScene = nullptr;
}

FootballGameTcpServer::FootballGameTcpServer( ControllableActor* playerOneA, ControllableActor* playerOneB, ControllableActor* playerTwoA, ControllableActor* playerTwoB, Football* football, ServerFootballScene* scene )
{
	_playerOneA = playerOneA;
	_playerOneB = playerOneB;
	_playerTwoA = playerTwoA;
	_playerTwoB = playerTwoB;

	_football = football;

	_numberOfCurrentPlayers = 0;
	_maxNumberOfPlayers = 2;

	_timeBetweenSceneSendings = 1.0f / 30.0f;

	_maxTimeUntilClientTimeOut = 15.0f;
	_timeBetweenClientTimeOutChecks = 3.0f;

	_playerManager.SubscribeToOnTeamsRearrangeEvent( std::bind( &FootballGameTcpServer::OnTeamsRearrange, this ) );
	_onTeamsRearrangeEvent = nullptr;

	_gameScene = scene;
}

void FootballGameTcpServer::Update( sf::Time elapsedTime )
{
	if( _updateClock.getElapsedTime().asSeconds() >= _timeBetweenSceneSendings )
	{
		_updateClock.restart();

		// Send game states to every valid connected client.
		std::vector<NetworkPlayerInfo*> allPlayers = _playerManager.GetAllConnectedPlayers();
		for ( int i = 0; i < allPlayers.size(); i++ )
		{
			if ( allPlayers[i] == nullptr || allPlayers[i]->GetConnection() == nullptr )
			{
				continue;
			}

			allPlayers[i]->GetConnection()->SendMessageToServer( GetPlayerPositionMessage() );
			allPlayers[i]->GetConnection()->SendMessageToServer( GetPlayerVelocityMessage() );
			allPlayers[i]->GetConnection()->SendMessageToServer( GetFootballInfoMessage() );
		}
	}

	// Remove timed out clients every now and then.
	if ( _clientTimeOutCheckClock.getElapsedTime().asSeconds() >= _timeBetweenClientTimeOutChecks )
	{
		_playerManager.RemoveTimedOutPlayers( _maxTimeUntilClientTimeOut );
		_playerManager.MoveFirstSpectatorIntoFirstEmptyTeam();
		SendPlayerNamesToClients();
		_clientTimeOutCheckClock.restart();
	}
}

const NetworkPlayerInfo* FootballGameTcpServer::GetPlayerOne() const
{
	return _playerManager.GetPlayerOne();
}

const NetworkPlayerInfo* FootballGameTcpServer::GetPlayerTwo() const
{
	return _playerManager.GetPlayerTwo();
}

void FootballGameTcpServer::SubscribeToOnTeamsRearrangeEvent( std::function<void()> callback )
{
	_onTeamsRearrangeEvent = callback;
}

void FootballGameTcpServer::UnsubscribeFromOnTeamsRearrangeEvent()
{
	_onTeamsRearrangeEvent = nullptr;
}

void FootballGameTcpServer::OnClientConnectEvent( SocketConnection* client )
{
	client->SendMessageToServer( GetGreetingMessage() );
}

void FootballGameTcpServer::OnMessageReceive( std::string message, SocketConnection& socketConnection )
{
	if( _gameScene != nullptr && _gameScene->GetRemainingTimeUntilKickoff() <= 0.0f )
	{
		if ( _playerManager.GetPlayerOne() != nullptr && &socketConnection == _playerManager.GetPlayerOne()->GetConnection() )
		{
			ParseablePlayerInput playerInput;
			if ( ParseablePlayerInput::ParseFromString( message, playerInput ) )
			{
				if ( _playerOneA != nullptr )
				{
					_playerOneA->MoveIntoDirection( playerInput.MovementInputLeft );
					_playerOneA->SetSprinting( playerInput.IsSprintingLeft );
					_playerOneB->MoveIntoDirection( playerInput.MovementInputRight );
					_playerOneB->SetSprinting( playerInput.IsSprintingRight );
				}
			}
		}
		else if ( _playerManager.GetPlayerTwo() != nullptr && &socketConnection == _playerManager.GetPlayerTwo()->GetConnection() )
		{
			ParseablePlayerInput playerInput;
			if ( ParseablePlayerInput::ParseFromString( message, playerInput ) )
			{
				if ( _playerTwoA != nullptr )
				{
					_playerTwoA->MoveIntoDirection( playerInput.MovementInputLeft );
					_playerTwoA->SetSprinting( playerInput.IsSprintingLeft );
					_playerTwoB->MoveIntoDirection( playerInput.MovementInputRight );
					_playerTwoB->SetSprinting( playerInput.IsSprintingRight );
				}
			}
		}
	}

	if ( message.find( "joinGame{" ) == 0 )
	{
		std::vector<std::string> messageElements = Parsing::FromStringToVector( message, "joinGame" );
		if ( messageElements.size() != 1 )
		{
			return;
		}

		std::string playerName = messageElements[0];
		WriteWarningWithCaller( "Player " + playerName + " connected to the game!" );

		_playerManager.AddPlayer( playerName, &socketConnection );

		return;
	}

	if ( message.find( "iAmStillHere{}" ) == 0 )
	{
		NetworkPlayerInfo* player = _playerManager.FindPlayerBySocketConnection( &socketConnection );
		if( player != nullptr )
		{
			player->ResetActivityClock();
		}
	}
}

std::string FootballGameTcpServer::GetGreetingMessage() const
{
	std::stringstream ss;
	ss << "footballWelcome{" << 1 << "}";
	return ss.str();
}

std::string FootballGameTcpServer::GetPlayerPositionMessage() const
{
	std::stringstream ss;
	ss << "playerPositions{";

	// Player A
	ss << _playerOneA->GetTransform().GetPosition().x << ";";
	ss << _playerOneA->GetTransform().GetPosition().y << ";";
	ss << _playerOneB->GetTransform().GetPosition().x << ";";
	ss << _playerOneB->GetTransform().GetPosition().y << ";";

	// Player B
	ss << _playerTwoA->GetTransform().GetPosition().x << ";";
	ss << _playerTwoA->GetTransform().GetPosition().y << ";";
	ss << _playerTwoB->GetTransform().GetPosition().x << ";";
	ss << _playerTwoB->GetTransform().GetPosition().y << ";";

	ss << "}";

	return ss.str();
}

std::string FootballGameTcpServer::GetPlayerVelocityMessage() const
{
	std::stringstream ss;
	ss << "playerVelocities{";

	// Player A
	ss << _playerOneA->GetEffectiveVelocity().x << ";";
	ss << _playerOneA->GetEffectiveVelocity().y << ";";
	ss << _playerOneB->GetEffectiveVelocity().x << ";";
	ss << _playerOneB->GetEffectiveVelocity().y << ";";

	// Player B
	ss << _playerTwoA->GetEffectiveVelocity().x << ";";
	ss << _playerTwoA->GetEffectiveVelocity().y << ";";
	ss << _playerTwoB->GetEffectiveVelocity().x << ";";
	ss << _playerTwoB->GetEffectiveVelocity().y << ";";

	ss << "}";

	return ss.str();
}

std::string FootballGameTcpServer::GetFootballInfoMessage() const
{
	std::stringstream ss;
	ss << "footballInfo{";

	// Position
	ss << _football->GetTransform().GetPosition().x << ";";
	ss << _football->GetTransform().GetPosition().y << ";";

	// Velocity
	ss << _football->GetEffectiveVelocity().x << ";";
	ss << _football->GetEffectiveVelocity().y << ";";

	ss << "}";

	return ss.str();
}

std::string FootballGameTcpServer::GetPlayerNamesMessage() const
{
	const NetworkPlayerInfo* playerOne = GetPlayerOne();
	const NetworkPlayerInfo* playerTwo = GetPlayerTwo();

	std::string playerOneName = "No Player";
	if( playerOne != nullptr )
	{
		playerOneName = playerOne->GetPlayerName();
	}

	std::string playerTwoName = "No Player";
	if ( playerTwo != nullptr )
	{
		playerTwoName = playerTwo->GetPlayerName();
	}

	std::stringstream ss;
	ss << "playerNames{";
	ss << playerOneName;
	ss << ";";
	ss << playerTwoName;
	ss << "}";
	return ss.str();
}

void FootballGameTcpServer::SendPlayerNamesToClients() const
{
	std::string nameMessage = GetPlayerNamesMessage();
	std::vector<NetworkPlayerInfo*> allPlayers = _playerManager.GetAllConnectedPlayers();
	for ( int i = 0; i < allPlayers.size(); i++ )
	{
		if ( allPlayers[i] == nullptr || allPlayers[i]->GetConnection() == nullptr )
		{
			continue;
		}

		allPlayers[i]->GetConnection()->SendMessageToServer( nameMessage );
	}
}

void FootballGameTcpServer::OnTeamsRearrange()
{
	SendPlayerNamesToClients();
	if( _onTeamsRearrangeEvent != nullptr )
	{
		_onTeamsRearrangeEvent();
	}
}
