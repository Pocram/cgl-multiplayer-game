﻿#include "NetworkPlayerInfo.hpp"

NetworkPlayerInfo::NetworkPlayerInfo()
{
	_connection = nullptr;
}

NetworkPlayerInfo::NetworkPlayerInfo( std::string playerName, SocketConnection* playerConnection )
{
	SetPlayerName( playerName );
	SetConnection( playerConnection );
}

void NetworkPlayerInfo::SetPlayerName( const std::string& playerName )
{
	_playerName = playerName;
}

void NetworkPlayerInfo::SetConnection( SocketConnection* connection )
{
	_connection = connection;
}

const std::string& NetworkPlayerInfo::GetPlayerName() const
{
	return _playerName;
}

SocketConnection* NetworkPlayerInfo::GetConnection() const
{
	return _connection;
}

void NetworkPlayerInfo::ResetActivityClock()
{
	_playerActivityClock.restart();
}

bool NetworkPlayerInfo::HasValidActivityTime( float maximumTimeSinceLastReport ) const
{
	return _playerActivityClock.getElapsedTime().asSeconds() < maximumTimeSinceLastReport;
}
