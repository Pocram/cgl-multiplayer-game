#define _WINSOCKAPI_
#include <stdio.h>

#include <Commons/Game/Game.hpp>

#include <windows.h>
#include <WinSock2.h>

#include <Commons/FileManagers/MainFileManager.hpp>
#include <Commons/FileManagers/TextureManager.hpp>
#include <Commons/FileManagers/SoundManager.hpp>
#include <Commons/FileManagers/FontManager.hpp>
#include <Server/Game/Scenes/ServerFootballScene.hpp>

int main( int argc, char* argv[] )
{
#ifndef WIN32
	printf( "This program is written for windows only!\nPress any key to exit.\n" );
	getchar();
	return 0;
#endif

	// SET UP
	//====================================================================================================
	// Enable networking
	WSAData wsData;
	WSAStartup( MAKEWORD( 2, 2 ), &wsData );

	srand( time( nullptr ) );

	printf( "Loading Assets ...\n" );
	sf::Clock assetLoadingClock;
	assetLoadingClock.restart();

	MainFileManager::Initialize( argv );
	TextureManager::Initialize();
	SoundManager::Initialize();
	FontManager::Initialize();

	printf( "Finished loading assets after %f seconds!\n", assetLoadingClock.getElapsedTime().asSeconds() );

	ServerFootballScene* scene = new ServerFootballScene();

	Game& game = Game::GetInstance();
	game.SetTargetFramerate( 144 );
	game.SetEnableVSynch( true );
	game.SetGameName( "Multiplayer Football Server" );
	game.SetWindowDimensionsByWidth( 1500, 16.0f / 9.0f );

	game.LoadScene( scene );

	game.Start();

	printf( "--------------------------------------------------------------------------------\n" );
	printf( "The game has ended.\nPress any key to close the application." );
	getchar();

	// CLEAN UP
	//====================================================================================================
	FreeConsole();
	WSACleanup();
	return 0;
}
