﻿#ifndef CLIENT_GAME_SCENES_FOOTBALL_GAME_SCENE_H
#define CLIENT_GAME_SCENES_FOOTBALL_GAME_SCENE_H

#include <Commons/Game/GameScene.hpp>
#include <Client/Game/FootballGameTcpClient.hpp>

#include <Client/Game/GameObjects/ClientPlayerController.hpp>

#include <SFML/Audio.hpp>
#include <Commons/Sound/RandomSoundContainer.hpp>

class SimpleSpriteObject;
class SimpleTextObject;
class ServerControlledObject;
class ClientFootball;

class FootballGameScene : public GameScene
{
	friend class FootballGameTcpClient;

public:
	FootballGameScene();
	FootballGameScene( std::string serverAddress, std::string playerName );

	void Update( sf::Time elapsedTime ) override;


private:
	void InstantiateAllObjects();
	void LoadSounds();

	void ResetPositions();

	void UpdatePlayerMarkers( sf::Time elapsedTime );

	void HandleGoalCheerLoop();

	FootballGameTcpClient _gameClient;
	ClientPlayerController _playerController;

	SimpleTextObject* _leftScoreText;
	SimpleTextObject* _rightScoreText;
	SimpleSpriteObject* _fieldBackground;
	
	SimpleSpriteObject* _leftGoal;
	SimpleSpriteObject* _rightGoal;

	//ServerControlledObject* _football;
	ClientFootball* _football;
	
	ServerControlledObject* _playerOneA;
	ServerControlledObject* _playerOneB;
	ServerControlledObject* _playerTwoA;
	ServerControlledObject* _playerTwoB;

	SimpleSpriteObject* _playerMarkerA;
	SimpleSpriteObject* _playerMarkerB;

	SimpleTextObject* _playerOneNameText;
	SimpleTextObject* _playerOneNameTextShadow;
	SimpleTextObject* _playerTwoNameText;
	SimpleTextObject* _playerTwoNameTextShadow;

	SimpleTextObject* _kickoffTimer;
	SimpleTextObject* _kickoffTimerShadow;


	// Sounds
	sf::Sound _matchStartSound;
	RandomSoundContainer _kickSounds;
	RandomSoundContainer _cheerSounds;
	sf::Sound _cheerSoundLoop;
};

#endif // CLIENT_GAME_SCENES_FOOTBALL_GAME_SCENE_H