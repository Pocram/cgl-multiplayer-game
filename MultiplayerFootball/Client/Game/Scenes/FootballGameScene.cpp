﻿#include "FootballGameScene.hpp"
#include <Commons/Console/Console.hpp>

#include <Client/Game/GameObjects/ClientPlayerController.hpp>
#include <Client/Game/GameObjects/ServerControlledObject.hpp>
#include <Client/Game/GameObjects/ClientFootball.hpp>

#include <Commons/FileManagers/FontManager.hpp>
#include <Commons/FileManagers/TextureManager.hpp>

#include <Commons/Game/GameObjects/SimpleTextObject.hpp>
#include <Commons/Game/GameObjects/SimpleSpriteObject.hpp>
#include <Commons/FileManagers/SoundManager.hpp>
#include <Commons/Math/SfmlVectorMath.hpp>
#include <Commons/Math/Math.hpp>
#include <Commons/Game/Physics/BoxCollider.hpp>

FootballGameScene::FootballGameScene()
{
	InstantiateAllObjects();
	LoadSounds();

	_gameClient.SetAssociatedScene( this );
	_gameClient.SetUp( "127.0.0.1", 20000 );
	_gameClient.StartClient();

	_playerController = ClientPlayerController();
}

FootballGameScene::FootballGameScene( std::string serverAddress, std::string playerName )
{
	InstantiateAllObjects();
	LoadSounds();

	_gameClient.SetAssociatedScene( this );
	_gameClient.SetPlayerName( playerName );
	_gameClient.SetUp( serverAddress, 20000 );
	_gameClient.StartClient();

	_playerController = ClientPlayerController();
}

void FootballGameScene::Update( sf::Time elapsedTime )
{
	static sf::Clock inputClock;
	static sf::Clock antiTimeoutClock;
	
	// Send input every few milliseconds.
	if ( inputClock.getElapsedTime().asSeconds() >= 0.005f && _gameClient.GetRemainingTimeUntilKickoff() <= 0.0f )
	{
		ParseablePlayerInput playerInput;
		if ( _playerController.CheckForNewInput( playerInput ) )
		{
			std::string parsedPlayerInput = playerInput.ParseToString();
			_gameClient.SendMessageToServer( parsedPlayerInput );
		}

		inputClock.restart();
	}

	UpdatePlayerMarkers( elapsedTime );

	// Let the server know you are still there to prevent a timeout!
	if ( antiTimeoutClock.getElapsedTime().asSeconds() >= 5.0f )
	{
		_gameClient.SendMessageToServer( "iAmStillHere{}" );
		antiTimeoutClock.restart();
	}

	HandleGoalCheerLoop();
}

void FootballGameScene::InstantiateAllObjects()
{
	// MOVING OBJECTS
	// -------------------------------------------------------------------------
	_football = new ClientFootball();
	_football->SetDrawOrderIndex( 500 );
	InstantiateGameObject( _football );

	_playerOneA = new ServerControlledObject( TextureManager::GetTexture( "Sprites/player_one_l.png" ) );
	_playerOneA->SetName( "Player 1 A" );
	_playerOneA->SetDrawOrderIndex( 100 );
	InstantiateGameObject( _playerOneA );

	_playerOneB = new ServerControlledObject( TextureManager::GetTexture( "Sprites/player_one_r.png" ) );
	_playerOneB->SetName( "Player 1 B" );
	_playerOneB->SetDrawOrderIndex( 150 );
	InstantiateGameObject( _playerOneB );

	_playerTwoA = new ServerControlledObject( TextureManager::GetTexture( "Sprites/player_two_l.png" ) );
	_playerTwoA->SetName( "Player 2 A" );
	_playerTwoA->SetDrawOrderIndex( 100 );
	InstantiateGameObject( _playerTwoA );

	_playerTwoB = new ServerControlledObject( TextureManager::GetTexture( "Sprites/player_two_r.png" ) );
	_playerTwoB->SetName( "Player 2 B" );
	_playerTwoB->SetDrawOrderIndex( 150 );
	InstantiateGameObject( _playerTwoB );

	_playerMarkerA = new SimpleSpriteObject( TextureManager::GetTexture( "Sprites/playe_marker.png" ), sf::Vector2f( 64.0f, 64.0f ) );
	_playerMarkerA->GetTransform().SetPosition( sf::Vector2f( 300.0f, 300.0f ) );
	_playerMarkerA->SetDrawOrderIndex( 700 );
	InstantiateGameObject( _playerMarkerA );

	_playerMarkerB = new SimpleSpriteObject( TextureManager::GetTexture( "Sprites/playe_marker.png" ), sf::Vector2f( 64.0f, 64.0f ) );
	_playerMarkerB->GetTransform().SetPosition( sf::Vector2f( 600.0f, 600.0f ) );
	_playerMarkerB->SetDrawOrderIndex( 710 );
	InstantiateGameObject( _playerMarkerB );

	ResetPositions();


	// STATIC OBJECTS
	// -------------------------------------------------------------------------
	_fieldBackground = new SimpleSpriteObject( TextureManager::GetTexture( "Sprites/field_bg.png" ), sf::Vector2f( 0.0f, 0.0f ) );
	_fieldBackground->SetName( "Field Background" );
	_fieldBackground->SetDrawOrderIndex( 0 );
	InstantiateGameObject( _fieldBackground );

	_leftScoreText = new SimpleTextObject( FontManager::GetFont( "Fonts/DroidSans-Bold.ttf" ), "0", 72, sf::Color( 255, 255, 0, 63 ) );
	_leftScoreText->GetTransform().SetPosition( sf::Vector2f( 471.0f, 418.875f ) );
	_leftScoreText->SetDrawOrderIndex( 2 );
	InstantiateGameObject( _leftScoreText );

	_rightScoreText = new SimpleTextObject( FontManager::GetFont( "Fonts/DroidSans-Bold.ttf" ), "0", 72, sf::Color( 0, 0, 255, 63 ) );
	_rightScoreText->GetTransform().SetPosition( sf::Vector2f( 1500.0f - 471.0f, 418.875f ) );
	_rightScoreText->SetDrawOrderIndex( 3 );
	InstantiateGameObject( _rightScoreText );

	_leftGoal = new SimpleSpriteObject( TextureManager::GetTexture( "Sprites/goal_left.png" ), sf::Vector2f( 0.0f, 310.0f / 2.0f ) );
	_leftGoal->GetTransform().SetPosition( sf::Vector2f( 0.0f, 421.875f ) );
	_leftGoal->SetDrawOrderIndex( 550 );
	InstantiateGameObject( _leftGoal );

	_rightGoal = new SimpleSpriteObject( TextureManager::GetTexture( "Sprites/goal_right.png" ), sf::Vector2f( 0.0f, 310.0f / 2.0f ) );
	_rightGoal->GetTransform().SetPosition( sf::Vector2f( 1400.0f, 421.875f ) );
	_rightGoal->SetDrawOrderIndex( 550 );
	InstantiateGameObject( _rightGoal );


	// PLAYER NAMES
	// -------------------------------------------------------------------------
	_playerOneNameText = new SimpleTextObject( FontManager::GetFont( "Fonts/DroidSans-Bold.ttf" ), "???", 26, sf::Color( 255, 255, 255, 255 ) );
	_playerOneNameText->GetTransform().SetPosition( sf::Vector2f( 375.0f, 25.0f ) );
	_playerOneNameText->SetDrawOrderIndex( 9000 );
	InstantiateGameObject( _playerOneNameText );

	_playerOneNameTextShadow = new SimpleTextObject( FontManager::GetFont( "Fonts/DroidSans-Bold.ttf" ), "???", 26, sf::Color( 0, 0, 0, 255 ) );
	_playerOneNameTextShadow->GetTransform().SetPosition( sf::Vector2f( 375.0f, 25.0f ) + sf::Vector2f( 2.0f, 2.0f ) );
	_playerOneNameTextShadow->SetDrawOrderIndex( 8999 );
	InstantiateGameObject( _playerOneNameTextShadow );

	_playerTwoNameText = new SimpleTextObject( FontManager::GetFont( "Fonts/DroidSans-Bold.ttf" ), "???", 26, sf::Color( 255, 255, 255, 255 ) );
	_playerTwoNameText->GetTransform().SetPosition( sf::Vector2f( 750.0f + 375.0f, 25.0f ) );
	_playerTwoNameText->SetDrawOrderIndex( 9000 );
	InstantiateGameObject( _playerTwoNameText );

	_playerTwoNameTextShadow = new SimpleTextObject( FontManager::GetFont( "Fonts/DroidSans-Bold.ttf" ), "???", 26, sf::Color( 0, 0, 0, 255 ) );
	_playerTwoNameTextShadow->GetTransform().SetPosition( sf::Vector2f( 750.0f + 375.0f, 25.0f ) + sf::Vector2f( 2.0f, 2.0f ) );
	_playerTwoNameTextShadow->SetDrawOrderIndex( 8999 );
	InstantiateGameObject( _playerTwoNameTextShadow );


	// TIMERS
	// -------------------------------------------------------------------------
	_kickoffTimer = new SimpleTextObject( FontManager::GetFont( "Fonts/DroidSans-Bold.ttf" ), "???", 200, sf::Color( 255, 255, 255, 255 ) );
	_kickoffTimer->GetTransform().SetPosition( sf::Vector2f( 750.0f, 370.0f ) );
	_kickoffTimer->SetDrawOrderIndex( 10000 );
	//_kickoffTimer->SetActive( false );
	InstantiateGameObject( _kickoffTimer );

	_kickoffTimerShadow = new SimpleTextObject( FontManager::GetFont( "Fonts/DroidSans-Bold.ttf" ), "???", 200, sf::Color( 0, 0, 0, 255 ) );
	_kickoffTimerShadow->GetTransform().SetPosition( _kickoffTimer->GetTransform().GetPosition() + sf::Vector2f( 4.0f, 4.0f ) );
	_kickoffTimerShadow->SetDrawOrderIndex( 9999 );
	//_kickoffTimerShadow->SetActive( false );
	InstantiateGameObject( _kickoffTimerShadow );
}

void FootballGameScene::LoadSounds()
{
	sf::SoundBuffer* matchStartBuffer = SoundManager::GetSound( "Sound/whistle.wav" );
	if( matchStartBuffer != nullptr )
	{
		_matchStartSound.setBuffer( *matchStartBuffer );
	}

	_kickSounds.AddSounds( { "Sound/Kicks/kick01.ogg", "Sound/Kicks/kick02.ogg" } );
	_cheerSounds.AddSounds( { "Sound/Cheers/cheer01.ogg", "Sound/Cheers/cheer02.ogg", "Sound/Cheers/cheer03.ogg", "Sound/Cheers/cheer04.ogg" } );

	sf::SoundBuffer* cheerLoopBuffer = SoundManager::GetSound( "Sound/Cheers/cheer_loop.ogg" );
	if ( cheerLoopBuffer != nullptr )
	{
		_cheerSoundLoop.setBuffer( *cheerLoopBuffer );
		_cheerSoundLoop.setLoop( true );
		_cheerSoundLoop.setVolume( 0.0f );
		_cheerSoundLoop.play();
	}
}

void FootballGameScene::ResetPositions()
{
	_playerOneA->UpdateState( sf::Vector2f( 371.0f, 211.0f ), sf::Vector2f() );
	_playerOneB->UpdateState( sf::Vector2f( 374.0f, 633.0f ), sf::Vector2f() );
	_playerTwoA->UpdateState( sf::Vector2f( 1126.0f, 211.0f ), sf::Vector2f() );
	_playerTwoB->UpdateState( sf::Vector2f( 1126.0f, 633.0f ), sf::Vector2f() );
	
	_football->UpdateState( sf::Vector2f( 750.0f, 421.875f ), sf::Vector2f() );
}

void FootballGameScene::UpdatePlayerMarkers( sf::Time elapsedTime )
{
	if( _playerMarkerA == nullptr || _playerMarkerB == nullptr )
	{
		return;
	}

	bool isPlayerInATeam = _gameClient.GetTeamId() != 0;

	_playerMarkerA->SetActive( isPlayerInATeam );
	_playerMarkerB->SetActive( isPlayerInATeam );

	if( isPlayerInATeam )
	{
		sf::Vector2f playerAPosition = _gameClient.GetTeamId() == 1 ? _playerOneA->GetTransform().GetPosition() : _playerTwoA->GetTransform().GetPosition();
		sf::Vector2f playerBPosition = _gameClient.GetTeamId() == 1 ? _playerOneB->GetTransform().GetPosition() : _playerTwoB->GetTransform().GetPosition();

		_playerMarkerA->GetTransform().SetRotation( _playerMarkerA->GetTransform().GetRotation() + 30.0f * elapsedTime.asSeconds() );
		_playerMarkerA->GetTransform().SetPosition( playerAPosition );
		_playerMarkerB->GetTransform().SetRotation( _playerMarkerB->GetTransform().GetRotation() + 30.0f * elapsedTime.asSeconds() );
		_playerMarkerB->GetTransform().SetPosition( playerBPosition );
	}
}

void FootballGameScene::HandleGoalCheerLoop()
{
	sf::Vector2f leftGoalTopRightPosition( _leftGoal->GetBounds().left + _leftGoal->GetBounds().width, _leftGoal->GetBounds().top );
	sf::Vector2f leftGoalBottomRightPosition( _leftGoal->GetBounds().left + _leftGoal->GetBounds().width, _leftGoal->GetBounds().top + _leftGoal->GetBounds().height );

	sf::Vector2f rightGoalTopLeftPosition( _rightGoal->GetBounds().left, _rightGoal->GetBounds().top );
	sf::Vector2f rightGoalBottomLeftPosition( _rightGoal->GetBounds().left, _rightGoal->GetBounds().top + _leftGoal->GetBounds().height );

	sf::Vector2f outClosestPoint;

	float distanceBallLeftGoal = sf::VectorMath::DistanceOfPointToLine( leftGoalTopRightPosition, leftGoalBottomRightPosition, _football->GetTransform().GetPosition(), outClosestPoint );
	float distanceBallRightGoal = sf::VectorMath::DistanceOfPointToLine( rightGoalTopLeftPosition, rightGoalBottomLeftPosition, _football->GetTransform().GetPosition(), outClosestPoint );

	float closestDistance = distanceBallLeftGoal < distanceBallRightGoal ? distanceBallLeftGoal : distanceBallRightGoal;
	float minDistanceForSound = 800.0f;
	float distancePercentage = 1.0f - Math::Clamp01<float>( closestDistance / minDistanceForSound );
	 
	_cheerSoundLoop.setVolume( distancePercentage * 100.0f );
}
