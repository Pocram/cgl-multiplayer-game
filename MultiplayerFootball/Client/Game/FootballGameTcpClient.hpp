﻿#ifndef CLIENT_GAME_FOOTBALL_GAME_TCP_CLIENT_H
#define CLIENT_GAME_FOOTBALL_GAME_TCP_CLIENT_H

#include <Commons/Networking/TcpClient.hpp>
#include <Commons/Networking/SocketConnection.hpp>

#include <SFML/Graphics.hpp>

class FootballGameScene;

class FootballGameTcpClient : public TcpClient<SocketConnection>
{
public:
	FootballGameTcpClient();
	~FootballGameTcpClient() override;

	void Update( sf::Time elapsedTime );

	void SetAssociatedScene( FootballGameScene* scene );
	void SetPlayerName( const std::string& name );

	int GetTeamId() const;

	const float& GetRemainingTimeUntilKickoff() const;


protected:
	virtual void OnMessageReceive( std::string message, SocketConnection& socketConnection ) override;


private:
	FootballGameScene* _scene;
	std::string _playerName;
	int _teamId;

	float _remainingTimeUntilKickoff;
	float _previousRemainingTimeUntilKickoff;

};

#endif // CLIENT_GAME_FOOTBALL_GAME_TCP_CLIENT_H