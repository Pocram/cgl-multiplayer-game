﻿#ifndef CLIENT_GAME_GAME_OBJECTS_FOOTBALL_H
#define CLIENT_GAME_GAME_OBJECTS_FOOTBALL_H

#include <Commons/Game/GameObject.hpp>

class ClientFootball : public GameObject
{
public:
	ClientFootball();

	void Update( sf::Time elapsedTime ) override;
	void DrawToWindow( sf::RenderWindow& targetWindow, Camera* camera ) override;
	void CalculateBoundingBox() override;

	void SetVelocity( sf::Vector2f velocity );
	void UpdateState( sf::Vector2f newPosition, sf::Vector2f newVelocity );


private:
	sf::Texture* _tilingFootballTexture;
	sf::CircleShape _mainBallShape;
	sf::CircleShape _shadeShape;
	sf::Sprite _shadowSprite;

	sf::Vector2f _velocity;
};

#endif // CLIENT_GAME_GAME_OBJECTS_FOOTBALL_H