﻿#include "ClientPlayerController.hpp"

ClientPlayerController::ClientPlayerController()
{
	_controllerId = FindControllerId();
}

bool ClientPlayerController::CheckForNewInput( ParseablePlayerInput& outPlayerInput )
{
	_previousPlayerInput = _currentPlayerInput;
	_currentPlayerInput = GetPlayerInput();
	outPlayerInput = _currentPlayerInput;

	return _currentPlayerInput != _previousPlayerInput;
}

int ClientPlayerController::FindControllerId() const
{
	const int maxControllerCount = 4;
	for( int i = 0; i < maxControllerCount; i++ )
	{
		if ( sf::Joystick::isConnected( i ) )
		{
			return i;
		}
	}

	return 0;
}

ParseablePlayerInput ClientPlayerController::GetPlayerInput() const
{
	ParseablePlayerInput controllerInput = GetControllerInput();
	ParseablePlayerInput keyboardInput = GetKeyboardInput();
	return controllerInput + keyboardInput;
}

ParseablePlayerInput ClientPlayerController::GetControllerInput() const
{
	ParseablePlayerInput input;

	float controllerAxisDeadZone = 20.0f;

	if ( sf::Joystick::isConnected( _controllerId ) == false )
	{
		return input;
	}

	float xInputLeft = sf::Joystick::getAxisPosition( _controllerId, sf::Joystick::X );
	float yInputLeft = sf::Joystick::getAxisPosition( _controllerId, sf::Joystick::Y );
	float xInputRight = sf::Joystick::getAxisPosition( _controllerId, sf::Joystick::U );
	float yInputRight = sf::Joystick::getAxisPosition( _controllerId, sf::Joystick::R );

	bool isLeftShoulderButtonPressed = sf::Joystick::isButtonPressed( _controllerId, 4 );
	bool isRightShoulderButtonPressed = sf::Joystick::isButtonPressed( _controllerId, 5 );

	xInputLeft = abs( xInputLeft ) >= controllerAxisDeadZone ? xInputLeft : 0.0f;
	yInputLeft = abs( yInputLeft ) >= controllerAxisDeadZone ? yInputLeft : 0.0f;
	xInputRight = abs( xInputRight ) >= controllerAxisDeadZone ? xInputRight : 0.0f;
	yInputRight = abs( yInputRight ) >= controllerAxisDeadZone ? yInputRight : 0.0f;

	sf::Vector2f directionLeft( xInputLeft, yInputLeft );
	directionLeft /= 100.0f;

	sf::Vector2f directionRight( xInputRight, yInputRight );
	directionRight /= 100.0f;

	input.MovementInputLeft = directionLeft;
	input.MovementInputRight = directionRight;
	input.IsSprintingLeft = isLeftShoulderButtonPressed;
	input.IsSprintingRight = isRightShoulderButtonPressed;

	return input;
}

ParseablePlayerInput ClientPlayerController::GetKeyboardInput() const
{
	ParseablePlayerInput input;

	// Left
	if ( sf::Keyboard::isKeyPressed( sf::Keyboard::A ) )
	{
		input.MovementInputLeft.x = -1.0f;
	}
	else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::D ) )
	{
		input.MovementInputLeft.x = 1.0f;
	}

	if ( sf::Keyboard::isKeyPressed( sf::Keyboard::W ) )
	{
		input.MovementInputLeft.y = -1.0f;
	}
	else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::S ) )
	{
		input.MovementInputLeft.y = 1.0f;
	}

	if ( sf::Keyboard::isKeyPressed( sf::Keyboard::LShift ) )
	{
		input.IsSprintingLeft = true;
	}


	// Right
	if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Left ) )
	{
		input.MovementInputRight.x = -1.0f;
	}
	else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Right ) )
	{
		input.MovementInputRight.x = 1.0f;
	}

	if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Up ) )
	{
		input.MovementInputRight.y = -1.0f;
	}
	else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Down ) )
	{
		input.MovementInputRight.y = 1.0f;
	}

	if ( sf::Keyboard::isKeyPressed( sf::Keyboard::RControl ) || sf::Keyboard::isKeyPressed( sf::Keyboard::Numpad0 ) )
	{
		input.IsSprintingRight = true;
	}

	return input;
}
