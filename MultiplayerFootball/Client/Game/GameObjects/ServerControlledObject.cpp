﻿#include "ServerControlledObject.hpp"
#include <Commons/Game/Camera.hpp>
#include <Commons/Console/Console.hpp>

ServerControlledObject::ServerControlledObject()
{
}

ServerControlledObject::ServerControlledObject( sf::Texture* texture )
{
	SetTexture( texture );
}

void ServerControlledObject::Update( sf::Time elapsedTime )
{
	GetTransform().Translate( _velocity * elapsedTime.asSeconds() );
}

void ServerControlledObject::DrawToWindow( sf::RenderWindow& targetWindow, Camera* camera )
{
	Transform drawTransform = camera->LocalTransformToCameraTransform( GetTransform() );
	targetWindow.draw( _sprite, drawTransform.GetSfmlTransform() );
}

void ServerControlledObject::CalculateBoundingBox()
{
	CalculateBoundingBoxFromSprite( _sprite );
}

void ServerControlledObject::SetTexture( sf::Texture* texture )
{
	if ( texture != nullptr )
	{
		_sprite.setTexture( *texture );
		GetTransform().SetOrigin( sf::Vector2f( texture->getSize() ) / 2.0f );
	}
}

void ServerControlledObject::SetVelocity( sf::Vector2f velocity )
{
	_velocity = velocity;
}

void ServerControlledObject::UpdateState( sf::Vector2f newPosition, sf::Vector2f newVelocity )
{
	GetTransform().SetPosition( newPosition );

	_velocity = newVelocity;
}
