﻿#ifndef CLIENT_GAME_CLIENT_PLAYER_CONTROLLER_H
#define CLIENT_GAME_CLIENT_PLAYER_CONTROLLER_H

#include <Commons/Game/ParseablePlayerInput.hpp>

class ClientPlayerController
{
public:
	ClientPlayerController();
	bool CheckForNewInput( ParseablePlayerInput& outPlayerInput );

private:
	int FindControllerId() const;
	ParseablePlayerInput GetPlayerInput() const;

	ParseablePlayerInput GetControllerInput() const;
	ParseablePlayerInput GetKeyboardInput() const;

	int _controllerId;

	ParseablePlayerInput _previousPlayerInput;
	ParseablePlayerInput _currentPlayerInput;
};

#endif // CLIENT_GAME_CLIENT_PLAYER_CONTROLLER_H