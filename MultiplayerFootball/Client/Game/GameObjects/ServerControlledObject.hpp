﻿#ifndef CLIENT_GAME_GAME_OBJECTS_SERVER_CONTROLLED_OBJECT_H
#define CLIENT_GAME_GAME_OBJECTS_SERVER_CONTROLLED_OBJECT_H

#include <Commons/Game/GameObject.hpp>

#include <SFML/Graphics.hpp>

class ServerControlledObject : public GameObject
{
public:
	ServerControlledObject();
	ServerControlledObject( sf::Texture* texture );

	void Update( sf::Time elapsedTime ) override;
	void DrawToWindow( sf::RenderWindow& targetWindow, Camera* camera ) override;
	void CalculateBoundingBox() override;

	void SetTexture( sf::Texture* texture );

	void SetVelocity( sf::Vector2f velocity );
	void UpdateState( sf::Vector2f newPosition, sf::Vector2f newVelocity );


private:
	sf::Sprite _sprite;
	sf::Vector2f _velocity;

};

#endif // CLIENT_GAME_GAME_OBJECTS_SERVER_CONTROLLED_OBJECT_H