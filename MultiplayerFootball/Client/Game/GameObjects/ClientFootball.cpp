﻿#include "ClientFootball.hpp"

#include <Commons/Game/Camera.hpp>
#include <Commons/FileManagers/TextureManager.hpp>

ClientFootball::ClientFootball()
{
	_tilingFootballTexture = TextureManager::GetTexture( "Sprites/Football/pattern_tile.png" );
	if ( _tilingFootballTexture != nullptr )
	{
		_tilingFootballTexture->setRepeated( true );
		_mainBallShape.setRadius( 24.0f );
		_mainBallShape.setTexture( _tilingFootballTexture );
		_mainBallShape.setTextureRect( sf::IntRect( 0, 0, 48, 48 ) );
	}

	sf::Texture* shadowTexture = TextureManager::GetTexture( "Sprites/shadow.png" );
	if( shadowTexture != nullptr )
	{
		GetTransform().SetOrigin( sf::Vector2f( shadowTexture->getSize() ) / 2.0f );
		_shadowSprite.setTexture( *shadowTexture );
	}

	sf::Texture* shadeTexture = TextureManager::GetTexture( "Sprites/Football/ball_shade.png" );
	if ( shadeTexture != nullptr )
	{
		GetTransform().SetOrigin( sf::Vector2f( shadowTexture->getSize() ) / 2.0f );
		_shadeShape.setRadius( 24.0f );
		_shadeShape.setTexture( shadeTexture );
	}
}

void ClientFootball::Update( sf::Time elapsedTime )
{
	GetTransform().Translate( _velocity * elapsedTime.asSeconds() );
	
	// Move texture to simulate rolling ball.
	sf::IntRect previousTextureRect = _mainBallShape.getTextureRect();
	sf::IntRect newTextureRect = previousTextureRect;
	/*newTextureRect.left -= _velocity.x * elapsedTime.asSeconds();
	newTextureRect.top -= _velocity.y * elapsedTime.asSeconds();*/
	newTextureRect.left = -GetTransform().GetPosition().x * 0.5f;
	newTextureRect.top = -GetTransform().GetPosition().y * 0.5f;
	_mainBallShape.setTextureRect( newTextureRect );
}

void ClientFootball::DrawToWindow( sf::RenderWindow& targetWindow, Camera* camera )
{
	Transform drawTransform = camera->LocalTransformToCameraTransform( GetTransform() );
	
	Transform mainBallTransform = drawTransform;
	mainBallTransform.SetPosition( mainBallTransform.GetPosition() + sf::Vector2f( 8.0f, 8.0f ) );	// Values taken from Photoshop.

	targetWindow.draw( _shadowSprite, drawTransform.GetSfmlTransform() );
	targetWindow.draw( _mainBallShape, mainBallTransform.GetSfmlTransform() );
	targetWindow.draw( _shadeShape, mainBallTransform.GetSfmlTransform() );
}

void ClientFootball::CalculateBoundingBox()
{
	CalculateBoundingBoxFromSprite( _shadowSprite );
}

void ClientFootball::SetVelocity( sf::Vector2f velocity )
{
	_velocity = velocity;
}

void ClientFootball::UpdateState( sf::Vector2f newPosition, sf::Vector2f newVelocity )
{
	GetTransform().SetPosition( newPosition );

	_velocity = newVelocity;
}
