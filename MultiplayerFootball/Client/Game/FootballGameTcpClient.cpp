﻿#include "FootballGameTcpClient.hpp"
#include <Commons/Console/Console.hpp>
#include <Commons/Utility/Parsing.hpp>
#include <Commons/Utility/Conversions.hpp>
#include <Commons/Game/Game.hpp>

#include <Client/Game/Scenes/FootballGameScene.hpp>
#include <Client/Game/GameObjects/ServerControlledObject.hpp>
#include <Client/Game/GameObjects/ClientFootball.hpp>
#include <Commons/Game/GameObjects/SimpleTextObject.hpp>

#include <math.h>

FootballGameTcpClient::FootballGameTcpClient()
{
	_scene = nullptr;
	_playerName = "Anonymous";
	_teamId = 0;

	_remainingTimeUntilKickoff = 0.0f;
	_previousRemainingTimeUntilKickoff = _remainingTimeUntilKickoff;
}

FootballGameTcpClient::~FootballGameTcpClient()
{
}

void FootballGameTcpClient::Update( sf::Time elapsedTime )
{
	
}

void FootballGameTcpClient::SetAssociatedScene( FootballGameScene* scene )
{
	_scene = scene;
}

void FootballGameTcpClient::SetPlayerName( const std::string& name )
{
	_playerName = name;
}

int FootballGameTcpClient::GetTeamId() const
{
	return _teamId;
}

const float& FootballGameTcpClient::GetRemainingTimeUntilKickoff() const
{
	return _remainingTimeUntilKickoff;
}

void FootballGameTcpClient::OnMessageReceive( std::string message, SocketConnection& socketConnection )
{
	if ( message.find( "footballWelcome{" ) == 0 )
	{
		std::vector<std::string> messageElements = Parsing::FromStringToVector( message, "footballWelcome" );
		if( messageElements.size() > 0 )
		{
			// We can use [0] here, since the message only contains one element.
			bool playerMayJoinServer = Conversions::StringToNumber<bool>( messageElements[0] );
			if( playerMayJoinServer )
			{
				WriteLineWithCaller( "The server has accepted you!" );
				std::stringstream helloMessage;
				helloMessage << "joinGame{" << _playerName << "}";
				socketConnection.SendMessageToServer( helloMessage.str() );
			}
			else
			{
				WriteErrorWithCaller( "The server has denied you entry!" );
			}
		}

		return;
	}

	if ( message.find( "playerPositions{" ) == 0 )
	{
		if ( _scene == nullptr )
		{
			return;
		}

		std::vector<std::string> messageElements = Parsing::FromStringToVector( message, "playerPositions" );
		// The message should contain 8 elements: p1A.x, p1A.y, p1B.x, p1B.y, p2A.x, p2A.y, p2B.x, p2B.y
		if(messageElements.size() != 8 )
		{
			return;
		}

		float playerOneAPositionX = Conversions::StringToNumber<float>( messageElements[0] );
		float playerOneAPositionY = Conversions::StringToNumber<float>( messageElements[1] );
		float playerOneBPositionX = Conversions::StringToNumber<float>( messageElements[2] );
		float playerOneBPositionY = Conversions::StringToNumber<float>( messageElements[3] );
		sf::Vector2f playerOneAPosition( playerOneAPositionX, playerOneAPositionY );
		sf::Vector2f playerOneBPosition( playerOneBPositionX, playerOneBPositionY );


		float playerTwoAPositionX = Conversions::StringToNumber<float>( messageElements[4] );
		float playerTwoAPositionY = Conversions::StringToNumber<float>( messageElements[5] );
		float playerTwoBPositionX = Conversions::StringToNumber<float>( messageElements[6] );
		float playerTwoBPositionY = Conversions::StringToNumber<float>( messageElements[7] );
		sf::Vector2f playerTwoAPosition( playerTwoAPositionX, playerTwoAPositionY );
		sf::Vector2f playerTwoBPosition( playerTwoBPositionX, playerTwoBPositionY );

		_scene->_playerOneA->GetTransform().SetPosition( playerOneAPosition );
		_scene->_playerOneB->GetTransform().SetPosition( playerOneBPosition );
		_scene->_playerTwoA->GetTransform().SetPosition( playerTwoAPosition );
		_scene->_playerTwoB->GetTransform().SetPosition( playerTwoBPosition );

		return;
	}

	if ( message.find( "playerVelocities{" ) == 0 )
	{
		if ( _scene == nullptr )
		{
			return;
		}

		std::vector<std::string> messageElements = Parsing::FromStringToVector( message, "playerVelocities" );
		if ( messageElements.size() != 8 )
		{
			return;
		}

		float playerOneAVelocityX = Conversions::StringToNumber<float>( messageElements[0] );
		float playerOneAVelocityY = Conversions::StringToNumber<float>( messageElements[1] );
		float playerOneBVelocityX = Conversions::StringToNumber<float>( messageElements[2] );
		float playerOneBVelocityY = Conversions::StringToNumber<float>( messageElements[3] );
		sf::Vector2f playerOneAVelocity( playerOneAVelocityX, playerOneAVelocityY );
		sf::Vector2f playerOneBVelocity( playerOneBVelocityX, playerOneBVelocityY );


		float playerTwoAVelocityX = Conversions::StringToNumber<float>( messageElements[4] );
		float playerTwoAVelocityY = Conversions::StringToNumber<float>( messageElements[5] );
		float playerTwoBVelocityX = Conversions::StringToNumber<float>( messageElements[6] );
		float playerTwoBVelocityY = Conversions::StringToNumber<float>( messageElements[7] );
		sf::Vector2f playerTwoAVelocity( playerTwoAVelocityX, playerTwoAVelocityY );
		sf::Vector2f playerTwoBVelocity( playerTwoBVelocityX, playerTwoBVelocityY );

		_scene->_playerOneA->SetVelocity( playerOneAVelocity );
		_scene->_playerOneB->SetVelocity( playerOneBVelocity );
		_scene->_playerTwoA->SetVelocity( playerTwoAVelocity );
		_scene->_playerTwoB->SetVelocity( playerTwoBVelocity );

		return;
	}

	if ( message.find( "footballInfo{" ) == 0 )
	{
		if ( _scene == nullptr )
		{
			return;
		}

		std::vector<std::string> messageElements = Parsing::FromStringToVector( message, "footballInfo" );
		// The message should contain 4 elements: Positions x, y and velocity x, y
		if ( messageElements.size() != 4 )
		{
			return;
		}

		float positionX = Conversions::StringToNumber<float>( messageElements[0] );
		float positionY = Conversions::StringToNumber<float>( messageElements[1] );
		sf::Vector2f position( positionX, positionY );

		float velocityX = Conversions::StringToNumber<float>( messageElements[2] );
		float velocityY = Conversions::StringToNumber<float>( messageElements[3] );
		sf::Vector2f velocity( velocityX, velocityY );

		_scene->_football->UpdateState( position, velocity );

		return;
	}

	if ( message.find( "score{" ) == 0 )
	{
		if( _scene == nullptr )
		{
			return;
		}

		std::vector<std::string> messageElements = Parsing::FromStringToVector( message, "score" );
		// The message should contain 2 elements: left, right
		if ( messageElements.size() != 2 )
		{
			return;
		}

		//int leftScore = Conversions::StringToNumber<int>( messageElements[0] );
		//int rightScore = Conversions::StringToNumber<int>( messageElements[1] );

		_scene->_leftScoreText->SetText( messageElements[0] );
		_scene->_rightScoreText->SetText( messageElements[1] );

		return;
	}

	if ( message.find( "goal{}" ) == 0 )
	{
		if( _scene == nullptr )
		{
			return;
		}

		_scene->_cheerSounds.PlayRandomSound();
		return;
	}

	if ( message.find( "playerNames{" ) == 0 )
	{
		if ( _scene == nullptr )
		{
			return;
		}

		std::vector<std::string> messageElements = Parsing::FromStringToVector( message, "playerNames" );
		// The message should contain 2 elements: one, two
		if ( messageElements.size() != 2 )
		{
			return;
		}

		if(_scene->_playerOneNameText != nullptr) _scene->_playerOneNameText->SetText( messageElements[0] );
		if ( _scene->_playerOneNameTextShadow != nullptr ) _scene->_playerOneNameTextShadow->SetText( messageElements[0] );

		if ( _scene->_playerTwoNameText != nullptr ) _scene->_playerTwoNameText->SetText( messageElements[1] );
		if ( _scene->_playerTwoNameTextShadow != nullptr ) _scene->_playerTwoNameTextShadow->SetText( messageElements[1] );

		return;
	}

	if ( message.find( "assignTeam{" ) == 0 )
	{
		std::vector<std::string> messageElements = Parsing::FromStringToVector( message, "assignTeam" );
		// The message should contain 1 element: The team id!!!
		if ( messageElements.size() != 1 )
		{
			return;
		}

		_teamId = Conversions::StringToNumber<int>( messageElements[0] );

		std::stringstream ss;
		ss << "You have been assigned to team " << _teamId << "!";
		WriteLineWithCaller( ss.str() );

		return;
	}

	if ( message.find( "timeUntilKickoff{" ) == 0 )
	{
		if ( _scene == nullptr || _scene->_kickoffTimer == nullptr || _scene->_kickoffTimerShadow == nullptr )
		{
			return;
		}

		std::vector<std::string> messageElements = Parsing::FromStringToVector( message, "timeUntilKickoff" );
		// The message should contain 1 element: The time!
		if ( messageElements.size() != 1 )
		{
			return;
		}

		_scene->_kickoffTimer->SetActive( true );
		_scene->_kickoffTimerShadow->SetActive( true );
		_previousRemainingTimeUntilKickoff = _remainingTimeUntilKickoff;
		_remainingTimeUntilKickoff = Conversions::StringToNumber<float>( messageElements[0] );

		std::stringstream ss;
		ss << std::trunc( _remainingTimeUntilKickoff ) + 1;
		std::string timerText = ss.str();
		_scene->_kickoffTimer->SetText( ss.str() );
		_scene->_kickoffTimerShadow->SetText( ss.str() );

		if( timerText == "3" )
		{
			_scene->_kickoffTimer->SetColor( sf::Color( 255, 191, 255 ) );
			_scene->_kickoffTimer->SetSize( 50 );
			_scene->_kickoffTimerShadow->SetSize( 50 );
		}
		else if( timerText == "2" )
		{
			_scene->_kickoffTimer->SetColor( sf::Color( 255, 128, 195 ) );
			_scene->_kickoffTimer->SetSize( 100 );
			_scene->_kickoffTimerShadow->SetSize( 100 );
		}
		else if( timerText == "1" )
		{
			_scene->_kickoffTimer->SetColor( sf::Color( 255, 75, 171 ) );
			_scene->_kickoffTimer->SetSize( 200 );
			_scene->_kickoffTimerShadow->SetSize( 200 );
		}

		if( _previousRemainingTimeUntilKickoff > 0.0f && _remainingTimeUntilKickoff <= 0.0f )
		{
			//_previousRemainingTimeUntilKickoff = -1.0f;
			//_remainingTimeUntilKickoff = -1.0f;

			_scene->_matchStartSound.play();
			_scene->_kickoffTimer->SetActive( false );
			_scene->_kickoffTimerShadow->SetActive( false );
		}
		
		return;
	}

	if ( message.find( "footballKick{}" ) == 0 )
	{
		if ( _scene == nullptr )
		{
			return;
		}

		_scene->_kickSounds.PlayRandomSound();
		return;
	}
}
