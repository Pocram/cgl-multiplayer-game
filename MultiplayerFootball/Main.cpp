#define _WINSOCKAPI_
#include <windows.h>
#include <WinSock2.h>

#include <iostream>
#include <stdio.h>
#include <string>

#include <Commons/Game/Game.hpp>
#include <Commons/FileManagers/MainFileManager.hpp>
#include <Commons/FileManagers/TextureManager.hpp>
#include <Commons/FileManagers/SoundManager.hpp>
#include <Commons/FileManagers/FontManager.hpp>
#include "Client/Game/Scenes/FootballGameScene.hpp"
#include <Commons/Game/ParseablePlayerInput.hpp>

std::string PromptUserForString( const std::string& promptMessage );

int main(int argc, char* argv[])
{
	// SET UP
	//====================================================================================================
	// Enable networking
	WSAData wsData;
	WSAStartup( MAKEWORD( 2, 2 ), &wsData );

	srand( time( nullptr ) );

	std::string serverId = PromptUserForString( "Please enter the server's IP address:" );
	std::string playerName = PromptUserForString( "Player name:" );

	std::cout << std::endl;

	printf( "Loading Assets ...\n" );
	sf::Clock assetLoadingClock;
	assetLoadingClock.restart();

	MainFileManager::Initialize( argv );
	TextureManager::Initialize();
	SoundManager::Initialize();
	FontManager::Initialize();

	printf( "Finished loading assets after %f seconds!\n", assetLoadingClock.getElapsedTime().asSeconds() );

	FootballGameScene* mainScene = new FootballGameScene( serverId, playerName );

	Game& game = Game::GetInstance();
	game.SetTargetFramerate( 144 );
	game.SetEnableVSynch( true );
	game.SetGameName( "Multiplayer Football Client" );
	game.SetWindowDimensionsByWidth( 1500, 16.0f / 9.0f );
	
	game.LoadScene( mainScene );

	game.Start();

	printf( "--------------------------------------------------------------------------------\n" );
	printf( "The game has ended.\nPress any key to close the application." );
	getchar();
	getchar();

	// CLEAN UP
	//====================================================================================================
	WSACleanup();
	FreeConsole();
	return 0;
}

std::string PromptUserForString( const std::string& promptMessage )
{
	std::string input;
	std::cout << promptMessage << " ";
	std::getline( std::cin, input );
	return input;
}